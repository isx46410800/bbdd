-- 2.1- Els identificadors de les oficines amb la seva ciutat, els objectius i les vendes reals.
training=# select ciudad, objetivo, ventas from oficinas;


-- 2.2- Els identificadors de les oficines de la regió est amb la seva ciutat, els objectius i les vendes reals.
training=# select ciudad, objetivo, ventas, oficinas from oficinas where region='Este';


-- 2.3- Les ciutats en ordre alfabètic de les oficines de la regió est amb els objectius i les vendes reals.
training=# select objetivo, ventas, ciudad, region from oficinas where region='Este' order by ciudad;


-- 2.4- Les ciutats, els objectius i les vendes d'aquelles oficines que les seves vendes superin els seus objectius.
Training=# select ciudad,objetivo, ventas from oficinas where ventas > objetivo;


-- 2.5- Nom, quota i vendes de l'empleat representant de vendes número 107.
training=# select nombre, cuota, ventas from repventas where num_empl=107;


-- 2.6- Nom i data de contracte dels representants de vendes amb vendes superiors a 300000.
training=# select nombre, contrato from repventas where ventas>300000;

-- 2.7- Nom dels representants de vendes dirigits per l'empleat numero 104 Bob Smith.
training=# select nombre from repventas where director=104;

-- 2.8- Nom dels venedors i data de contracte d'aquells que han estat contractats abans del 1988.
training=# select nombre, contrato from repventas where contrato < '1988-1-1';
  

-- 2.9- Identificador de les oficines i ciutat d'aquelles oficines que el seu objectiu és diferent a 800000.
training=# select oficina, ciudad from oficinas where objetivo <> 800000;


-- 2.10- Nom de l'empresa i limit de crèdit del client número 2107.
training=# select empresa, limite_credito from clientes where num_clie=2107;
-- 2.11- id_fab com a "Identificador del fabricant", id_producto com a "Identificador del producte" i descripcion com a "descripció" dels productes.
training=# select id_fab as "Identificador_del_fabricant", id_producto as "Identificador_del_producte", descripcion as "descripcion" from productos;



-- 2.12- Identificador del fabricant, identificador del producte i descripció del producte d'aquells productes que el seu identificador de fabricant acabi amb la lletra i.
training=# select id_fab, id_producto, descripcion from productos where id_fab like '%i';

-- 2.13- Nom i identificador dels venedors que estan per sota la quota i tenen vendes inferiors a 300000.
training=# select nombre, num_empl from repventas where ventas < 300000 and ventas < cuota;

-- 2.14- Identificador i nom dels venedors que treballen a les oficines 11 o 13.
training=# select nombre, num_empl from repventas where oficina_rep = 11 or oficina_rep = 13;

-- 2.15- Identificador, descripció i preu dels productes ordenats del més car al més barat.
training=# select id_fab, descripcion, precio from productos order by precio DESC;

-- 2.16- Identificador i descripció de producte amb el valor d'inventari (existencies * preu).
training=# select id_fab, descripcion, (existencias*precio) as valor_inventario from productos;



-- 2.17- Vendes de cada oficina en una sola columna i format amb format "<ciutat> te unes vendes de <vendes>", exemple "Denver te unes vendes de 186042.00".
training=# select ciudad || ' tiene ventas de ' || ventas from oficinas;

-- 2.18- Codis d'empleats que són directors d'oficines.
training=# select distinct dir from oficinas order by dir;
#Enseñaria 108,106,104,105 (Para no repetir se pone el distinct)
training=# select dir from oficinas order by dir;
#Enseñaria 108,106,104,105,108

-- 2.19- Identificador i ciutat de les oficines que tinguin ventes per sota el 80% del seu objectiu.
training=# select oficina, ciudad, (ventas/objetivo) as objetivos from oficinas where (ventas/objetivo) <= 0.80;

-- 2.20- Identificador, ciutat i director de les oficines que no siguin dirigides per l'empleat 108.
training=# select oficina, ciudad, dir from oficinas where dir!=108;


-- 2.21- Identificadors i noms dels venedors amb vendes entre el 80% i el 120% de llur quota.
training=# select num_empl, nombre from repventas where ventas/cuota >= 0.80 and ventas/cuota<=1.2; (mejor opcion)
training=# select num_empl, nombre from repventas where ventas/cuota between 0.80 and 1.2;


-- 2.22- Identificador, vendes i ciutat de cada oficina ordenades alfabèticament per regió i, dintre de cada regió ordenades per ciutat.
training=# select oficina, ventas, ciudad from oficinas order by region, ciudad;

-- 2.23- Llista d'oficines classificades alfabèticament per regió i, per cada regió, en ordre descendent de rendiment de vendes (vendes-objectiu).
training=# select oficina from oficinas order by region, ventas/objetivo desc;

-- 2.24- Codi i nom dels tres venedors que tinguin unes vendes superiors.
training=# select num_empl, nombre from repventas order by ventas desc limit 3;

-- 2.25- Nom i data de contracte dels empleats que les seves vendes siguin superiors a 500000.
training=# select nombre, contrato from repventas where ventas>500000;

-- 2.26- Nom i quota actual dels venedors amb el calcul d'una "nova possible quota" que serà la quota de cada venedor augmentada un 3 per cent de les seves propies vendes.
training=# select nombre, cuota, (cuota+0.03*ventas) as "Nova possible quota" from repventas;

-- 2.27- Identificador i nom de les oficines que les seves vendes estan per sota del 80% de l'objectiu.
training=# select oficina, ciudad from oficinas where (ventas/objetivo)<0.80;


-- 2.28- Numero i import de les comandes que el seu import oscil·li entre 20000 i 29999.
training=# select num_pedido, importe from pedidos where importe between 20000 and 29999;
training=# select num_pedido, importe from pedidos where importe>=20000 and importe<=29999;

-- 2.29- Nom, ventes i quota dels venedors que les seves vendes no estan entre el 80% i el 120% de la seva quota.
training=# select nombre, ventas, cuota from repventas where (ventas/cuota) < 0.80 or (ventas/cuota) > 1.20;
training=# select nombre, ventas, cuota from repventas where not ((ventas/cuota) >= 0.80 and (ventas/cuota) <= 1.20);
training=# select nombre, ventas, cuota from repventas where not (ventas between 0.8*cuota and 1.2*cuota);
training=# select nombre, ventas, cuota from repventas where not (ventas/cuota between 0.8 and 1.2);

-- 2.30- Nom de l'empresa i el seu limit de crèdit, de les empreses que el seu nom comença per Smith.
training=# select empresa, limite_credito from clientes where empresa like 'Smith%';
training=# select empresa, limite_credito from clientes where empresa ilike 'smith%'; (ilike → no es sensible a mayusculas/minisculas)

-- 2.31- Identificador i nom dels venedors que no tenen assignada oficina.
training=# select num_empl, nombre from repventas where oficina_rep is null;

-- 2.32- Identificador i nom dels venedors, amb l'identificador de l'oficina d'aquells venedors que tenen una oficina assignada.
training=# select num_empl, nombre from repventas where oficina_rep is not null;

-- 2.33- Identificador i descripció dels productes del fabricant identificat per imm dels quals hi hagin existències superiors o iguals 200, també del fabricant bic amb existències superiors o iguals a 50.
training=# select id_fab, descripcion from productos where (id_fab='imm' and existencias>=200) or (id_fab='bic' and existencias>=50);
training=# select id_fab, descripcion from productos where (id_fab like 'imm' and existencias>=200) or (id_fab like 'bic' and existencias>=50);

-- 2.34- Identificador i nom dels venedors que treballen a les oficines 11, 12 o 22 i compleixen algun dels següents supòsits:
-- a) han estat contractats a partir de juny del 1988 i no tenen director
training=# select num_empl, nombre from repventas where (oficina_rep=11 or oficina_rep=12 or oficina_rep=22) and (contrato > '1988-06-01' and director is null);
-- b) estan per sobre la quota però tenen vendes de 600000 o menors.
training=# select num_empl, nombre from repventas where (oficina_rep=11 or oficina_rep=12 or oficina_rep=22) and (ventas/cuota>1 and ventas<=600000);
training=# select num_empl, nombre from repventas where ((oficina_rep=11 or oficina_rep=12 or oficina_rep=22) and (contrato > '1988-06-01' and director is null)) or ((oficina_rep=11 or oficina_rep=12 or oficina_rep=22) and (ventas/cuota>1 and ventas<=600000));


-- 2.35- Identificador i descripció dels productes amb un preu superior a 1000 i siguin del fabricant amb identificador rei o les existències siguin superiors a 20.
training=# select id_fab, descripcion from productos where (precio>1000) and (id_fab='rei' or existencias>20);

-- 2.36- Identificador del fabricant,identificador i descripció dels productes amb fabricats pels fabricants que tenen una lletra qualsevol, una lletra 'i' i una altre lletra qualsevol com a identificador de fabricant.
training=# select id_fab, id_producto, descripcion from productos where id_fab like '_i_';

-- 2.37- Identificador i descripció dels productes que la seva descripció comença per "art" sense tenir en compte les majúscules i minúscules.
training=# select id_fab, descripcion from productos where descripcion ilike 'art%';

-- 2.38- Identificador i nom dels clients que la segona lletra del nom sigui una "a" minúscula o majuscula.
training=# select num_clie, empresa from clientes where empresa ilike '_a%';

-- 2.39- Identificador i ciutat de les oficines que compleixen algun dels següents supòsits:
-- a) És de la regió est amb unes vendes inferiors a 700000.
-- b) És de la regió oest amb unes vendes inferiors a 600000.
training=# select oficina, ciudad from oficinas where (region='Este' and ventas<700000) or (region='Oeste' and ventas<600000);


-- 2.40- Identificador del fabricant, identificació i descripció dels productes que compleixen tots els següents supòsits:
-- a) L'identificador del fabricant és "imm" o el preu és menor a 500.
-- b) Les existències són inferiors a 5 o el producte te l'identificador 41003.  
training=# select id_fab, id_producto, descripcion from productos where (id_fab='imm' or precio<500) or (existencias<5 or id_producto='41003');

-- 2.41- Identificador de les comandes del fabricant amb identificador "rei" amb una quantitat superior o igual a 10 o amb un import superior o igual a 10000.
training=# select num_pedido from pedidos where fab='rei' and (cant>=10 or importe>=10000);

-- 2.42- Data de les comandes amb una quantitat superior a 20 i un import superior a 1000 dels clients 2102, 2106 i 2109.
training=# select fecha_pedido from pedidos where (cant>20 and importe>1000) and (clie=2102 or clie=2106 or clie=2109);

-- 2.43- Identificador dels clients que el seu nom no conté " Corp." o " Inc." amb crèdit major a 30000.
training=# select empresa from clientes where (empresa not like '%Corp.%' or empresa not like '%Inc.%') and limite_credito>30000;

-- 2.44- Identificador dels representants de vendes majors de 40 anys amb vendes inferiors a 400000.
training=# select num_empl from repventas where edad>40 and ventas<400000;

-- 2.45- Identificador dels representants de vendes menors de 35 anys amb vendes superiors a 350000.
training=# select num_empl from repventas where edad<35 and ventas>350000;

