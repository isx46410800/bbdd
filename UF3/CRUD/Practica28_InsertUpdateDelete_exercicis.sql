--          Modificació
-------------------------------------------------------------------------------

-- 6.1-  Inserir un nou venedor amb nom "Enric Jimenez" amb identificador 1012, oficina 18, títol "Dir Ventas", contracte d'1 de febrer del 2012, director 101 i vendes 0.
training=# insert into copia_repventas values (1012, 'Enric Jimenez', 99, 18, 'Dir Ventas', '2012-01-02', 101, 0, 0);
INSERT 0 1
training=# select * from copia_repventas;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
     1012 | Enric Jimenez |   99 |          18 | Dir Ventas | 2012-01-02 |      101 |      0.00 |      0.00
(11 rows)

-- 6.2- Inserir un nou client "C1" i una nova comanda pel venedor anterior.
training=# insert into copia_clientes values (3000, 'C1', 1012, 20000);
INSERT 0 1
training=# select * from copia_clientes;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     3000 | C1                |     1012 |       20000.00
(22 rows)

training=# insert into copia_pedidos values (11500, '2019-05-06', 3000, 1013, null, null, null, null);
INSERT 0 1
training=# select * from copia_pedidos;
 num_pedido | fecha_pedido | clie | rep  | fab | producto | cant | importe  
------------+--------------+------+------+-----+----------+------+----------
      11400 | 2019-05-06   | 3001 | 1013 |     |          |      |         
      11500 | 2019-05-06   | 3000 | 1013 |     |          |      |         


-- 6.3- Inserir un nou venedor amb nom "Pere Mendoza" amb identificador 1013, contracte del 15 de agost del 2011 i vendes 0. La resta de camps a null.
training=# insert into copia_repventas values (1013, 'Pere Mendoza', null, null, null, '2011-08-15', null, null, 0);
INSERT 0 1
training=# select * from copia_repventas;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
     1012 | Enric Jimenez |   99 |          18 | Dir Ventas | 2012-01-02 |      101 |      0.00 |      0.00
     1013 | Pere Mendoza  |      |             |            | 2011-08-15 |          |           |      0.00
(12 rows)


-- 6.4- Inserir un nou client "C2" omplint els mínims camps.
training=# insert into copia_clientes (num_clie, empresa, rep_clie) values (3001, 'C2', 1013);
INSERT 0 1
training=# select * from copia_clientes;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     3000 | C1                |     1012 |       20000.00
     3001 | C2                |     1013 |               
(23 rows)


-- 6.5- Inserir una nova comanda del client "C2" al venedor "Pere Mendoza" sense especificar la llista de camps pero si la de valors.
training=# insert into copia_pedidos values (11400, '2019-05-06', 3001, 1013, null, null, null, null);
INSERT 0 1
training=# select * from copia_pedidos;
 num_pedido | fecha_pedido | clie | rep  | fab | producto | cant | importe  
------------+--------------+------+------+-----+----------+------+----------
      11400 | 2019-05-06   | 3001 | 1013 |     |          |      |         
(31 rows)


-- 6.6- Esborrar de la còpia de la base de dades el venedor afegit anteriorment anomenat "Enric Jimenez".
training=# delete from copia_repventas where nombre='Enric Jimenez';
DELETE 1
training=# select * from copia_repventas;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00
      104 | Bob Smith     |   33 |          12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00
      101 | Dan Roberts   |   45 |          12 | Rep Ventas | 1986-10-20 |      104 | 300000.00 | 305673.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00
      103 | Paul Cruz     |   29 |          12 | Rep Ventas | 1987-03-01 |      104 | 275000.00 | 286775.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00
     1013 | Pere Mendoza  |      |             |            | 2011-08-15 |          |           |      0.00
(11 rows)


-- 6.7- Eliminar totes les comandes del client "C1" afegit anteriorment.
training=# delete from copia_pedidos where clie=3000;
DELETE 1
training=# select * from copia_pedidos;
 num_pedido | fecha_pedido | clie | rep  | fab | producto | cant | importe  
------------+--------------+------+------+-----+----------+------+----------
     113003 | 1990-01-25   | 2108 |  109 | imm | 779c     |    3 |  5625.00
     113049 | 1990-02-10   | 2118 |  108 | qsa | xk47     |    2 |   776.00
     112987 | 1989-12-31   | 2103 |  105 | aci | 4100y    |   11 | 27500.00
     113057 | 1990-02-18   | 2111 |  103 | aci | 4100x    |   24 |   600.00
     113042 | 1990-02-02   | 2113 |  101 | rei | 2a44r    |    5 | 22500.00
      11400 | 2019-05-06   | 3001 | 1013 |     |          |      |         
(31 rows)


-- 6.8- Esborrar totes les comandes d'abans del 15-11-1989.
training=# delete from copia_pedidos where fecha_pedido<'1989-11-15';
DELETE 4
training=# select * from copia_pedidos;
 num_pedido | fecha_pedido | clie | rep  | fab | producto | cant | importe  
------------+--------------+------+------+-----+----------+------+----------
     112961 | 1989-12-17   | 2117 |  106 | rei | 2a44l    |    7 | 31500.00
     113012 | 1990-01-11   | 2111 |  105 | aci | 41003    |   35 |  3745.00
     112989 | 1990-01-03   | 2101 |  106 | fea | 114      |    6 |  1458.00
     113051 | 1990-02-10   | 2118 |  108 | qsa | k47      |    4 |  1420.00
     110036 | 1990-01-30   | 2107 |  110 | aci | 4100z    |    9 | 22500.00
     113045 | 1990-02-02   | 2112 |  108 | rei | 2a44r    |   10 | 45000.00
     112963 | 1989-12-17   | 2103 |  105 | aci | 41004    |   28 |  3276.00
     113013 | 1990-01-14   | 2118 |  108 | bic | 41003    |    1 |   652.00
     113058 | 1990-02-23   | 2108 |  109 | fea | 112      |   10 |  1480.00
     112997 | 1990-01-08   | 2124 |  107 | bic | 41003    |    1 |   652.00
     112983 | 1989-12-27   | 2103 |  105 | aci | 41004    |    6 |   702.00
     113024 | 1990-01-20   | 2114 |  108 | qsa | xk47     |   20 |  7100.00
     113062 | 1990-02-24   | 2124 |  107 | fea | 114      |   10 |  2430.00
     113027 | 1990-01-22   | 2103 |  105 | aci | 41002    |   54 |  4104.00
     113007 | 1990-01-08   | 2112 |  108 | imm | 773c     |    3 |  2925.00
     113069 | 1990-03-02   | 2109 |  107 | imm | 775c     |   22 | 31350.00
     113034 | 1990-01-29   | 2107 |  110 | rei | 2a45c    |    8 |   632.00
     112975 | 1989-12-12   | 2111 |  103 | rei | 2a44g    |    6 |  2100.00
     113055 | 1990-02-15   | 2108 |  101 | aci | 4100x    |    6 |   150.00
     113048 | 1990-02-10   | 2120 |  102 | imm | 779c     |    2 |  3750.00
     113065 | 1990-02-27   | 2106 |  102 | qsa | xk47     |    6 |  2130.00
     113003 | 1990-01-25   | 2108 |  109 | imm | 779c     |    3 |  5625.00
     113049 | 1990-02-10   | 2118 |  108 | qsa | xk47     |    2 |   776.00
     112987 | 1989-12-31   | 2103 |  105 | aci | 4100y    |   11 | 27500.00
     113057 | 1990-02-18   | 2111 |  103 | aci | 4100x    |   24 |   600.00
     113042 | 1990-02-02   | 2113 |  101 | rei | 2a44r    |    5 | 22500.00
      11400 | 2019-05-06   | 3001 | 1013 |     |          |      |         
(27 rows)


-- 6.9- Esborrar tots els clients dels venedors: Adams, Jones i Roberts.
training=# delete from copia_clientes where rep_clie=105 or rep_clie=109 or rep_clie=101; (rep_clie in (select num_empl from repventas where nombre like '%adams'...);
DELETE 7
training=# select * from copia_clientes;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2107 | Ace International |      110 |       35000.00
     2101 | Jones Mfg.        |      106 |       65000.00
     2112 | Zetacorp          |      108 |       50000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2114 | Orion Corp        |      102 |       20000.00
     2124 | Peter Brothers    |      107 |       40000.00
     2117 | J.P. Sinclair     |      106 |       35000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2106 | Fred Lewis Corp.  |      102 |       65000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2113 | Ian & Schmidt     |      104 |       20000.00
     2109 | Chen Associates   |      103 |       25000.00
     3000 | C1                |     1012 |       20000.00
     3001 | C2                |     1013 |               
(16 rows)


-- 6.10- Esborrar tots els venedors contractats abans del juliol del 1988 que encara no se'ls ha assignat una quota.
training=# delete from copia_repventas where contrato<'1988-07-01' and cuota is null;
DELETE 6
training=# select * from copia_repventas;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00
     1013 | Pere Mendoza  |      |             |            | 2011-08-15 |          |           |      0.00
(5 rows)


-- 6.11- Esborrar totes les comandes.
training=# delete from copia_pedidos;
DELETE 27
training=# select * from copia_pedidos;
 num_pedido | fecha_pedido | clie | rep | fab | producto | cant | importe 
------------+--------------+------+-----+-----+----------+------+---------
(0 rows)


-- 6.12- Esborrar totes les comandes acceptades per la Sue Smith (cal tornar a disposar  de la taula pedidos)
training=# delete from copia_pedidos where rep=102;
training=# delete from copia_pedidos where rep = (select num_empl from copia_repventas where nombre='Sue Smith';

DELETE 4
training=# select * from copia_pedidos;
 num_pedido | fecha_pedido | clie | rep  | fab | producto | cant | importe  
     113057 | 1990-02-18   | 2111 |  103 | aci | 4100x    |   24 |   600.00
     113042 | 1990-02-02   | 2113 |  101 | rei | 2a44r    |    5 | 22500.00
      11500 | 2019-05-06   | 3000 | 1013 |     |          |      |         
      11400 | 2019-05-06   | 3001 | 1013 |     |          |      |         
(28 rows)



-- 6.13- Suprimeix els clients atesos per venedors les vendes dels quals són inferiors al 80% de la seva quota.
training=# delete from copia_clientes where rep_clie in (select num_empl from repventas where ventas<cuota*0.8);
DELETE 2
training=# select * from copia_clientes;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2107 | Ace International |      110 |       35000.00
     2101 | Jones Mfg.        |      106 |       65000.00
     2112 | Zetacorp          |      108 |       50000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2114 | Orion Corp        |      102 |       20000.00
     2117 | J.P. Sinclair     |      106 |       35000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2106 | Fred Lewis Corp.  |      102 |       65000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2109 | Chen Associates   |      103 |       25000.00
     3000 | C1                |     1012 |       20000.00
     3001 | C2                |     1013 |               
(14 rows)


-- 6.14- Suprimir els venedors els quals el seu total de comandes actual (imports) és menor que el 2% de la seva quota.
training=# delete from copia_repventas where 0.02*cuota >= any(select sum(importe) from pedidos where num_empl=rep);
DELETE 3
training=# select * from copia_repventas;
 num_empl |    nombre    | edad | oficina_rep |   titulo   |  contrato  | director | cuota |  ventas  
----------+--------------+------+-------------+------------+------------+----------+-------+----------
      110 | Tom Snyder   |   41 |             | Rep Ventas | 1990-01-13 |      101 |       | 75985.00
     1013 | Pere Mendoza |      |             |            | 2011-08-15 |          |       |     0.00
(2 rows)


-- 6.15- Suprimeix els clients que no han realitzat comandes des del 10-11-1989.
training=# delete from copia_clientes where num_clie not in (select clie from copia_pedidos where fecha_pedido>'1989-11-10');
DELETE 4
training=# select * from copia_clientes;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2107 | Ace International |      110 |       35000.00
     2101 | Jones Mfg.        |      106 |       65000.00
     2112 | Zetacorp          |      108 |       50000.00
     2114 | Orion Corp        |      102 |       20000.00
     2117 | J.P. Sinclair     |      106 |       35000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2109 | Chen Associates   |      103 |       25000.00
     3000 | C1                |     1012 |       20000.00
     3001 | C2                |     1013 |               
(10 rows)



-- 6.16 Eleva el límit de crèdit de l'empresa Acme Manufacturing a 60000 i la reassignes a Mary Jones.
training=# update copia_clientes set limite_credito=60000, rep_clie=109 where num_clie=2103;
training=# update copia_clientes set limite_credito=60000, rep_clie=(select num_empl from repventas where nombre='Mary Jones') where empresa='Acme Mfg.';
UPDATE 1
training=# select * from copia_clientes where num_clie=2103;
 num_clie |  empresa  | rep_clie | limite_credito 
----------+-----------+----------+----------------
     2103 | Acme Mfg. |      109 |       60000.00
(1 row)


-- 6.17- Transferir tots els venedors de l'oficina de Chicago (12) a la de Nova York (11), i rebaixa les seves quotes un 10%.
training=# update copia_repventas set oficina_rep=11, cuota=cuota-cuota*0.10 where oficina_rep=12;
training=# update copia_repventas set oficina_rep=(select oficina from oficinas where ciudad='New York'), cuota=cuota-cuota*0.10 where oficina_rep=(select oficina from oficinas where ciudad='Chicago');

UPDATE 3
training=# select * from copia_repventas;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00
      104 | Bob Smith     |   33 |          11 | Dir Ventas | 1987-05-19 |      106 | 180000.00 | 142594.00
      101 | Dan Roberts   |   45 |          11 | Rep Ventas | 1986-10-20 |      104 | 270000.00 | 305673.00
      103 | Paul Cruz     |   29 |          11 | Rep Ventas | 1987-03-01 |      104 | 247500.00 | 286775.00
(10 rows)


-- 6.18- Reassigna tots els clients atesos pels empleats 105, 106, 107, a l'emleat 102.
training=# update copia_clientes set rep_clie=102 where rep_clie in (105,106,107);
UPDATE 4
training=# select * from copia_clientes;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2102 | First Corp.       |      101 |       65000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2107 | Ace International |      110 |       35000.00
     2115 | Smithson Corp.    |      101 |       20000.00
     2112 | Zetacorp          |      108 |       50000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2114 | Orion Corp        |      102 |       20000.00
     2108 | Holm & Landis     |      109 |       55000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2106 | Fred Lewis Corp.  |      102 |       65000.00
     2119 | Solomon Inc.      |      109 |       25000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2113 | Ian & Schmidt     |      104 |       20000.00
     2109 | Chen Associates   |      103 |       25000.00
     2105 | AAA Investments   |      101 |       45000.00
     2103 | Acme Mfg.         |      109 |       60000.00
     2101 | Jones Mfg.        |      102 |       65000.00
     2124 | Peter Brothers    |      102 |       40000.00
     2117 | J.P. Sinclair     |      102 |       35000.00
     2122 | Three-Way Lines   |      102 |       30000.00
(21 rows)



-- 6.19- Assigna una quota de 100000 a tots aquells venedors que actualment no tenen quota.
training=# update copia_repventas set cuota=100000 where cuota is null;
UPDATE 1
training=# select * from copia_repventas;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00
      104 | Bob Smith     |   33 |          11 | Dir Ventas | 1987-05-19 |      106 | 180000.00 | 142594.00
      101 | Dan Roberts   |   45 |          11 | Rep Ventas | 1986-10-20 |      104 | 270000.00 | 305673.00
      103 | Paul Cruz     |   29 |          11 | Rep Ventas | 1987-03-01 |      104 | 247500.00 | 286775.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 | 100000.00 |  75985.00
(10 rows)


-- 6.20- Eleva totes les quotes un 5%.
training=# update copia_repventas set cuota= cuota + cuota*0.05;
UPDATE 10
training=# select * from copia_repventas;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 367500.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 315000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 367500.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 288750.00 | 299912.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 367500.00 | 361865.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 315000.00 | 186042.00
      104 | Bob Smith     |   33 |          11 | Dir Ventas | 1987-05-19 |      106 | 189000.00 | 142594.00
      101 | Dan Roberts   |   45 |          11 | Rep Ventas | 1986-10-20 |      104 | 283500.00 | 305673.00
      103 | Paul Cruz     |   29 |          11 | Rep Ventas | 1987-03-01 |      104 | 259875.00 | 286775.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 | 105000.00 |  75985.00
(10 rows)


-- 6.21- Eleva en 5000 el límit de crèdit de qualsevol client que ha fet una comanda d'import superior a 25000.
training=# update copia_clientes set limite_credito=limite_credito + 5000 where num_clie in (select clie from  copia_pedidos where importe>25000);
UPDATE 4
training=# select * from copia_clientes;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2102 | First Corp.       |      101 |       65000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2107 | Ace International |      110 |       35000.00
     2115 | Smithson Corp.    |      101 |       20000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2114 | Orion Corp        |      102 |       20000.00
     2108 | Holm & Landis     |      109 |       55000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2106 | Fred Lewis Corp.  |      102 |       65000.00
     2119 | Solomon Inc.      |      109 |       25000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2113 | Ian & Schmidt     |      104 |       20000.00
     2105 | AAA Investments   |      101 |       45000.00
     2101 | Jones Mfg.        |      102 |       65000.00
     2124 | Peter Brothers    |      102 |       40000.00
     2122 | Three-Way Lines   |      102 |       30000.00
     2112 | Zetacorp          |      108 |       55000.00
     2109 | Chen Associates   |      103 |       30000.00
     2103 | Acme Mfg.         |      109 |       65000.00
     2117 | J.P. Sinclair     |      102 |       40000.00
(21 rows)

   
-- 6.22- Reassigna tots els clients atesos pels venedors les vendes dels quals són menors al 80% de les seves quotes. Reassignar al venedor 105.
training=# update copia_clientes set rep_clie=105 where rep_clie in (select num_empl from copia_repventas where ventas<cuota*0.8);
UPDATE 2
training=# select * from copia_clientes;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2102 | First Corp.       |      101 |       65000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2115 | Smithson Corp.    |      101 |       20000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2114 | Orion Corp        |      102 |       20000.00
     2108 | Holm & Landis     |      109 |       55000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2106 | Fred Lewis Corp.  |      102 |       65000.00
     2119 | Solomon Inc.      |      109 |       25000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2105 | AAA Investments   |      101 |       45000.00
     2101 | Jones Mfg.        |      102 |       65000.00
     2124 | Peter Brothers    |      102 |       40000.00
     2122 | Three-Way Lines   |      102 |       30000.00
     2112 | Zetacorp          |      108 |       55000.00
     2109 | Chen Associates   |      103 |       30000.00
     2103 | Acme Mfg.         |      109 |       65000.00
     2117 | J.P. Sinclair     |      102 |       40000.00
     2107 | Ace International |      105 |       35000.00
     2113 | Ian & Schmidt     |      105 |       20000.00
(21 rows)

   
-- 6.23- Fer que tots els venedors que atenen a més de tres clients estiguin sota de les ordres de Sam Clark (106).
training=# update copia_repventas set director=106 where num_empl in (select rep_clie from copia_clientes group by rep_clie having count(rep_clie)>3);
UPDATE 1
training=# select * from copia_repventas;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 367500.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 315000.00 | 392725.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 288750.00 | 299912.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 367500.00 | 361865.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 315000.00 | 186042.00
      104 | Bob Smith     |   33 |          11 | Dir Ventas | 1987-05-19 |      106 | 189000.00 | 142594.00
      101 | Dan Roberts   |   45 |          11 | Rep Ventas | 1986-10-20 |      104 | 283500.00 | 305673.00
      103 | Paul Cruz     |   29 |          11 | Rep Ventas | 1987-03-01 |      104 | 259875.00 | 286775.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 | 105000.00 |  75985.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      106 | 367500.00 | 474050.00
(10 rows)



-- 6.24- Augmentar un 50% el límit de credit d'aquells clients que totes les seves comandes tenen un import major a 30000.
training=# update copia_clientes set limite_credito=limite_credito + limite_credito*0.5 where num_clie in (select clie from copia_pedidos group by clie having sum(importe)>30000);
UPDATE 4
training=# select * from copia_clientes;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2102 | First Corp.       |      101 |       65000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2115 | Smithson Corp.    |      101 |       20000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2114 | Orion Corp        |      102 |       20000.00
     2108 | Holm & Landis     |      109 |       55000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2106 | Fred Lewis Corp.  |      102 |       65000.00
     2119 | Solomon Inc.      |      109 |       25000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2105 | AAA Investments   |      101 |       45000.00
     2101 | Jones Mfg.        |      102 |       65000.00
     2124 | Peter Brothers    |      102 |       40000.00
     2122 | Three-Way Lines   |      102 |       30000.00
     2107 | Ace International |      105 |       35000.00
     2113 | Ian & Schmidt     |      105 |       20000.00
     2112 | Zetacorp          |      108 |       82500.00
     2109 | Chen Associates   |      103 |       45000.00
     2103 | Acme Mfg.         |      109 |       97500.00
     2117 | J.P. Sinclair     |      102 |       60000.00
(21 rows)

-- ALERTA sense l'EXISTS augmenta el limit de credit dels clients que no tenen comandes.
training=# update copia_clientes set limite_credito=limite_credito + 0.5*limite_credito where 30000 < all (select importe from pedidos where clie=num_clie) and exists(select importe from pedidos where clie=num_clie);
UPDATE 2


-- 6.25- Disminuir un 2% el preu d'aquells productes que tenen un estoc superior a 200 i no han tingut comandes.
training=# update copia_productos set precio=precio-precio*0.02 where existencias>200 and not exists(select * from copia_pedidos where fab=id_fab and producto=id_producto);
UPDATE 3


-- 6.26- Establir un nou objectiu per aquelles oficines en que l'objectiu actual sigui inferior a les vendes. Aquest nou objectiu serà el doble de la suma de les vendes dels treballadors assignats a l'oficina.
training=# update copia_oficinas set objetivo=2*(select sum(ventas) from copia_repventas where oficina_rep=oficina) where objetivo<ventas;
UPDATE 3



-- 6.27- Modificar la quota dels directors d'oficina que tinguin una quota superior a la quota d'algun empleat de la seva oficina. Aquests directors han de tenir la mateixa quota que l'empleat de la seva oficina que tingui una quota menor.



-- 6.28- Cal que els 5 clients amb un total de compres (cantidad) més alt siguin transferits a l'empleat Tom Snyder i que se'ls augmenti el límit de crèdit en un 50%.



-- 6.29- Es volen donar de baixa els productes dels que no tenen existències i alhora no se n'ha venut cap des de l'any 89.



-- 6.30- Afegir una oficina de la ciutat de "San Francisco", regió oest, el director ha de ser "Larry Fitch", les vendes 0, l'objectiu ha de ser la mitja de l'objectiu de les oficines de l'oest i l'identificador de l'oficina ha de ser el següent valor després del valor més alt.


