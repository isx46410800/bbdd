-- videoclub 

create table socis (
    id_socio serial PRIMARY KEY,
    dni char(9) UNIQUE check length(dni) = 9,
    nombre varchar(20) not null,
    primer_apellido varchar(20) not null,
    segundo_apellido varchar(20),
    direccion varchar(30) not null,
    telefono int not null,
    sexo char(1)
);

create table directors (
    id_dir serial PRIMARY KEY,
    nombre varchar(20) not null,
    apellido varchar(20) not null,
    nacionalidad varchar(20),
    UNIQUE(nombre, apellido)
);

create table generes (
    id_gen serial PRIMARY KEY,
    genero VARCHAR(20) not NULL
);

create table actors (
    id_actor serial PRIMARY KEY,
    nombre varchar(20) not null,
    apellido varchar(20) not null,
    UNIQUE(nombre, apellido)
);

create table pelicules (
    id_pelicula serial PRIMARY key,
    titulo  varchar(40) not null,
    genero smallint REFERENCES generes(id_gen),
    director SMALLINT REFERENCES directors(id_dir),
    duracion decimal,
    descripcion text not null,
    fecha date not null,
    UNIQUE(titulo)
);


create table dvds (
    coddvd serial PRIMARY KEY,
    descrip text,
    codpeli serial not null REFERENCES pelicules(id_pelicula),
    reservat char(1) default 'N',
);


create table lloguer (
    codsoci smallint not null REFERENCES socis(id_socio),
    coddvd SMALLINT not null REFERENCES dvds(coddvd),
    datapres TIMESTAMP with time zone default current_timestamp,
    datadev date,
    import int
);

create table repartiment (
	**id_pelicula_actor serial primary key,**
	id_pelicula smallint not null references pelicules(id_pelicula),
	id_actor smallint references actors(id_actor)  
	unique(id_pelicula,id_actor
);


create table generosfav (
    codsoci  smallint not null REFERENCES socis(id_socio),
    codgen   smallint not null REFERENCES generes(id_gen),
    unique(codsoci, codgen)
);

create table actoresfav (
    codsoci  smallint not null REFERENCES socis(id_socio),
    codactor SMALLINT not null REFERENCES actors(id_actor),
    unique(codsoci, codactor)
);

create table directorsfav (
    codsoci  smallint not null REFERENCES socis(id_socio),
    coddir SMALLINT not null REFERENCES directors(id_dir),
    unique(codsoci, coddir)
);




---------------  reserves hotels 


CREATE TABLE TipusHab (
  CodTipus serial PRIMARY KEY,
  Descripcio varchar(50) not null UNIQUE,
  Preu money not null
);


CREATE TABLE Habitacio (
  NumHab smallint PRIMARY KEY, 
  CodTipus int not null REFERENCES TipusHab(codTipus)
);

CREATE TABLE Servei (
  IdServei serial PRIMARY KEY,
  Descripcio varchar(100) not null UNIQUE,
  Preu money not null
);


CREATE TABLE Client (
  CodClient serial PRIMARY KEY,
  DNI char(9) UNIQUE not null check length(dni) = 9,
  Nom varchar(20) not null,
  Cognom1 varchar(20) not null,
  Cognom2 varchar(20),
  Adreca varchar(50),
  Ciutat varchar(20),
  Telefon numeric(9,0) not null CHECK ( Telefon >= 100000000 and Telefon <= 999999999 ),
  Nacionalitat varchar(20),
  Sexe char(1)
);


CREATE TABLE Reserva (
  IdReserva serial PRIMARY key,
  NumHab smallint not null REFERENCES habitacio(NumHab),
  CodClient INTEGER not null REFERENCES Client(CodClient),
  DataArribada date not null,
  DataSortida date not null,
  DataReserva date not null DEFAULT current_date,
  Cancelada boolean default false
);
