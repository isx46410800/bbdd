CREATE DATABASE: creació de base de dades

	CREATE DATABASE training;

CREATE TABLE: creació de taules

    CREATE TABLE clientes (
	        num_clie smallint SERIAL, --serial: va poniendo automaticamente el numero en los siguientes registros, +1
	        empresa character varying(20) NOT NULL, --limite de 20 chars y si se pone mas, lo cortará
	        rep_clie smallint NOT NULL, --not null indica que es obligatorio poner el valor en ese campo
	        limite_credito numeric(8,2) --maximo 8 cifras con 2 decimales
	    );

    CONSTRAINTS: es poden posar a cada columna o al final de la creació de la taula, mirar documentació Postgresql -- se puede poner mas de un parametro
        NOT NULL: la columna no pot ser NULL
        PRIMARY KEY: clau primària
        FOREIGN KEY/REFERENCES: clau forània, per assegurar integritat referencial
        DEFAULT: posar valor per defecte
        UNIQUE: cada valor ha de ser únic a la columna. Mirar: http://stackoverflow.com/questions/9565996/difference-between-primary-key-and-unique-key --puede ser NOT NULL/NULL
        CHECK: comprova que els valors compleixin una condició

	    CREATE TABLE clientes (
	        num_clie smallint PRIMARY KEY, --al poner primary key no se puede repetir ese valor, sin eso, puede ponerse el mismo num_clie, primarykey= unique y not null
	        --CIF varchar(10) UNIQUE,
	        empresa character varying(20) NOT NULL, --si ponemos "text" no hace falta poner tope de chars
	        rep_clie smallint NOT NULL,
	        limite_credito numeric(8,2)
	    );

	    CREATE TABLE clientes (
	        num_clie smallint,
	        empresa character varying(20) NOT NULL, --character varying=varchar
	        rep_clie smallint NOT NULL,
	        limite_credito numeric(8,2),
	        PRIMARY KEY(num_clie) -- en vez de ponerlo en el campo, podemos ponerlo a final de tabla el Primary key
	    );

	    CREATE TABLE productos (
	        id_fab character(3),
	        id_producto character(5),
	        descripcion character varying(20) NOT NULL,
	        precio numeric(7,2) NOT NULL,
	        existencias integer NOT NULL,
	        PRIMARY KEY(id_fab, id_producto) -- no se puede poner dos campos con primary key, se pone uno conjuntado a final de la creacion de la tabla
	    );

	    CREATE TABLE clientes (
	        num_clie smallint PRIMARY KEY,
	        empresa character varying(20) NOT NULL,
	        rep_clie smallint NOT NULL 
	                          DEFAULT 108, --sino ponemos ningun valor, nos podrá este valor por defecto
	        limite_credito numeric(8,2)
	    );
	    -- No cal posar rep_clie tot i que sigui un camp NOT NULL, ja té valor per defecte
	    INSERT INTO clientes(num_clie, empresa) VALUES(3000, 'Bit'); 

	    CREATE TABLE clientes (
	        num_clie smallint PRIMARY KEY,
	        empresa character varying(20) NOT NULL,
	        rep_clie smallint NOT NULL,
	        limite_credito numeric(8,2),
	        CHECK (num_clie < 1000) --condicion para que haga el check
	    );

    Integritat referencial: La integritat referencial exigeix que els valors d''una columna que és clau forània, hagi d''existir com a clau primària de la taula relacionada.

   -- Exemple: Si a la taula clientes, una fila té el camp rep_clie amb valor 108, hauria d''existir una fila a repventas amb num_empl 108.

    REFERENCES: denota que és clau forània, força integritat referencial en afegir files

        CREATE TABLE clientes (
            num_clie smallint PRIMARY KEY,
            empresa character varying(20) NOT NULL,
            rep_clie smallint NOT NULL 
                              REFERENCES repventas(num_empl), --su valor va referenciado con el valor de esa tabla, ha de existir en esa otra tabla para poder poner ese valor
            limite_credito numeric(8,2)						  --siempre hace referencia a una primary key
        );

o

	CREATE TABLE clientes (
            num_clie smallint,
            empresa character varying(20) NOT NULL,
            rep_clie smallint NOT NULL,
            limite_credito numeric(8,2),
            PRIMARY KEY(num_clie),
	    FOREIGN KEY (rep_clie) REFERENCES repventas(num_empl) --ponerlo de manera a final de tabla, tabla hija a la tabla madre
        );

        INSERT INTO clientes
        VALUES (200, 'Bit', 212, 10000); -- Donarà error, el treballador 212 no existeix

    ON DELETE: què fer si trenquem integritat referencial en esborrar una fila de la taula referenciada
    ON UPDATE: què fer si trenquem integritat referencial en modificar una fila de la taula referenciada

    RESTRICT: retorna un error i no es deixa fer l''operació
    CASCADE: esborra o modifica les files afectades
    SET DEFAULT: es posa el valor per defecte
    SET NULL: es posa el valor a NULL

        CREATE TABLE clientes (
            num_clie smallint,
            empresa character varying(20) NOT NULL,
            rep_clie smallint NOT NULL 
                              REFERENCES repventas(num_empl)
                              ON DELETE RESTRICT,
            limite_credito numeric(8,2),
            PRIMARY KEY(num_clie)
        );
        
        INSERT INTO clientes
        VALUES (200, 'Bit', 108, 10000);
        
        DELETE FROM repventas
        WHERE num_empl = 108; -- Donarà error, hi ha un client amb el treballador 108 associat

        CREATE TABLE clientes (
            num_clie smallint,
            empresa character varying(20) NOT NULL,
            rep_clie smallint NOT NULL 
                              REFERENCES repventas(num_empl)
                              ON DELETE CASCADE,
            limite_credito numeric(8,2),
            PRIMARY KEY(num_clie)
        );
        
        INSERT INTO clientes
        VALUES (200, 'Bit', 108, 10000);
        
        DELETE FROM repventas
        WHERE num_empl = 108; -- Esborrarà la fila de repventas i també tots els clients que tenen per representant el 108
        
        CREATE TABLE clientes (
            num_clie smallint,
            empresa character varying(20) NOT NULL,
            rep_clie smallint NOT NULL
                              DEFAULT 105 
                              REFERENCES repventas(num_empl)
                              ON DELETE RESTRICT
                              ON UPDATE SET DEFAULT,
            limite_credito numeric(8,2),
            PRIMARY KEY(num_clie)
        );
        
        INSERT INTO clientes
        VALUES (200, 'Bit', 108, 10000);
        
        UPDATE repventas 
        SET num_empl = 222 
        WHERE num_empl = 101;  

    INHERITS: hereda tota l'estructura d'una taula i afegim camps nous
    
        CREATE TABLE clientes_vip (puntos integer) INHERITS (clientes);
        
    CREATE TABLE ... AS : crear una taula a partir d''una consulta
    
        CREATE TABLE oficinas_este AS (SELECT oficina, ciudad, objetivo, ventas
                                       FROM oficinas 
                                       WHERE region = 'Oeste');

    CREATE TEMP TABLE ... : crea una taula temporal. Quan es tanca la sessió, la taula desapareix.

DROP DATABASE: esborrar base de dades

	DROP DATABASE training;

DROP TABLE: esborrar una taula

	DROP DATABASE oficinas;

ALTER TABLE: modificar estructura de la base de dades

	Afegir camp:
		ALTER TABLE oficinas 
			ADD direccion varchar(100);

	Modificar camp:
		ALTER TABLE oficinas 
			ALTER direccion varchar(200);

	Esborrar camp:
		ALTER TABLE oficinas 
			DROP direccion;

	Afegir constraint:
		ALTER TABLE oficinas
			ADD CHECK(ciudad <> 'Barcelona')
			
                o amb nom

        	ALTER TABLE oficinas
			ADD CONSTRAINT no_badalona CHECK(ciudad <> 'Badalona')
