-- --------------------------------------------
--    Estructura
-- --------------------------------------------

-- 8.1- Crear una base de dades anomenada "biblioteca". Dins aquesta base de dades
-- crear una taula anomenada llibres amb els camps següents:
-- - "ref": clau primaria numèrica que identifica els llibres i s'ha d'assignar automàticament.
-- - "titol": títol del llibre.
-- - "editorial": nom de l'editorial.
-- - "autor": identificador de l'autor, clau forana, per defecte ha de
-- referenciar a primer valor de la taula autors que simbolitzar l'autor "Anònim".

create database biblioteca;
-- si pone serial no hace falta poner smallint

create table llibres (
		ref serial primary key,
		titol varchar(200) not null,
		editorial varchar(100) not null,
		autor int not null default 1
		foreign key (autor) references autors(autor)
	);
	
biblioteca=# create table llibres (
ref serial primary key,
titol varchar(200) not null,
editorial varchar(100) not null,
autor int not null default 1 references autors(autor)
);

biblioteca=# select * from llibres;
 ref | titol | editorial | autor 
-----+-------+-----------+-------
(0 rows)


-- Crear una altre taula anomenada "autors" amb els següents camps:
-- - "autor": identificador de l'autor.
-- - "nom": Nom i cognoms de l'autor.
-- - "nacionalitat": Nacionalitat de l'autor.
-- Ambdues taules han de mantenir integritat referencial. Cal que si es trenca
-- la integritat per delete d'autor, la clau forana del llibre apunti a "Anònim".
-- Si es trenca la integritat per insert/update s'ha d'impedir l'alta/modificació.
-- Cal inserir l'autor "Anònim" a la taula autors.

create table autors (
		autor serial primary key, 
		nom varchar(100) not null
		nacionalitat varchar(50)
	);
INSERT INTO autors VALUES (1, 'Anonim');

--id_autor es clave y serial para que aumenta +1

biblioteca=# create table autors (
autor serial primary key, 
nom varchar(100) not null,
nacionalitat varchar(50)
);

biblioteca=# INSERT INTO autors VALUES (1, 'Anonim');

biblioteca=# select * from autors;
 autor |  nom   | nacionalitat 
-------+--------+--------------
     1 | Anonim | 
(1 row)

--integritat error:
--que funcione la integridad significa que si ponemos campos con primary key repetida tiene que dar error.
--o que relacione mal la integridad y pongas un campo que no sale con su referencia madre
biblioteca=# INSERT INTO llibres(ref,titol,editorial,autor) values(1, 'Sangre y fuego', 'Amaya', 2);
ERROR:  insert or update on table "llibres" violates foreign key constraint "llibres_autor_fkey"
DETAIL:  Key (autor)=(2) is not present in table "autors".

-- 8.2- A la base biblioteca cal crear una taula anomenada "socis"
-- amb els següents camps:
-- - num_soci: clau primària
-- - nom: nom i cognoms del soci.
-- - dni: DNI del soci.

create table socis (
		num_soci int primary key,
		nom varchar(100) not null,
		dni varchar(9) unique not null
	);
	
biblioteca=# create table socis (
num_soci int primary key,
nom varchar(100) not null,
dni varchar(9) unique not null
);

-- També una taula anomenada préstecs amb els següents camps:
-- - ref: clau forana, que fa referència al llibre prestat.
-- - soci: clau forana, que fa referència al soci.
-- - data_prestec: data en que s'ha realitzat el préstec.
-- No cal que préstecs tingui clau principal ja que només és una taula de relació.
-- En eliminar un llibre cal que s'eliminin els seus préstecs automàticament.
-- No s'ha de poder eliminar un soci amb préstecs pendents.

create table prestecs (
		ref int not null,
		soci smallint not null,
		data_prestec date default current_date, -- para que ponga la fecha de hoy por defecto
		foreign key (ref) references llibres(ref) on delete cascade,
		foreign key (soci) references socis(num_soci) on delete restrict
	);

--on delete restrict si quieres borrar un socio, no te dejará
biblioteca=# create table prestecs (
ref int not null,
soci smallint not null,
data_prestec date default current_date,
foreign key (ref) references llibres(ref) on delete cascade,
foreign key (soci) references socis(num_soci) on delete restrict
);

-- 8.3- A la base de dades training crear una taula anomenada "repventas_baja" que tingui la mateixa estructura que la taula repventas i 
--a més a més un camp anomenat "baja" que pugui contenir la data en que un representant de vendes s'ha donat de baixa.
training=# create table repventas_baja(baja date) inherits (repventas);
CREATE TABLE

training=# select * from repventas_baja ;
 num_empl | nombre | edad | oficina_rep | titulo | contrato | director | cuota | ventas | baja 
----------+--------+------+-------------+--------+----------+----------+-------+--------+------
(0 rows)

insert into repventas_baja(select * from repventas); -- te rellena todos los datos de la table que copiamos

training=# insert into repventas_baja(select * from repventas);
INSERT 0 10
training=# select * from repventas_baja ;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   | baja 
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------+------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00 | 
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00 | 
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00 | 
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00 | 
      104 | Bob Smith     |   33 |          12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00 | 
      101 | Dan Roberts   |   45 |          12 | Rep Ventas | 1986-10-20 |      104 | 300000.00 | 305673.00 | 
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 |           |  75985.00 | 
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00 | 
      103 | Paul Cruz     |   29 |          12 | Rep Ventas | 1987-03-01 |      104 | 275000.00 | 286775.00 | 
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00 | 
(10 rows)


-- 8.4- A la base de dades training crear una taula anomenada "productos_sin_pedidos" omplerta amb aquells productes que no han tingut mai cap comanda. 
--A continuació esborrar de la taula "productos" aquells productes que estan en aquesta nova taula.
training=# select id_fab, id_producto from productos where not exists (select fab,producto from pedidos where id_fab=fab and id_producto=producto);
 id_fab | id_producto 
--------+-------------
 bic    | 41672
 imm    | 887p 
 qsa    | xk48 
 imm    | 887h 
 bic    | 41089
 aci    | 41001
 qsa    | xk48a
 imm    | 887x 
(8 rows)

--solo mantiene la estructura
training=# create table productos_sin_pedidos as (select id_fab, id_producto, descripcion, precio, existencias from productos where not exists (select fab,producto from pedidos where id_fab=fab and id_producto=producto));
SELECT 8

training=# delete from copia_productos where id_fab not in (select fab from pedidos where id_fab=fab and id_producto=producto) and id_producto not in(select producto from pedidos where id_producto=producto and id_fab=fab);
DELETE 8
--para mantener estructura y relaciones de tabla con sus formatos de campo
create table productos_sin_pedidos() inherits (productos);
insert into repventas_baja(select id_fab, id_producto, descripcion, precio, existencias from productos where not exists (select fab,producto from pedidos where id_fab=fab and id_producto=producto));;

-- 8.5- A la base de dades training crear una taula temporal que substitueixi la taula "clientes" però només ha de contenir aquells clients que no han fet comandes
-- i tenen assignat un representant de vendes amb unes vendes inferiors al 110% de la seva quota.
training=# select num_clie from clientes where num_clie not in (select clie from pedidos where num_clie=clie) and rep_clie in (select num_empl from repventas where rep_clie=num_empl and ventas<cuota*1.1);
 num_clie 
----------
     2115
     2121
     2122
     2105
(4 rows)

training=# create temp table clientes_temp as (select num_clie, empresa, rep_clie, limite_credito from clientes where num_clie not in (select clie from pedidos where num_clie=clie) and rep_clie in (select num_empl from repventas where rep_clie=num_empl and ventas<cuota*1.1));
SELECT 4
training=# select * from clientes_temp ;
 num_clie |     empresa     | rep_clie | limite_credito 
----------+-----------------+----------+----------------
     2115 | Smithson Corp.  |      101 |       20000.00
     2121 | QMA Assoc.      |      103 |       45000.00
     2122 | Three-Way Lines |      105 |       30000.00
     2105 | AAA Investments |      101 |       45000.00
(4 rows)



-- 8.6- Escriu les sentències necessàries per a crear l'estructura de l'esquema proporcionat de la base de dades training. Justifica les accions a realitzar en modificar/actualitzar les claus primàries.
CREATE TABLE clientes (
    num_clie smallint primary key,
    empresa character varying(20) unique NOT NULL,
    rep_clie smallint NOT NULL references repventas(num_empl),
    limite_credito numeric(8,2)
);

CREATE TABLE oficinas (
    oficina smallint primary key,
    ciudad character varying(15) unique NOT NULL,
    region character varying(10) NOT NULL,
    dir smallint,
    objetivo numeric(9,2),
    ventas numeric(9,2) NOT NULL default 0
);

CREATE TABLE pedidos (
    num_pedido integer primary key,
    fecha_pedido date NOT NULL default current_date,
    clie smallint NOT NULL references clientes(num_clie),
    rep smallint references repventas(num_empl),
    fab character(3) NOT NULL,
    producto character(5) NOT NULL,
    cant smallint NOT NULL,
    importe numeric(7,2) NOT NULL
    foreign key (fab, producto) references productos(id_fab,id_producto)
);

CREATE TABLE productos (
    id_fab character(3),
    id_producto character(5),
    descripcion character varying(20) NOT NULL,
    precio numeric(7,2) NOT NULL default 0,
    existencias integer NOT NULL default 0,
    PRIMARY KEY(id_fab, id_producto)
);

CREATE TABLE repventas (
    num_empl smallint primary key,
    nombre character varying(15) NOT NULL,
    edad smallint,
    oficina_rep smallint references oficinas(oficina),
    titulo character varying(10),
    contrato date NOT NULL,
    director smallint references repventas(num_empl),
    cuota numeric(8,2),
    ventas numeric(8,2) NOT NULL
);

alter table oficinas add constraint director_de_la_oficina foreign key (dir) references repventas(num_empl)
on delete set null --si un rep desapareix, la oficina quedara temporalmente vacia
on update cascade -- si un rep canvia de codi, canviara tambe el codi a la oficina

-- 8.7- Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que afegeixi un camp anomenat "nif" a la taula "clientes" que permeti emmagatzemar el NIF de cada client. 
--També s'ha de procurar que el NIF de cada client sigui únic.
ALTER TABLE copia_clientes ADD nif varchar(9) unique not null;
ALTER TABLE copia_clientes ADD nif varchar(9) unique not null check (nif like '_________');



-- 8.8- Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que afegeixi un camp anomenat "tel" a la taula "clientes" que permeti emmagatzemar
-- el número de telèfon de cada client. També s'ha de procurar que aquest contingui 9 xifres.
ALTER TABLE copia_clientes ADD tel numeric(9,0) unique not null check (tel >=100000000 and tel<=999999999);


-- 8.9- Escriu les sentències necessàries per modificar la base de dades training proporcionada. Cal que s'impedeixi que els noms dels representants de vendes i els noms dels clients estiguin buits,
-- és a dir que ni siguin nuls ni continguin la cadena buida.
ALTER TABLE copia_clientes ALTER CONSTRAINT empresa not null;
ALTER TABLE copia_repventas ALTER CONSTRAINT nombre not null;

alter table repventas add nombre set not null add check(nombre<>'');
alter table clientes add nombre set not null add check(empresa<>'');


-- 8.10- Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que procuri que l'edat dels representants de vendes no sigui inferior a 18 anys ni superior a 65.
ALTER TABLE copia_repventas ADD CHECK(edad>=18 and edad<=65);


-- 8.11- Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que esborri el camp "titulo" de la taula "repventas" 
--esborrant també les possibles restriccions i referències que tingui.
ALTER TABLE copia_repventas DROP titulo;


-- 8.12- Escriu les sentències necessàries per modificar la base de dades training proporcionada per tal que aquesta tingui integritat referencial. Justifica les accions a realitzar per modificar les dades.

alter table oficinas 
	add constraint director_de_la_oficina 
	foreign key (dir) references repventas(num_empl);

alter table pedidos 
	add constraint cliente_que_ha_hecho_pedido
	foreign (clie) references clientes(num_clie)
	add constraint rep_que_atendido_pedido
	foreign key (rep) references repventas(num_empl)
	add constraint producto_del_pedido
	foreign key (fab,producto) references productos(id_fab,id_producto);
	
alter table repventas
	add constraint oficina_donde_trabaja_rep
	foreign key (director) references repventas(num_empl)
	add constraint director_del_rep
	foreign key (director) references repventas(num_empl);

alter table clientes
	add constraint rep_del_cliente
	foreign key (rep_clie) references repventas(num_empl)
	
alter table oficinas
		add constraint director_oficina
		foreign key(dir) references repventas(num_empl);



