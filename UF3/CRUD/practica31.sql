PRACTICA 31

create database museus_mon;

create table museus(
	Id_museu serial primary key,
	Nom_museu varchar(50) not null,
	Address varchar(50) unique not null,
	Ciutat varchar(50) not null,
	Inauguracio date not null
)

create table sales(
	Id_sala serial primary key,
	Nom_sala varchar(50) unique not null,
	Id_museu smallint not null,
	foreign key id_museu references museus(Id_museu)
);

