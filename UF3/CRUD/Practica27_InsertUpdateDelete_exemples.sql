CRUD : CREATE + READ + UPDATE + DELETE

1) fer còpia de les taules

create table new_table
as 
select t1.col1, t2.col2
from some_table t1
   join t2 on t1.id = t2.some_id;

CREATE TABLE AS SELECT    rating,    COUNT (film_id) FROM     film  GROUP BY  rating;   

SELECT
    column_list 
INTO [ TEMPORARY | TEMP | UNLOGGED ] [ TABLE ] new_table_name
FROM
    table_name
WHERE
    condition;  

SELECT     film_id, title, rental_rate INTO TABLE film_r FROM  film WHERE  rating = 'R' AND rental_duration = 5 ORDER BY  title;	
   
2) treballar amb taules còpia

3) INSERT
--para agregar nuevas filas en una tabla
INSERT INTO table_name
       [ ( column_name [, ...] ) ]
       VALUES ( value [, ...] )
	   
INSERT INTO films (code, title, did, date_prod, kind) VALUES
    ('B6717', 'Tampopo', 110, '1985-02-10', 'Comedy'),
    ('HG120', 'The Dinner Game', 140, DEFAULT, 'Comedy');   
	
	
4) UPDATE
UPDATE [ ONLY ] table [ * ] [ [ AS ] alias ]
    SET { column = { expression | DEFAULT } |
          ( column [, ...] ) = ( { expression | DEFAULT } [, ...] ) } [, ...]
    [ FROM from_list ]
    [ WHERE condition | WHERE CURRENT OF cursor_name ]
    [ RETURNING * | output_expression [ [ AS ] output_name ] [, ...] ]

--serveix per modificar registres
UPDATE films SET kind = 'Dramatic' WHERE kind = 'Drama';
--donde pone drama, pone dramatic

5) DELETE
--borra filas o tablas
DELETE FROM [ ONLY ] table_name [ * ] [ [ AS ] alias ]
    [ USING using_list ]
    [ WHERE condition | WHERE CURRENT OF cursor_name ]
    [ RETURNING * | output_expression [ [ AS ] output_name ] [, ...] ]
	
DELETE FROM films WHERE kind <> 'Musical';

-- 6.1-  Inserir un nou venedor amb nom "Enric Jimenez" amb identificador 1012, oficina 18, títol "Dir Ventas", contracte d'1 de febrer del 2012, director 101 i vendes 0.

INSERT INTO repventas 
(num_empl, nombre, oficina_rep, titulo, contrato, director, ventas)
VALUES (1012, 'Enric Jimenez', 18, 'Dir Ventas', '2012-02-01', 101, 0);

INSERT INTO repventas
 (num_empl, nombre, edad, oficina_rep, titulo, contrato, director, cuota, ventas)
VALUES (1013, 'Miguel Amoros', 18, 'Dir Ventas', '2012-02-01', 101, 0);

INSERT INTO repventas
VALUES (1014, 'Diego Sanchez', NULL, 18, 'Dir Ventas', '2012-02-01', 101, NULL, 0);


-- 6.6- Esborrar de la còpia de la base de dades el venedor afegit anteriorment anomenat "Enric Jimenez".

DELETE FROM repventas
WHERE nombre = 'Enric Jimenez';
   
-- 6.7- Eliminar totes les comandes del client "C1" afegit anteriorment.

DELETE FROM pedidos
WHERE clie = (SELECT num_clie FROM clientes WHERE empresa = 'C1');


-- 6.17- Transferir tots els venedors
-- de l'oficina de Chicago (12) a la de Nova York (11),
-- i rebaixa les seves quotes un 10%.

UPDATE repventas
SET oficina_rep = 11,
    cuota = cuota - 0.1 * cuota
WHERE oficina_rep = 12;

-- 6.18- Reassigna tots els clients atesos pels empleats 105, 106, 107,
-- a l'emleat 102.

UPDATE clientes 
SET rep_clie = 102
WHERE rep_clie IN (105, 106, 107);


