PRÀCTICA 13. WHERE + GROUP BY + JOIN + UNION

1. Visualitzar tota la informació de cada client demanat,la quantitat de comanes que ha fet, 
la informació de les compres realitzades 
per cada client i el total d''import comprat per cada client. 
Si un client no ha comprat mai no s''han de mostrar els seus totals. 
La informació ha de sortir ordenada per codi de client. Els clients que es demanen són :

Clients amb un nom acabat amb 'International' o amb un nom acabat amb 'Corp.' o amb un nom
 que no comenci ni en 'A' ni en 'B' ni en 'C' ni en 'D' ni en 'O' ni en 'P' ni en 'J'.


1 parte:
training=# select 'info cliente', num_clie, empresa, rep_clie, limite_credito, count(clie) from clientes left join pedidos on num_clie=clie where (empresa ilike '%internacional') or (empresa ilike '%corp') or (empresa not like 'A%' and empresa not like 'B%' and empresa not like 'C%' and empresa not like 'D%' and empresa not like 'O%' and empresa not like 'P%' and empresa not like 'J%') group by 2;
   ?column?   | num_clie |     empresa      | rep_clie | limite_credito | count 
--------------+----------+------------------+----------+----------------+-------
 info cliente |     2102 | First Corp.      |      101 |       65000.00 |     1
 info cliente |     2119 | Solomon Inc.     |      109 |       25000.00 |     0
 info cliente |     2112 | Zetacorp         |      108 |       50000.00 |     2
 info cliente |     2122 | Three-Way Lines  |      105 |       30000.00 |     0
 info cliente |     2121 | QMA Assoc.       |      103 |       45000.00 |     0
 info cliente |     2118 | Midwest Systems  |      108 |       60000.00 |     4
 info cliente |     2115 | Smithson Corp.   |      101 |       20000.00 |     0
 info cliente |     2120 | Rico Enterprises |      102 |       50000.00 |     1
 info cliente |     2106 | Fred Lewis Corp. |      102 |       65000.00 |     2
 info cliente |     2114 | Orion Corp       |      102 |       20000.00 |     2
 info cliente |     2108 | Holm & Landis    |      109 |       55000.00 |     3
 info cliente |     2113 | Ian & Schmidt    |      104 |       20000.00 |     1
(12 rows)

training=# select 'info cliente', num_clie, empresa, rep_clie, limite_credito, count(clie), null, null, null, null, null, null from clientes left join pedidos on num_clie=clie where (empresa ilike '%internacional') or (empresa ilike '%corp') or (empresa not like 'A%' and empresa not like 'B%' and empresa not like 'C%' and empresa not like 'D%' and empresa not like 'O%' and empresa not like 'P%' and empresa not like 'J%') group by 2;
   ?column?   | num_clie |     empresa      | rep_clie | limite_credito | count | ?column? | ?column? | ?column? | ?column? | ?column? | ?column? 
--------------+----------+------------------+----------+----------------+-------+----------+----------+----------+----------+----------+----------
 info cliente |     2102 | First Corp.      |      101 |       65000.00 |     1 |          |          |          |          |          | 
 info cliente |     2119 | Solomon Inc.     |      109 |       25000.00 |     0 |          |          |          |          |          | 
 info cliente |     2112 | Zetacorp         |      108 |       50000.00 |     2 |          |          |          |          |          | 
 info cliente |     2122 | Three-Way Lines  |      105 |       30000.00 |     0 |          |          |          |          |          | 
 info cliente |     2121 | QMA Assoc.       |      103 |       45000.00 |     0 |          |          |          |          |          | 
 info cliente |     2118 | Midwest Systems  |      108 |       60000.00 |     4 |          |          |          |          |          | 
 info cliente |     2115 | Smithson Corp.   |      101 |       20000.00 |     0 |          |          |          |          |          | 
 info cliente |     2120 | Rico Enterprises |      102 |       50000.00 |     1 |          |          |          |          |          | 
 info cliente |     2106 | Fred Lewis Corp. |      102 |       65000.00 |     2 |          |          |          |          |          | 
 info cliente |     2114 | Orion Corp       |      102 |       20000.00 |     2 |          |          |          |          |          | 
 info cliente |     2108 | Holm & Landis    |      109 |       55000.00 |     3 |          |          |          |          |          | 
 info cliente |     2113 | Ian & Schmidt    |      104 |       20000.00 |     1 |          |          |          |          |          | 
(12 rows)



2 parte:
training=# select 'info pedido', pedidos.* from pedidos left join clientes on num_clie=clie where (empresa ilike '%internacional') or (empresa ilike '%corp') or (empresa not like 'A%' and empresa not like 'B%' and empresa not like 'C%' and empresa not like 'D%' and empresa not like 'O%' and empresa not like 'P%' and empresa not like 'J%') group by 2 order by clie;
  ?column?   | num_pedido | fecha_pedido | clie | rep | fab | producto | cant | importe  
-------------+------------+--------------+------+-----+-----+----------+------+----------
 info pedido |     112968 | 1989-10-12   | 2102 | 101 | aci | 41004    |   34 |  3978.00
 info pedido |     112993 | 1989-01-04   | 2106 | 102 | rei | 2a45c    |   24 |  1896.00
 info pedido |     113065 | 1990-02-27   | 2106 | 102 | qsa | xk47     |    6 |  2130.00
 info pedido |     113055 | 1990-02-15   | 2108 | 101 | aci | 4100x    |    6 |   150.00
 info pedido |     113003 | 1990-01-25   | 2108 | 109 | imm | 779c     |    3 |  5625.00
 info pedido |     113058 | 1990-02-23   | 2108 | 109 | fea | 112      |   10 |  1480.00
 info pedido |     113007 | 1990-01-08   | 2112 | 108 | imm | 773c     |    3 |  2925.00
 info pedido |     113045 | 1990-02-02   | 2112 | 108 | rei | 2a44r    |   10 | 45000.00
 info pedido |     113042 | 1990-02-02   | 2113 | 101 | rei | 2a44r    |    5 | 22500.00
 info pedido |     113024 | 1990-01-20   | 2114 | 108 | qsa | xk47     |   20 |  7100.00
 info pedido |     112979 | 1989-10-12   | 2114 | 102 | aci | 4100z    |    6 | 15000.00
 info pedido |     112992 | 1989-11-04   | 2118 | 108 | aci | 41002    |   10 |   760.00
 info pedido |     113013 | 1990-01-14   | 2118 | 108 | bic | 41003    |    1 |   652.00
 info pedido |     113049 | 1990-02-10   | 2118 | 108 | qsa | xk47     |    2 |   776.00
 info pedido |     113051 | 1990-02-10   | 2118 | 108 | qsa | k47      |    4 |  1420.00
 info pedido |     113048 | 1990-02-10   | 2120 | 102 | imm | 779c     |    2 |  3750.00
(16 rows)

training=# select 'info pedido', clie, null, rep, null, null, num_pedido, fecha_pedido, fab, producto, cant, importe from pedidos left join clientes on num_clie=clie where (empresa ilike '%internacional') or (empresa ilike '%corp') or (empresa not like 'A%' and empresa not like 'B%' and empresa not like 'C%' and empresa not like 'D%' and empresa not like 'O%' and empresa not like 'P%' and empresa not like 'J%') group by num_pedido;
  ?column?   | clie | ?column? | rep | ?column? | ?column? | num_pedido | fecha_pedido | fab | producto | cant | importe  
-------------+------+----------+-----+----------+----------+------------+--------------+-----+----------+------+----------
 info pedido | 2106 |          | 102 |          |          |     112993 | 1989-01-04   | rei | 2a45c    |   24 |  1896.00
 info pedido | 2113 |          | 101 |          |          |     113042 | 1990-02-02   | rei | 2a44r    |    5 | 22500.00
 info pedido | 2114 |          | 102 |          |          |     112979 | 1989-10-12   | aci | 4100z    |    6 | 15000.00
 info pedido | 2108 |          | 101 |          |          |     113055 | 1990-02-15   | aci | 4100x    |    6 |   150.00
 info pedido | 2118 |          | 108 |          |          |     113051 | 1990-02-10   | qsa | k47      |    4 |  1420.00
 info pedido | 2120 |          | 102 |          |          |     113048 | 1990-02-10   | imm | 779c     |    2 |  3750.00
 info pedido | 2102 |          | 101 |          |          |     112968 | 1989-10-12   | aci | 41004    |   34 |  3978.00
 info pedido | 2118 |          | 108 |          |          |     112992 | 1989-11-04   | aci | 41002    |   10 |   760.00
 info pedido | 2106 |          | 102 |          |          |     113065 | 1990-02-27   | qsa | xk47     |    6 |  2130.00
 info pedido | 2112 |          | 108 |          |          |     113007 | 1990-01-08   | imm | 773c     |    3 |  2925.00
 info pedido | 2112 |          | 108 |          |          |     113045 | 1990-02-02   | rei | 2a44r    |   10 | 45000.00
 info pedido | 2108 |          | 109 |          |          |     113058 | 1990-02-23   | fea | 112      |   10 |  1480.00
 info pedido | 2114 |          | 108 |          |          |     113024 | 1990-01-20   | qsa | xk47     |   20 |  7100.00
 info pedido | 2118 |          | 108 |          |          |     113049 | 1990-02-10   | qsa | xk47     |    2 |   776.00
 info pedido | 2108 |          | 109 |          |          |     113003 | 1990-01-25   | imm | 779c     |    3 |  5625.00
 info pedido | 2118 |          | 108 |          |          |     113013 | 1990-01-14   | bic | 41003    |    1 |   652.00
(16 rows)



3 parte:
training=# select 'totales', clie, sum(importe) from pedidos left join clientes on num_clie=clie where (empresa ilike '%internacional') or (empresa ilike '%corp') or (empresa not like 'A%' and empresa not like 'B%' and empresa not like 'C%' and empresa not like 'D%' and empresa not like 'O%' and empresa not like 'P%' and empresa not like 'J%') group by 2;
 ?column? | clie |   sum    
----------+------+----------
 totales  | 2102 |  3978.00
 totales  | 2112 | 47925.00
 totales  | 2118 |  3608.00
 totales  | 2108 |  7255.00
 totales  | 2114 | 22100.00
 totales  | 2120 |  3750.00
 totales  | 2106 |  4026.00
 totales  | 2113 | 22500.00
(8 rows)

training=# select 'totales', clie, null, null, null, null, null, null, null, null, null, sum(importe) from pedidos left join clientes on num_clie=clie where (empresa ilike '%internacional') or (empresa ilike '%corp') or (empresa not like 'A%' and empresa not like 'B%' and empresa not like 'C%' and empresa not like 'D%' and empresa not like 'O%' and empresa not like 'P%' and empresa not like 'J%') group by 2;
 ?column? | clie | ?column? | ?column? | ?column? | ?column? | ?column? | ?column? | ?column? | ?column? | ?column? |   sum    
----------+------+----------+----------+----------+----------+----------+----------+----------+----------+----------+----------
 totales  | 2102 |          |          |          |          |          |          |          |          |          |  3978.00
 totales  | 2112 |          |          |          |          |          |          |          |          |          | 47925.00
 totales  | 2118 |          |          |          |          |          |          |          |          |          |  3608.00
 totales  | 2108 |          |          |          |          |          |          |          |          |          |  7255.00
 totales  | 2114 |          |          |          |          |          |          |          |          |          | 22100.00
 totales  | 2120 |          |          |          |          |          |          |          |          |          |  3750.00
 totales  | 2106 |          |          |          |          |          |          |          |          |          |  4026.00
 totales  | 2113 |          |          |          |          |          |          |          |          |          | 22500.00
(8 rows)

4 parte:
training=# select 'info cliente', num_clie, empresa, rep_clie, limite_credito, count(clie), null, null, null, null, null, null from clientes left join pedidos on num_clie=clie where (empresa ilike '%internacional') or (empresa ilike '%corp') or (empresa not like 'A%' and empresa not like 'B%' and empresa not like 'C%' and empresa not like 'D%' and empresa not like 'O%' and empresa not like 'P%' and empresa not like 'J%') group by 2
training-# union
training-# select 'info pedido', clie, null, rep, null, null, num_pedido, fecha_pedido, fab, producto, cant, importe from pedidos left join clientes on num_clie=clie where (empresa ilike '%internacional') or (empresa ilike '%corp') or (empresa not like 'A%' and empresa not like 'B%' and empresa not like 'C%' and empresa not like 'D%' and empresa not like 'O%' and empresa not like 'P%' and empresa not like 'J%') group by num_pedido
training-# union
training-# select 'totales', clie, null, null, null, null, null, null, null, null, null, sum(importe) from pedidos left join clientes on num_clie=clie where (empresa ilike '%internacional') or (empresa ilike '%corp') or (empresa not like 'A%' and empresa not like 'B%' and empresa not like 'C%' and empresa not like 'D%' and empresa not like 'O%' and empresa not like 'P%' and empresa not like 'J%') group by 2 order by 2, 1;
   ?column?   | num_clie |     empresa      | rep_clie | limite_credito | count | ?column? |  ?column?  | ?column? | ?column? | ?column? | ?column? 
--------------+----------+------------------+----------+----------------+-------+----------+------------+----------+----------+----------+----------
 info cliente |     2102 | First Corp.      |      101 |       65000.00 |     1 |          |            |          |          |          |         
 info pedido  |     2102 |                  |      101 |                |       |   112968 | 1989-10-12 | aci      | 41004    |       34 |  3978.00
 totales      |     2102 |                  |          |                |       |          |            |          |          |          |  3978.00
 info cliente |     2106 | Fred Lewis Corp. |      102 |       65000.00 |     2 |          |            |          |          |          |         
 info pedido  |     2106 |                  |      102 |                |       |   113065 | 1990-02-27 | qsa      | xk47     |        6 |  2130.00
 info pedido  |     2106 |                  |      102 |                |       |   112993 | 1989-01-04 | rei      | 2a45c    |       24 |  1896.00
 totales      |     2106 |                  |          |                |       |          |            |          |          |          |  4026.00
 info cliente |     2108 | Holm & Landis    |      109 |       55000.00 |     3 |          |            |          |          |          |         
 info pedido  |     2108 |                  |      109 |                |       |   113003 | 1990-01-25 | imm      | 779c     |        3 |  5625.00
 info pedido  |     2108 |                  |      109 |                |       |   113058 | 1990-02-23 | fea      | 112      |       10 |  1480.00
 info pedido  |     2108 |                  |      101 |                |       |   113055 | 1990-02-15 | aci      | 4100x    |        6 |   150.00
 totales      |     2108 |                  |          |                |       |          |            |          |          |          |  7255.00
 info cliente |     2112 | Zetacorp         |      108 |       50000.00 |     2 |          |            |          |          |          |         
 info pedido  |     2112 |                  |      108 |                |       |   113045 | 1990-02-02 | rei      | 2a44r    |       10 | 45000.00
 info pedido  |     2112 |                  |      108 |                |       |   113007 | 1990-01-08 | imm      | 773c     |        3 |  2925.00
 totales      |     2112 |                  |          |                |       |          |            |          |          |          | 47925.00
 info cliente |     2113 | Ian & Schmidt    |      104 |       20000.00 |     1 |          |            |          |          |          |         
 info pedido  |     2113 |                  |      101 |                |       |   113042 | 1990-02-02 | rei      | 2a44r    |        5 | 22500.00
 totales      |     2113 |                  |          |                |       |          |            |          |          |          | 22500.00
 info cliente |     2114 | Orion Corp       |      102 |       20000.00 |     2 |          |            |          |          |          |         
 info pedido  |     2114 |                  |      102 |                |       |   112979 | 1989-10-12 | aci      | 4100z    |        6 | 15000.00
 info pedido  |     2114 |                  |      108 |                |       |   113024 | 1990-01-20 | qsa      | xk47     |       20 |  7100.00
 totales      |     2114 |                  |          |                |       |          |            |          |          |          | 22100.00
 info cliente |     2115 | Smithson Corp.   |      101 |       20000.00 |     0 |          |            |          |          |          |         
 info cliente |     2118 | Midwest Systems  |      108 |       60000.00 |     4 |          |            |          |          |          |         
 info pedido  |     2118 |                  |      108 |                |       |   112992 | 1989-11-04 | aci      | 41002    |       10 |   760.00
 info pedido  |     2118 |                  |      108 |                |       |   113049 | 1990-02-10 | qsa      | xk47     |        2 |   776.00
 info pedido  |     2118 |                  |      108 |                |       |   113051 | 1990-02-10 | qsa      | k47      |        4 |  1420.00
 info pedido  |     2118 |                  |      108 |                |       |   113013 | 1990-01-14 | bic      | 41003    |        1 |   652.00
 totales      |     2118 |                  |          |                |       |          |            |          |          |          |  3608.00
 info cliente |     2119 | Solomon Inc.     |      109 |       25000.00 |     0 |          |            |          |          |          |         
 info cliente |     2120 | Rico Enterprises |      102 |       50000.00 |     1 |          |            |          |          |          |         
 info pedido  |     2120 |                  |      102 |                |       |   113048 | 1990-02-10 | imm      | 779c     |        2 |  3750.00
 totales      |     2120 |                  |          |                |       |          |            |          |          |          |  3750.00
 info cliente |     2121 | QMA Assoc.       |      103 |       45000.00 |     0 |          |            |          |          |          |         
 info cliente |     2122 | Three-Way Lines  |      105 |       30000.00 |     0 |          |            |          |          |          |         
(36 rows)




2. Visualitzar tota la informació de cada producte demanat i la quantitat de comandes fetes per aquell producte,
 la informació de les ventes fetes de cada producte i el total de l''import venut de cada producte.
 Si un producte no s'ha venut també ha de sortir. 
 La informació ha d'estar ordenada per codi de fàbrica i codi de producte.
 Els productes que es demanen són : 

Productes de les fàbriques rei, aci i qsa amb preus entre 75 i 200.

1 parte:
training=# select 'info producto', id_fab, id_producto, descripcion, precio, existencias, count(num_pedido) from productos left join pedidos on id_fab=fab and id_producto=producto where (id_fab='rei' or id_fab='aci' or id_fab='qsa') and precio>=75 and precio<=200group by 2,3;
   ?column?    | id_fab | id_producto |    descripcion    | precio | existencias | count 
---------------+--------+-------------+-------------------+--------+-------------+-------
 info producto | aci    | 41002       | Articulo Tipo 2   |  76.00 |         167 |     2
 info producto | aci    | 41003       | Articulo Tipo 3   | 107.00 |         207 |     1
 info producto | aci    | 41004       | Articulo Tipo 4   | 117.00 |         139 |     3
 info producto | qsa    | xk48        | Reductor          | 134.00 |         203 |     0
 info producto | qsa    | xk48a       | Reductor          | 117.00 |          37 |     0
 info producto | rei    | 2a45c       | V Stago Trinquete |  79.00 |         210 |     2
(6 rows)

training=# select 'info producto', id_fab, id_producto, descripcion, precio, existencias, count(num_pedido), null, null, null, null, null, sum(importe) from productos left join pedidos on id_fab=fab and id_producto=producto where (id_fab='rei' or id_fab='aci' or id_fab='qsa') and precio>=75 and precio<=200 group by 2,3;
   ?column?    | id_fab | id_producto |    descripcion    | precio | existencias | count | ?column? | ?column? | ?column? | ?column? | ?column? | ?column? 
---------------+--------+-------------+-------------------+--------+-------------+-------+----------+----------+----------+----------+----------+----------
 info producto | aci    | 41002       | Articulo Tipo 2   |  76.00 |         167 |     2 |          |          |          |          |          | 
 info producto | aci    | 41003       | Articulo Tipo 3   | 107.00 |         207 |     1 |          |          |          |          |          | 
 info producto | aci    | 41004       | Articulo Tipo 4   | 117.00 |         139 |     3 |          |          |          |          |          | 
 info producto | qsa    | xk48        | Reductor          | 134.00 |         203 |     0 |          |          |          |          |          | 
 info producto | qsa    | xk48a       | Reductor          | 117.00 |          37 |     0 |          |          |          |          |          | 
 info producto | rei    | 2a45c       | V Stago Trinquete |  79.00 |         210 |     2 |          |          |          |          |          | 
(6 rows)


2 parte:
training=# select 'info pedido', fab, producto, null, null, null, null, num_pedido, fecha_pedido, clie, rep, cant, importe from pedidos left join productos on id_fab=fab and id_producto=producto where (id_fab='rei' or id_fab='aci' or id_fab='qsa') and precio>=75 and precio<=200 group by num_pedido;
  ?column?   | fab | producto | ?column? | ?column? | ?column? | ?column? | num_pedido | fecha_pedido | clie | rep | cant | importe 
-------------+-----+----------+----------+----------+----------+----------+------------+--------------+------+-----+------+---------
 info pedido | aci | 41004    |          |          |          |          |     112963 | 1989-12-17   | 2103 | 105 |   28 | 3276.00
 info pedido | aci | 41004    |          |          |          |          |     112968 | 1989-10-12   | 2102 | 101 |   34 | 3978.00
 info pedido | aci | 41004    |          |          |          |          |     112983 | 1989-12-27   | 2103 | 105 |    6 |  702.00
 info pedido | aci | 41002    |          |          |          |          |     112992 | 1989-11-04   | 2118 | 108 |   10 |  760.00
 info pedido | rei | 2a45c    |          |          |          |          |     112993 | 1989-01-04   | 2106 | 102 |   24 | 1896.00
 info pedido | aci | 41003    |          |          |          |          |     113012 | 1990-01-11   | 2111 | 105 |   35 | 3745.00
 info pedido | aci | 41002    |          |          |          |          |     113027 | 1990-01-22   | 2103 | 105 |   54 | 4104.00
 info pedido | rei | 2a45c    |          |          |          |          |     113034 | 1990-01-29   | 2107 | 110 |    8 |  632.00
(8 rows)

3 parte:
training=# select 'info producto', id_fab, id_producto, descripcion, precio, existencias, count(num_pedido), null, null, null, null, null, sum(importe) from productos left join pedidos on id_fab=fab and id_producto=producto where (id_fab='rei' or id_fab='aci' or id_fab='qsa') and precio>=75 and precio<=200 group by 2,3 union select 'info pedido', fab, producto, null, null, null, null, num_pedido, fecha_pedido, clie, rep, cant, importe from pedidos left join productos on id_fab=fab and id_producto=producto where (id_fab='rei' or id_fab='aci' or id_fab='qsa') and precio>=75 and precio<=200 group by num_pedido order by 2,3,1;
   ?column?    | id_fab | id_producto |    descripcion    | precio | existencias | count | ?column? |  ?column?  | ?column? | ?column? | ?column? |   sum   
---------------+--------+-------------+-------------------+--------+-------------+-------+----------+------------+----------+----------+----------+---------
 info pedido   | aci    | 41002       |                   |        |             |       |   113027 | 1990-01-22 |     2103 |      105 |       54 | 4104.00
 info pedido   | aci    | 41002       |                   |        |             |       |   112992 | 1989-11-04 |     2118 |      108 |       10 |  760.00
 info producto | aci    | 41002       | Articulo Tipo 2   |  76.00 |         167 |     2 |          |            |          |          |          | 4864.00
 info pedido   | aci    | 41003       |                   |        |             |       |   113012 | 1990-01-11 |     2111 |      105 |       35 | 3745.00
 info producto | aci    | 41003       | Articulo Tipo 3   | 107.00 |         207 |     1 |          |            |          |          |          | 3745.00
 info pedido   | aci    | 41004       |                   |        |             |       |   112963 | 1989-12-17 |     2103 |      105 |       28 | 3276.00
 info pedido   | aci    | 41004       |                   |        |             |       |   112968 | 1989-10-12 |     2102 |      101 |       34 | 3978.00
 info pedido   | aci    | 41004       |                   |        |             |       |   112983 | 1989-12-27 |     2103 |      105 |        6 |  702.00
 info producto | aci    | 41004       | Articulo Tipo 4   | 117.00 |         139 |     3 |          |            |          |          |          | 7956.00
 info producto | qsa    | xk48        | Reductor          | 134.00 |         203 |     0 |          |            |          |          |          |        
 info producto | qsa    | xk48a       | Reductor          | 117.00 |          37 |     0 |          |            |          |          |          |        
 info pedido   | rei    | 2a45c       |                   |        |             |       |   113034 | 1990-01-29 |     2107 |      110 |        8 |  632.00
 info pedido   | rei    | 2a45c       |                   |        |             |       |   112993 | 1989-01-04 |     2106 |      102 |       24 | 1896.00
 info producto | rei    | 2a45c       | V Stago Trinquete |  79.00 |         210 |     2 |          |            |          |          |          | 2528.00
(14 rows)


3. Visualitzar tota la informació de cada venedor demanat i la quantitat de comanes que ha fet;
 la informació de totes les ventes realitzades per cada venedor i el total d'import venut per cada venedor. 
 Si un venedor no ha venut res també s'ha de mostrar. La informació ha de sortir ordenada per codi de venedor.
 Els venedors que es demanen són :

Venedors majors de 32 anys, amb un nom començat per 'S' or per 'B' o per 'N' 

1 parte:
training=# select 'representant', num_empl, nombre, edad, oficina_rep, contrato, director, cuota, ventas, count(rep), sum(importe) from pedidos right join repventas on rep=num_empl where edad>32 and ( nombre like 'S%' or nombre like 'B%' or nombre like 'N%') group by 2;
   ?column?   | num_empl |    nombre     | edad | oficina_rep |  contrato  | director |   cuota   |  ventas   | count |   sum    
--------------+----------+---------------+------+-------------+------------+----------+-----------+-----------+-------+----------
 representant |      102 | Sue Smith     |   48 |          21 | 1986-12-10 |      108 | 350000.00 | 474050.00 |     4 | 22776.00
 representant |      104 | Bob Smith     |   33 |          12 | 1987-05-19 |      106 | 200000.00 | 142594.00 |     0 |         
 representant |      105 | Bill Adams    |   37 |          13 | 1988-02-12 |      104 | 350000.00 | 367911.00 |     5 | 39327.00
 representant |      106 | Sam Clark     |   52 |          11 | 1988-06-14 |          | 275000.00 | 299912.00 |     2 | 32958.00
 representant |      107 | Nancy Angelli |   49 |          22 | 1988-11-14 |      108 | 300000.00 | 186042.00 |     3 | 34432.00
(5 rows)


2 parte:
training=# select 'pedido', pedidos.* from pedidos left join repventas on rep=num_empl where edad>32 and ( nombre like 'S%' or nombre like 'B%' or nombre like 'N%');
 ?column? | num_pedido | fecha_pedido | clie | rep | fab | producto | cant | importe  
----------+------------+--------------+------+-----+-----+----------+------+----------
 pedido   |     112961 | 1989-12-17   | 2117 | 106 | rei | 2a44l    |    7 | 31500.00
 pedido   |     113012 | 1990-01-11   | 2111 | 105 | aci | 41003    |   35 |  3745.00
 pedido   |     112989 | 1990-01-03   | 2101 | 106 | fea | 114      |    6 |  1458.00
 pedido   |     112963 | 1989-12-17   | 2103 | 105 | aci | 41004    |   28 |  3276.00
 pedido   |     112997 | 1990-01-08   | 2124 | 107 | bic | 41003    |    1 |   652.00
 pedido   |     112983 | 1989-12-27   | 2103 | 105 | aci | 41004    |    6 |   702.00
 pedido   |     113062 | 1990-02-24   | 2124 | 107 | fea | 114      |   10 |  2430.00
 pedido   |     112979 | 1989-10-12   | 2114 | 102 | aci | 4100z    |    6 | 15000.00
 pedido   |     113027 | 1990-01-22   | 2103 | 105 | aci | 41002    |   54 |  4104.00
 pedido   |     113069 | 1990-03-02   | 2109 | 107 | imm | 775c     |   22 | 31350.00
 pedido   |     113048 | 1990-02-10   | 2120 | 102 | imm | 779c     |    2 |  3750.00
 pedido   |     112993 | 1989-01-04   | 2106 | 102 | rei | 2a45c    |   24 |  1896.00
 pedido   |     113065 | 1990-02-27   | 2106 | 102 | qsa | xk47     |    6 |  2130.00
 pedido   |     112987 | 1989-12-31   | 2103 | 105 | aci | 4100y    |   11 | 27500.00
(14 rows)

3 parte:
 select rep.*, count(ped.*) as num_ventas, sum(ped.importe) as tot_importe,
    null as num_pedido, null as fab, null as producto, null as clie, null as fecha_pedido, null as importe 
from repventas rep
left join pedidos ped on( rep.num_empl=ped.rep )
where rep.edad > 32 and rep.nombre ilike any(array['S%','B%','N%'])  
group by rep.num_empl
UNION
select rep.*, null , null, ped.num_pedido, ped.fab,ped.producto, ped.clie, ped.fecha_pedido, ped.importe 
from repventas rep
join pedidos ped on( rep.num_empl=ped.rep )
where rep.edad > 32 and rep.nombre ilike any(array['S%','B%','N%'])
order by 1,10 desc;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   | num_ventas | tot_importe | num_pedido | fab | producto | clie | fecha_pedido | importe  
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------+------------+-------------+------------+-----+----------+------+--------------+----------
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00 |            |             |     113065 | qsa | xk47     | 2106 | 1990-02-27   |  2130.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00 |            |             |     112993 | rei | 2a45c    | 2106 | 1989-01-04   |  1896.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00 |            |             |     113048 | imm | 779c     | 2120 | 1990-02-10   |  3750.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00 |            |             |     112979 | aci | 4100z    | 2114 | 1989-10-12   | 15000.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00 |          4 |    22776.00 |            |     |          |      |              |         
      104 | Bob Smith     |   33 |          12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00 |          0 |             |            |     |          |      |              |         
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00 |            |             |     112983 | aci | 41004    | 2103 | 1989-12-27   |   702.00
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00 |            |             |     112987 | aci | 4100y    | 2103 | 1989-12-31   | 27500.00
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00 |            |             |     113012 | aci | 41003    | 2111 | 1990-01-11   |  3745.00
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00 |            |             |     113027 | aci | 41002    | 2103 | 1990-01-22   |  4104.00
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00 |            |             |     112963 | aci | 41004    | 2103 | 1989-12-17   |  3276.00
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00 |          5 |    39327.00 |            |     |          |      |              |         
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00 |            |             |     112989 | fea | 114      | 2101 | 1990-01-03   |  1458.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00 |            |             |     112961 | rei | 2a44l    | 2117 | 1989-12-17   | 31500.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00 |          2 |    32958.00 |            |     |          |      |              |         
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00 |            |             |     113069 | imm | 775c     | 2109 | 1990-03-02   | 31350.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00 |            |             |     113062 | fea | 114      | 2124 | 1990-02-24   |  2430.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00 |            |             |     112997 | bic | 41003    | 2124 | 1990-01-08   |   652.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00 |          3 |    34432.00 |            |     |          |      |              |         
(19 filas)


4. Visualitzar tota la informació de cada oficina demanada i el número de venedors que té;
 la informació de tots els venedors assignats a la oficina i el número de cliens assignat a cada venedor
 i la informació de tots els clients assignats a cada venedor. 
 Si una oficina no té venedors també ha de sortir. 
 Si un venedor no té clients assignats ha de sortir igualment. 
 La informació ha de sortir ordenada per codi d'oficina i codi de venedor. 
 Les oficines que es demanen són:

Oficines de ciutats que continguin alguna 'a' o alguna 'e' i amb un objectiu més gran de 300.000 '

1 parte:
training=# select 'oficinia', oficinas.*, count(oficina_rep) from oficinas left join repventas on oficina=oficina_rep where (ciudad ilike '%a%' or ciudad ilike '%e%') and objetivo>300000 group by oficina;
 ?column? | oficina |   ciudad    | region | dir | objetivo  |  ventas   | count 
----------+---------+-------------+--------+-----+-----------+-----------+-------
 oficinia |      13 | Atlanta     | Este   | 105 | 350000.00 | 367911.00 |     1
 oficinia |      21 | Los Angeles | Oeste  | 108 | 725000.00 | 835915.00 |     2
 oficinia |      11 | New York    | Este   | 106 | 575000.00 | 692637.00 |     2
 oficinia |      12 | Chicago     | Este   | 104 | 800000.00 | 735042.00 |     3
(4 rows)


2 parte:
training=# select 'representante', repventas.*, count(rep_clie) from oficinas left join repventas on oficina=oficina_rep left join clientes on num_empl=rep_clie where (ciudad ilike '%a%' or ciudad ilike '%e%') and objetivo>300000 group by num_empl;
   ?column?    | num_empl |   nombre    | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   | count 
---------------+----------+-------------+------+-------------+------------+------------+----------+-----------+-----------+-------
 representante |      106 | Sam Clark   |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00 |     2
 representante |      101 | Dan Roberts |   45 |          12 | Rep Ventas | 1986-10-20 |      104 | 300000.00 | 305673.00 |     3
 representante |      109 | Mary Jones  |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00 |     2
 representante |      104 | Bob Smith   |   33 |          12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00 |     1
 representante |      102 | Sue Smith   |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00 |     4
 representante |      103 | Paul Cruz   |   29 |          12 | Rep Ventas | 1987-03-01 |      104 | 275000.00 | 286775.00 |     3
 representante |      105 | Bill Adams  |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00 |     2
 representante |      108 | Larry Fitch |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00 |     2
(8 rows)

3 parte:
training=# select 'cliente', clientes.* from clientes right join repventas on rep_clie=num_empl left join oficinas on oficina_rep=oficina where (ciudad ilike '%a%' or ciudad ilike '%e%') and objetivo>300000;
 ?column? | num_clie |     empresa      | rep_clie | limite_credito 
----------+----------+------------------+----------+----------------
 cliente  |     2111 | JCP Inc.         |      103 |       50000.00
 cliente  |     2102 | First Corp.      |      101 |       65000.00
 cliente  |     2103 | Acme Mfg.        |      105 |       50000.00
 cliente  |     2123 | Carter & Sons    |      102 |       40000.00
 cliente  |     2115 | Smithson Corp.   |      101 |       20000.00
 cliente  |     2101 | Jones Mfg.       |      106 |       65000.00
 cliente  |     2112 | Zetacorp         |      108 |       50000.00
 cliente  |     2121 | QMA Assoc.       |      103 |       45000.00
 cliente  |     2114 | Orion Corp       |      102 |       20000.00
 cliente  |     2108 | Holm & Landis    |      109 |       55000.00
 cliente  |     2117 | J.P. Sinclair    |      106 |       35000.00
 cliente  |     2122 | Three-Way Lines  |      105 |       30000.00
 cliente  |     2120 | Rico Enterprises |      102 |       50000.00
 cliente  |     2106 | Fred Lewis Corp. |      102 |       65000.00
 cliente  |     2119 | Solomon Inc.     |      109 |       25000.00
 cliente  |     2118 | Midwest Systems  |      108 |       60000.00
 cliente  |     2113 | Ian & Schmidt    |      104 |       20000.00
 cliente  |     2109 | Chen Associates  |      103 |       25000.00
 cliente  |     2105 | AAA Investments  |      101 |       45000.00
(19 rows)


