1- Visualitzar quants repventas tenim assignats a cada regió (este/oest), quants repventas tenim assignats a cada ciutat i quants clients tenim assignats a cadascun d’aquests repventas. 
També han de sortir els repventas que no estan assignats a cap zona.
Ordenar per zona, ciutat i nom de venedor.
El resultat de la consulta cal que surti així:

 Regió | RepventasPerRegio |   Ciutat    | RepeventassPerCiutat |    venedor    | ClientsPerRepventas 
-------+-------------------+-------------+----------------------+---------------+---------------------
  ...
  ...
 Oeste |                   | Denver      |                      | Nancy Angelli |                   1
 Oeste |                   | Denver      |                    1 |               |                    
 Oeste |                   | Los Angeles |                      | Larry Fitch   |                   2
 Oeste |                   | Los Angeles |                      | Sue Smith     |                   4
 Oeste |                   | Los Angeles |                    2 |               |                    
 Oeste |                 3 |             |                      |               |                   
  ...
  ...


1 parte:
training=# select region, count(oficina_rep) from oficinas right join repventas on oficina_rep=oficina group by region;
 region | count 
--------+-------
        |     0
 Este   |     6
 Oeste  |     3
(3 rows)

2 parte:
training=# select ciudad, count(oficina_rep) from oficinas right join repventas on oficina_rep=oficina group by ciudad;
   ciudad    | count 
-------------+-------
             |     0
 Denver      |     1
 Atlanta     |     1
 Los Angeles |     2
 New York    |     2
 Chicago     |     3
(6 rows)

3 parte:
training=# select num_empl, count(rep_clie) from repventas left join clientes on num_empl=rep_clie group by 1;
 num_empl | count 
----------+-------
      106 |     2
      101 |     3
      109 |     2
      104 |     1
      102 |     4
      107 |     1
      108 |     2
      110 |     1
      103 |     3
      105 |     2
(10 rows)

4 parte:
training=# select region, count(oficina_rep), null, null, null, 0 from oficinas right join repventas on oficina_rep=oficina group by region
union
select null, null, ciudad, count(oficina_rep), null, 0 from oficinas right join repventas on oficina_rep=oficina group by ciudad
union
select null, null, null, null, nombre, count(rep_clie) from repventas left join clientes on num_empl=rep_clie group by num_empl order by 1,3,5;
 region | count |  ?column?   | ?column? |   ?column?    | ?column? 
--------+-------+-------------+----------+---------------+----------
 Este   |     6 |             |          |               |        0
 Oeste  |     3 |             |          |               |        0
        |       | Atlanta     |        1 |               |        0
        |       | Chicago     |        3 |               |        0
        |       | Denver      |        1 |               |        0
        |       | Los Angeles |        2 |               |        0
        |       | New York    |        2 |               |        0
        |       |             |          | Bill Adams    |        2
        |       |             |          | Bob Smith     |        1
        |       |             |          | Dan Roberts   |        3
        |       |             |          | Larry Fitch   |        2
        |       |             |          | Mary Jones    |        2
        |       |             |          | Nancy Angelli |        1
        |       |             |          | Paul Cruz     |        3
        |       |             |          | Sam Clark     |        2
        |       |             |          | Sue Smith     |        4
        |       |             |          | Tom Snyder    |        1
        |     0 |             |          |               |        0
        |       |             |        0 |               |        0
(19 rows)



2-  Per cadascun dels nostres productes volem saber quantes unitats se n'han venut en total i quantes unitats n'ha venut cada ciutat. 
Els productes que no s'han venut també han de sortir. 
Ordenar per producte i ciutat.

El resultat de la consulta cal que surti així:'

 tipusregistre |     producte      |   ciutat    | numproductes 
---------------+-------------------+-------------+--------------
  ...
  ...
 -----         | Articulo Tipo 2   | Atlanta     |           54
 -----         | Articulo Tipo 2   | Los Angeles |           10
 Total         | Articulo Tipo 2   |             |           64
 -----         | Articulo Tipo 3   | Atlanta     |           35
 Total         | Articulo Tipo 3   |             |           35
  ...
  ...



3- Volem classificar els clients en 3 classes : -Classe A- per clients que ens han gastat més de 30.000,   -Classe B- per clients que ens han gastat més de 7.000 fins a  30.000 i -Classe C- per la resta. 
Visualitzar cadascun dels clients (nom empresa) amb las seva classe corresponent i l’import total gastat. Ordenar per classe i per import total.
El resultat de la consulta cal que surti així:

    classeclient    |      empresa      | TotalGastat 
--------------------+-------------------+-------------
  ...
  ...
 Client Classe A -- | Zetacorp          |    47925.00
 Client Classe A -- | Acme Mfg.         |    35582.00
 Client Classe A -- | J.P. Sinclair     |    31500.00
 Client Classe A -- | Chen Associates   |    31350.00
 Client Classe B -- | Ace International |    23132.00
 Client Classe B -- | Ian & Schmidt     |    22500.00
 Client Classe B -- | Orion Corp        |    22100.00
  ...
  ...










4- Visualitzar, per cada una de les ciutats, quants clients ens han comprat, quantes vegades ens ha comprat cadascun d'aquests clients (quants 'pedidos' ens han fet) i el nom d'aquests clients amb l'import total que han gastat. Ordenar per zona, núm de compres per client i import total per client.

El resultat de la consulta cal que surti així:'


   ciudat    | tipusreg | num_clients | client |        nom        | numcompres | totalimport 
-------------+----------+-------------+--------+-------------------+------------+-------------
  ...
  ...
 Atlanta     | -----    |             |   2103 | Acme Mfg.         |          4 |    35582.00
 Atlanta     | -----    |             |   2111 | JCP Inc.          |          1 |     3745.00
 Atlanta     | Total    |           2 |        |                   |            |            
 Chicago     | -----    |             |   2111 | JCP Inc.          |          2 |     2700.00
 Chicago     | -----    |             |   2113 | Ian & Schmidt     |          1 |    22500.00
 Chicago     | -----    |             |   2102 | First Corp.       |          1 |     3978.00
 Chicago     | -----    |             |   2108 | Holm & Landis     |          1 |      150.00
 Chicago     | Total    |           4 |        |                   |            |            
  ...
  ...
