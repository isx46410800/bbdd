    1. Mostar les ventes individuals dels productes dels fabricants 'aci' i 'rei' que comencin per 'Bisagra' o 'Articulo'. Mostrar també el total venut d'aquests productes.
1 paso:
training=# select 'producto individual', id_fab, id_producto, importe from productos join pedidos on id_fab=fab and id_producto=producto where descripcion ilike 'Bisagra%' or descripcion ilike 'Articulo%'; '
      ?column?       | id_fab | id_producto | importe  
---------------------+--------+-------------+----------
 producto individual | rei    | 2a44l       | 31500.00
 producto individual | aci    | 41003       |  3745.00
 producto individual | aci    | 41004       |  3978.00
 producto individual | rei    | 2a44r       | 45000.00
 producto individual | aci    | 41004       |  3276.00
 producto individual | aci    | 41004       |   702.00
 producto individual | aci    | 41002       |  4104.00
 producto individual | aci    | 41002       |   760.00
 producto individual | rei    | 2a44r       | 22500.00
(9 rows)

2 paso:
training=# select 'total', id_fab, id_producto, sum(importe) from productos join pedidos on id_fab=fab and id_producto=producto where descripcion ilike 'Bisagra%' or descripcion ilike 'Articulo%' group by id_fab, id_producto;
 ?column? | id_fab | id_producto |   sum    
----------+--------+-------------+----------
 total    | aci    | 41002       |  4864.00
 total    | aci    | 41003       |  3745.00
 total    | aci    | 41004       |  7956.00
 total    | rei    | 2a44l       | 31500.00
 total    | rei    | 2a44r       | 67500.00
(5 rows)

3 paso(union y orden y con descripcion):
training=# select 'producto individual', id_fab, id_producto, descripcion, importe from productos join pedidos on id_fab=fab and id_producto=producto where descripcion ilike 'Bisagra%' or descripcion ilike 'Articulo%' union select 'total', id_fab, id_producto, descripcion, sum(importe) from productos join pedidos on id_fab=fab and id_producto=producto where descripcion ilike 'Bisagra%' or descripcion ilike 'Articulo%' group by id_fab, id_producto, descripcion order by 2,3,1;
      ?column?       | id_fab | id_producto |   descripcion   | importe  
---------------------+--------+-------------+-----------------+----------
 producto individual | aci    | 41002       | Articulo Tipo 2 |  4104.00
 producto individual | aci    | 41002       | Articulo Tipo 2 |   760.00
 total               | aci    | 41002       | Articulo Tipo 2 |  4864.00
 producto individual | aci    | 41003       | Articulo Tipo 3 |  3745.00
 total               | aci    | 41003       | Articulo Tipo 3 |  3745.00
 producto individual | aci    | 41004       | Articulo Tipo 4 |  3978.00
 producto individual | aci    | 41004       | Articulo Tipo 4 |   702.00
 producto individual | aci    | 41004       | Articulo Tipo 4 |  3276.00
 total               | aci    | 41004       | Articulo Tipo 4 |  7956.00
 producto individual | rei    | 2a44l       | Bisagra Izqda.  | 31500.00
 total               | rei    | 2a44l       | Bisagra Izqda.  | 31500.00
 producto individual | rei    | 2a44r       | Bisagra Dcha.   | 22500.00
 producto individual | rei    | 2a44r       | Bisagra Dcha.   | 45000.00
 total               | rei    | 2a44r       | Bisagra Dcha.   | 67500.00
(14 rows)

	
    2. Mostrar les ventes individuals fetes pels venedors de New York i de Chicago que superin els 2500€. 
    Mostrar també el total de ventes de cada oficina.
    1 paso:
  training=# select 'individual', ciudad, nombre, importe from pedidos join repventas on rep=num_empl join oficinas on oficina_rep=oficina where (oficina_rep=11 or oficina_rep=12) and importe>2500;
  ?column?  |  ciudad  |   nombre    | importe  
------------+----------+-------------+----------
 individual | New York | Sam Clark   | 31500.00
 individual | Chicago  | Dan Roberts |  3978.00
 individual | New York | Mary Jones  |  5625.00
 individual | Chicago  | Dan Roberts | 22500.00
(4 rows)

	2 paso:
training=# select 'total', ciudad, '-------', sum(importe) from pedidos join repventas on rep=num_empl join oficinas on oficina_rep=oficina where (oficina_rep=11 or oficina_rep=12) and importe>2500 group by ciudad;
 ?column? |  ciudad  | ?column? |   sum    
----------+----------+----------+----------
 total    | Chicago  | -------  | 26478.00
 total    | New York | -------  | 37125.00
(2 rows)

#ponemos guiones en el segundo porque en el segundo paso no ponemos el nombre, porque ahii iria el nombre pero no lo agrupamos
por group by

	3 paso(union y order):
training=# select 'individual', ciudad, nombre, importe from pedidos join repventas on rep=num_empl join oficinas on oficina_rep=oficina where (oficina_rep=11 or oficina_rep=12) and importe>2500 union select 'total', ciudad, '-------', sum(importe) from pedidos join repventas on rep=num_empl join oficinas on oficina_rep=oficina where (oficina_rep=11 or oficina_rep=12) and importe>2500 group by ciudad order by 2,1;
  ?column?  |  ciudad  |   nombre    | importe  
------------+----------+-------------+----------
 individual | Chicago  | Dan Roberts |  3978.00
 individual | Chicago  | Dan Roberts | 22500.00
 total      | Chicago  | -------     | 26478.00
 individual | New York | Sam Clark   | 31500.00
 individual | New York | Mary Jones  |  5625.00
 total      | New York | -------     | 37125.00
(6 rows)
	

    
    
    3. Mostrar quantes ventes ha fet cada venedor, la mitjana de numero de ventes dels venedors de cada oficina 
    i el numero de ventes total.

    
    1 paso:
training=# select 'vendedor', num_empl, oficina_rep, count(num_pedido) from pedidos left join repventas on rep=num_empl group by num_empl;

?column?  | Empleado | Oficina | CALCUL 
-----------+----------+---------+--------
 Venedor:  |      106 |      11 |      2
 Venedor:  |      101 |      12 |      3
 Venedor:  |      109 |      11 |      2
 Venedor:  |      104 |      12 |      0
 Venedor:  |      102 |      21 |      4
 Venedor:  |      107 |      22 |      3
 Venedor:  |      108 |      21 |      7
 Venedor:  |      110 |         |      2
 Venedor:  |      103 |      12 |      2
 Venedor:  |      105 |      13 |      5
(10 rows)

2 paso:
training=# select 'Media Oficina', NULL, oficina_rep, count(num_pedido)/count(distinct num_empl) from repventas left join pedidos on rep=num_empl group by 3;
   ?column?    | ?column? | oficina_rep | ?column? 
---------------+----------+-------------+----------
 Media Oficina |          |          11 |        2
 Media Oficina |          |          12 |        1
 Media Oficina |          |          13 |        5
 Media Oficina |          |          21 |        5
 Media Oficina |          |          22 |        3
 Media Oficina |          |             |        2
(6 rows)


3 paso:
training=# select 'Total oficinas', NULL, oficina_rep, count(num_pedido) from repventas left join pedidos on rep=num_empl group by 3 order by 3, 1 desc;
    ?column?    | ?column? | oficina_rep | count 
----------------+----------+-------------+-------
 Total oficinas |          |          11 |     4
 Total oficinas |          |          12 |     5
 Total oficinas |          |          13 |     5
 Total oficinas |          |          21 |    11
 Total oficinas |          |          22 |     3
 Total oficinas |          |             |     2
(6 rows)

#se pone NULL en columnas con numeros que no salen en otros selects
# se pone ---- en columnas con strings que no salen en otros selects

4 paso:
training=# select 'vendedor', num_empl, oficina_rep, count(num_pedido) from pedidos left join repventas on rep=num_empl group by num_empl union select 'Media Oficina', NULL, oficina_rep, count(num_pedido)/count(distinct num_empl) from repventas left join pedidos on rep=num_empl group by 3 union select 'Total oficinas', NULL, oficina_rep, count(num_pedido) from repventas left join pedidos on rep=num_empl group by 3 order by 3, 1 desc;
    ?column?    | num_empl | oficina_rep | count 
----------------+----------+-------------+-------
 vendedor       |      109 |          11 |     2
 vendedor       |      106 |          11 |     2
 Total oficinas |          |          11 |     4
 Media Oficina  |          |          11 |     2
 vendedor       |      101 |          12 |     3
 vendedor       |      103 |          12 |     2
 Total oficinas |          |          12 |     5
 Media Oficina  |          |          12 |     1
 vendedor       |      105 |          13 |     5
 Total oficinas |          |          13 |     5
 Media Oficina  |          |          13 |     5
 vendedor       |      108 |          21 |     7
 vendedor       |      102 |          21 |     4
 Total oficinas |          |          21 |    11
 Media Oficina  |          |          21 |     5
 vendedor       |      107 |          22 |     3
 Total oficinas |          |          22 |     3
 Media Oficina  |          |          22 |     3
 vendedor       |      110 |             |     2
 Total oficinas |          |             |     2
 Media Oficina  |          |             |     2
(21 rows)

    
    4. Mostrar les compres de productes de la fabrica 'aci' que han fet els clients del Bill Adams i el Dan Roberts. Mostrar també l'import total per cada client.
'1 paso:
training=# select 'compra producto', num_pedido, rep, importe from pedidos join repventas on rep=num_empl where (rep=105 or rep=101) and fab='aci';
    ?column?     | num_pedido | rep | importe  
-----------------+------------+-----+----------
 compra producto |     113012 | 105 |  3745.00
 compra producto |     112968 | 101 |  3978.00
 compra producto |     112963 | 105 |  3276.00
 compra producto |     112983 | 105 |   702.00
 compra producto |     113027 | 105 |  4104.00
 compra producto |     113055 | 101 |   150.00
 compra producto |     112987 | 105 | 27500.00
(7 rows)'



'2 paso:
training=# select 'compra cliente', NULL, rep, sum(importe) from pedidos join repventas on rep=num_empl where (rep=105 or rep=101) and fab='aci' group by rep;
    ?column?    | ?column? | rep |   sum    
----------------+----------+-----+----------
 compra cliente |          | 101 |  4128.00
 compra cliente |          | 105 | 39327.00
(2 rows)'

'3 paso:
training=# select 'compra producto', num_pedido, rep, importe from pedidos join repventas on rep=num_empl where (rep=105 or rep=101) and fab='aci' union select 'compra cliente', NULL, rep, sum(importe) from pedidos join repventas on rep=num_empl where (rep=105 or rep=101) and fab='aci' group by rep;
    ?column?     | num_pedido | rep | importe  
-----------------+------------+-----+----------
 compra cliente  |            | 101 |  4128.00
 compra cliente  |            | 105 | 39327.00
 compra producto |     112963 | 105 |  3276.00
 compra producto |     112968 | 101 |  3978.00
 compra producto |     112983 | 105 |   702.00
 compra producto |     112987 | 105 | 27500.00
 compra producto |     113012 | 105 |  3745.00
 compra producto |     113027 | 105 |  4104.00
 compra producto |     113055 | 101 |   150.00
(9 rows)

--4. Mostrar les compres de productes de la fabrica 'aci' que han fet els clients del Bill Adams i el Dan Roberts. Mostrar també l'import total per cada client.


select 'compras', clie, producto, importe from pedidos join clientes on clie = num_clie
join repventas on rep = num_empl where fab = 'aci' and nombre in ('Bill Adams', 'Dan Roberts')
union
select 'total compras', clie, NULL, sum(importe) from pedidos  join clientes on clie = num_clie
join repventas on rep = num_empl where fab = 'aci' and nombre in ('Bill Adams', 'Dan Roberts')
group by clie order by 2 ,1;


   ?column?    | clie | producto | importe  
---------------+------+----------+----------
 compras       | 2102 | 41004    |  3978.00
 total compras | 2102 |          |  3978.00
 compras       | 2103 | 41004    |   702.00
 compras       | 2103 | 4100y    | 27500.00
 compras       | 2103 | 41004    |  3276.00
 compras       | 2103 | 41002    |  4104.00
 total compras | 2103 |          | 35582.00
 compras       | 2108 | 4100x    |   150.00
 total compras | 2108 |          |   150.00
 compras       | 2111 | 41003    |  3745.00
 total compras | 2111 |          |  3745.00
(11 rows)


    5. Mostrar el total de ventes de cada oficina i el total de ventes de cada regió
training=select 'oficina:',region, ciudad, sum(importe) from pedidos join repventas on rep = num_empl join oficinas on oficina = oficina_rep group by ciudad, region;
union
select 'region:', region, NULL, sum(importe) from pedidos join repventas on rep = num_empl join oficinas on oficina = oficina_rep group by region order by 2,1;
 ?column? | region |   ciudad    |    sum    
----------+--------+-------------+-----------
 oficina: | Este   | Chicago     |  29328.00
 oficina: | Este   | New York    |  40063.00
 oficina: | Este   | Atlanta     |  39327.00
 region:  | Este   |             | 108718.00
 oficina: | Oeste  | Los Angeles |  81409.00
 oficina: | Oeste  | Denver      |  34432.00
 region:  | Oeste  |             | 115841.00
(7 rows)

       
    6. Mostrar els noms dels venedors de cada ciutat i el numero total de venedors per ciutat
    
1 paso: #ponemos 0 porque luego nos pide otro campo
training=# select 'empleado', nombre, ciudad, NULL  from repventas left join oficinas on oficina_rep=oficina;
 ?column? |    nombre     |   ciudad    | ?column? 
----------+---------------+-------------+----------
 empleado | Bill Adams    | Atlanta     | 
 empleado | Mary Jones    | New York    | 
 empleado | Sue Smith     | Los Angeles | 
 empleado | Sam Clark     | New York    | 
 empleado | Bob Smith     | Chicago     | 
 empleado | Dan Roberts   | Chicago     | 
 empleado | Tom Snyder    |             | 
 empleado | Larry Fitch   | Los Angeles | 
 empleado | Paul Cruz     | Chicago     | 
 empleado | Nancy Angelli | Denver      | 
(10 rows)

2 paso:
training=# select 'numero empleados', NULL, ciudad, count(*)  from repventas left join oficinas on oficina_rep=oficina group by 3;
     ?column?     | ?column? |   ciudad    | count 
------------------+----------+-------------+-------
 numero empleados |          |             |     1
 numero empleados |          | Denver      |     1
 numero empleados |          | Atlanta     |     1
 numero empleados |          | Los Angeles |     2
 numero empleados |          | New York    |     2
 numero empleados |          | Chicago     |     3
(6 rows)
#contamos cuantos oficina_rep hay de cada oficina , no count oficina_rep porque hay un null

3 paso:
training=# select 'empleado', nombre, ciudad, NULL  from repventas left join oficinas on oficina_rep=oficina union  select 'numero empleados', NULL, ciudad, count(*)  from repventas left join oficinas on oficina_rep=oficina group by 3;
     ?column?     |    nombre     |   ciudad    | ?column? 
------------------+---------------+-------------+----------
 numero empleados |               | Atlanta     |        1
 empleado         | Mary Jones    | New York    |         
 empleado         | Sue Smith     | Los Angeles |         
 empleado         | Nancy Angelli | Denver      |         
 numero empleados |               |             |        1
 empleado         | Bob Smith     | Chicago     |         
 empleado         | Dan Roberts   | Chicago     |         
 empleado         | Larry Fitch   | Los Angeles |         
 numero empleados |               | Chicago     |        3
 empleado         | Paul Cruz     | Chicago     |         
 empleado         | Bill Adams    | Atlanta     |         
 numero empleados |               | Los Angeles |        2
 numero empleados |               | Denver      |        1
 empleado         | Sam Clark     | New York    |         
 numero empleados |               | New York    |        2
 empleado         | Tom Snyder    |             |         
(16 rows)



    7. Mostrat els noms dels clients de cada ciutat i el numero total de clients per ciutat.
1 paso:(ponemos un null porque habra otro campo)
 training=# select 'nombre cliente', empresa, ciudad, null from clientes join repventas on rep_clie=num_empl left join oficinas on oficina_rep=oficina;
    ?column?    |      empresa      |   ciudad    | ?column? 
----------------+-------------------+-------------+----------
 nombre cliente | JCP Inc.          | Chicago     | 
 nombre cliente | First Corp.       | Chicago     | 
 nombre cliente | Acme Mfg.         | Atlanta     | 
 nombre cliente | Carter & Sons     | Los Angeles | 
 nombre cliente | Ace International |             | 
 nombre cliente | Smithson Corp.    | Chicago     | 
 nombre cliente | Jones Mfg.        | New York    | 
 nombre cliente | Zetacorp          | Los Angeles | 
 nombre cliente | QMA Assoc.        | Chicago     | 
 nombre cliente | Orion Corp        | Los Angeles | 
 nombre cliente | Peter Brothers    | Denver      | 
 nombre cliente | Holm & Landis     | New York    | 
 nombre cliente | J.P. Sinclair     | New York    | 
 nombre cliente | Three-Way Lines   | Atlanta     | 
 nombre cliente | Rico Enterprises  | Los Angeles | 
 nombre cliente | Fred Lewis Corp.  | Los Angeles | 
 nombre cliente | Solomon Inc.      | New York    | 
 nombre cliente | Midwest Systems   | Los Angeles | 
 nombre cliente | Ian & Schmidt     | Chicago     | 
 nombre cliente | Chen Associates   | Chicago     | 
 nombre cliente | AAA Investments   | Chicago     | 
(21 rows)

2 paso:
training=# select 'total clientes', null, ciudad, count(*) from clientes join repventas on rep_clie=num_empl left join oficinas on oficina_rep=oficina group by ciudad;
    ?column?    | ?column? |   ciudad    | count 
----------------+----------+-------------+-------
 total clientes |          |             |     1
 total clientes |          | Denver      |     1
 total clientes |          | Atlanta     |     2
 total clientes |          | Los Angeles |     6
 total clientes |          | New York    |     4
 total clientes |          | Chicago     |     7
(6 rows)


3 paso(ordenado por ciudad y nombre de etiqueta)
training=# select 'nombre cliente', empresa, ciudad, null from clientes join repventas on rep_clie=num_empl left join oficinas on oficina_rep=oficina union select 'total clientes', null, ciudad, count(*) from clientes join repventas on rep_clie=num_empl left join oficinas on oficina_rep=oficina group by ciudad order by 3,1;
    ?column?    |      empresa      |   ciudad    | ?column? 
----------------+-------------------+-------------+----------
 nombre cliente | Three-Way Lines   | Atlanta     |         
 nombre cliente | Acme Mfg.         | Atlanta     |         
 total clientes |                   | Atlanta     |        2
 nombre cliente | JCP Inc.          | Chicago     |         
 nombre cliente | Smithson Corp.    | Chicago     |         
 nombre cliente | Chen Associates   | Chicago     |         
 nombre cliente | First Corp.       | Chicago     |         
 nombre cliente | AAA Investments   | Chicago     |         
 nombre cliente | QMA Assoc.        | Chicago     |         
 nombre cliente | Ian & Schmidt     | Chicago     |         
 total clientes |                   | Chicago     |        7
 nombre cliente | Peter Brothers    | Denver      |         
 total clientes |                   | Denver      |        1
 nombre cliente | Orion Corp        | Los Angeles |         
 nombre cliente | Rico Enterprises  | Los Angeles |         
 nombre cliente | Fred Lewis Corp.  | Los Angeles |         
 nombre cliente | Carter & Sons     | Los Angeles |         
 nombre cliente | Zetacorp          | Los Angeles |         
 nombre cliente | Midwest Systems   | Los Angeles |         
 total clientes |                   | Los Angeles |        6
 nombre cliente | Solomon Inc.      | New York    |         
 nombre cliente | J.P. Sinclair     | New York    |         
 nombre cliente | Holm & Landis     | New York    |         
 nombre cliente | Jones Mfg.        | New York    |         
 total clientes |                   | New York    |        4
 nombre cliente | Ace International |             |         
 total clientes |                   |             |        1
(27 rows)

    
    
    8. Mostrat els noms dels treballadors que son -caps- d'algú, els noms dels seus -subordinats- 
    i el numero de treballadors que té assignat cada cap.

'1 parte:

training=# select 'nombre treballador-cap', cap.nombre as "CAP", empleado.nombre as "SUBORDINADO", null from repventas as empleado left join repventas as cap on cap.num_empl=empleado.director;
        ?column?        |     CAP     |  SUBORDINADO  | ?column? 
------------------------+-------------+---------------+----------
 nombre treballador-cap | Bob Smith   | Bill Adams    | 
 nombre treballador-cap | Sam Clark   | Mary Jones    | 
 nombre treballador-cap | Larry Fitch | Sue Smith     | 
 nombre treballador-cap |             | Sam Clark     | 
 nombre treballador-cap | Sam Clark   | Bob Smith     | 
 nombre treballador-cap | Bob Smith   | Dan Roberts   | 
 nombre treballador-cap | Dan Roberts | Tom Snyder    | 
 nombre treballador-cap | Sam Clark   | Larry Fitch   | 
 nombre treballador-cap | Bob Smith   | Paul Cruz     | 
 nombre treballador-cap | Larry Fitch | Nancy Angelli | 
(10 rows)


2 parte:
training=# select 'treballador asignat', cap.nombre as "CAP", null, count(*) from repventas as empleado left join repventas as cap on cap.num_empl=empleado.director group by cap.num_empl;
      ?column?       |     CAP     | ?column? | count 
---------------------+-------------+----------+-------
 treballador asignat |             |          |     1
 treballador asignat | Sam Clark   |          |     3
 treballador asignat | Dan Roberts |          |     1
 treballador asignat | Bob Smith   |          |     3
 treballador asignat | Larry Fitch |          |     2
(5 rows)


3 parte: 
training=# select 'nombre treballador-cap', cap.nombre as "CAP", empleado.nombre as "SUBORDINADO", null from repventas as empleado left join repventas as cap on cap.num_empl=empleado.director union select 'treballador asignat', cap.nombre as "CAP", null, count(*) from repventas as empleado left join repventas as cap on cap.num_empl=empleado.director group by cap.num_empl order by 2,1;
        ?column?        |     CAP     |  SUBORDINADO  | ?column? 
------------------------+-------------+---------------+----------
 nombre treballador-cap | Bob Smith   | Dan Roberts   |         
 nombre treballador-cap | Bob Smith   | Paul Cruz     |         
 nombre treballador-cap | Bob Smith   | Bill Adams    |         
 treballador asignat    | Bob Smith   |               |        3
 nombre treballador-cap | Dan Roberts | Tom Snyder    |         
 treballador asignat    | Dan Roberts |               |        1
 nombre treballador-cap | Larry Fitch | Sue Smith     |         
 nombre treballador-cap | Larry Fitch | Nancy Angelli |         
 treballador asignat    | Larry Fitch |               |        2
 nombre treballador-cap | Sam Clark   | Mary Jones    |         
 nombre treballador-cap | Sam Clark   | Larry Fitch   |         
 nombre treballador-cap | Sam Clark   | Bob Smith     |         
 treballador asignat    | Sam Clark   |               |        3
 nombre treballador-cap |             | Sam Clark     |         
 treballador asignat    |             |               |        1
(15 rows)


















