Miguel Amorós Moret
46410800

part A

EXERCICI 1:
training=# select descripcion, id_fab, id_producto, '-----', count(distinct clie) as count_clie, '----', count(distinct rep) as count_rep from productos 
left join pedidos on id_fab=fab and id_producto=producto left join clientes on clie=rep_clie group by 2,3 
union                                  
select null, fab, producto, empresa, null, '-----', null from pedidos left join clientes on clie=num_clie
union                                                                                                                   
select null, fab, producto, '-----', null, nombre, null from pedidos left join repventas on rep=num_empl order by 2,3,1;
    descripcion    | id_fab | id_producto |     ?column?      | count_clie |   ?column?    | count_rep 
-------------------+--------+-------------+-------------------+------------+---------------+-----------
 Articulo Tipo 1   | aci    | 41001       | -----             |          0 | ----          |         0
 Articulo Tipo 2   | aci    | 41002       | -----             |          2 | ----          |         2
                   | aci    | 41002       | -----             |            | Bill Adams    |          
                   | aci    | 41002       | Acme Mfg.         |            | -----         |          
                   | aci    | 41002       | Midwest Systems   |            | -----         |          
                   | aci    | 41002       | -----             |            | Larry Fitch   |          
 Articulo Tipo 3   | aci    | 41003       | -----             |          1 | ----          |         1
                   | aci    | 41003       | -----             |            | Bill Adams    |          
                   | aci    | 41003       | JCP Inc.          |            | -----         |          
 Articulo Tipo 4   | aci    | 41004       | -----             |          2 | ----          |         2
                   | aci    | 41004       | -----             |            | Dan Roberts   |          
                   | aci    | 41004       | Acme Mfg.         |            | -----         |          
                   | aci    | 41004       | First Corp.       |            | -----         |          
                   | aci    | 41004       | -----             |            | Bill Adams    |          
 Ajustador         | aci    | 4100x       | -----             |          2 | ----          |         2
                   | aci    | 4100x       | -----             |            | Paul Cruz     |          
                   | aci    | 4100x       | JCP Inc.          |            | -----         |          
                   | aci    | 4100x       | -----             |            | Dan Roberts   |          
                   | aci    | 4100x       | Holm & Landis     |            | -----         |          
 Extractor         | aci    | 4100y       | -----             |          1 | ----          |         1
                   | aci    | 4100y       | Acme Mfg.         |            | -----         |          
                   | aci    | 4100y       | -----             |            | Bill Adams    |          
 Montador          | aci    | 4100z       | -----             |          2 | ----          |         2
                   | aci    | 4100z       | -----             |            | Tom Snyder    |          
                   | aci    | 4100z       | -----             |            | Sue Smith     |          
                   | aci    | 4100z       | Orion Corp        |            | -----         |          
                   | aci    | 4100z       | Ace International |            | -----         |          
 Manivela          | bic    | 41003       | -----             |          2 | ----          |         2
                   | bic    | 41003       | -----             |            | Larry Fitch   |          
                   | bic    | 41003       | Midwest Systems   |            | -----         |          
                   | bic    | 41003       | Peter Brothers    |            | -----         |          
                   | bic    | 41003       | -----             |            | Nancy Angelli |          
 Retn              | bic    | 41089       | -----             |          0 | ----          |         0
 Plate             | bic    | 41672       | -----             |          0 | ----          |         0
 Cubierta          | fea    | 112         | -----             |          1 | ----          |         1
                   | fea    | 112         | Holm & Landis     |            | -----         |          
                   | fea    | 112         | -----             |            | Mary Jones    |          
 Bancada Motor     | fea    | 114         | -----             |          2 | ----          |         2
                   | fea    | 114         | Jones Mfg.        |            | -----         |          
                   | fea    | 114         | -----             |            | Sam Clark     |          
                   | fea    | 114         | -----             |            | Nancy Angelli |          
                   | fea    | 114         | Peter Brothers    |            | -----         |          
 Riostra 1/2-Tm    | imm    | 773c        | -----             |          1 | ----          |         1
                   | imm    | 773c        | Zetacorp          |            | -----         |          
                   | imm    | 773c        | -----             |            | Larry Fitch   |          
 Riostra 1-Tm      | imm    | 775c        | -----             |          1 | ----          |         1
                   | imm    | 775c        | -----             |            | Nancy Angelli |          
                   | imm    | 775c        | Chen Associates   |            | -----         |          
 Riostra 2-Tm      | imm    | 779c        | -----             |          2 | ----          |         2
                   | imm    | 779c        | Rico Enterprises  |            | -----         |          
                   | imm    | 779c        | Holm & Landis     |            | -----         |          
                   | imm    | 779c        | -----             |            | Mary Jones    |          
                   | imm    | 779c        | -----             |            | Sue Smith     |          
 Soporte Riostra   | imm    | 887h        | -----             |          0 | ----          |         0
 Perno Riostra     | imm    | 887p        | -----             |          0 | ----          |         0
 Retenedor Riostra | imm    | 887x        | -----             |          0 | ----          |         0
                   | qsa    | k47         | Midwest Systems   |            | -----         |          
                   | qsa    | k47         | -----             |            | Larry Fitch   |          
 Reductor          | qsa    | xk47        | -----             |          3 | ----          |         2
                   | qsa    | xk47        | Orion Corp        |            | -----         |          
                   | qsa    | xk47        | -----             |            | Sue Smith     |          
                   | qsa    | xk47        | Midwest Systems   |            | -----         |          
                   | qsa    | xk47        | -----             |            | Larry Fitch   |          
                   | qsa    | xk47        | Fred Lewis Corp.  |            | -----         |          
 Reductor          | qsa    | xk48        | -----             |          0 | ----          |         0
 Reductor          | qsa    | xk48a       | -----             |          0 | ----          |         0
 Pasador Bisagra   | rei    | 2a44g       | -----             |          1 | ----          |         1
                   | rei    | 2a44g       | -----             |            | Paul Cruz     |          
                   | rei    | 2a44g       | JCP Inc.          |            | -----         |          
 Bisagra Izqda.    | rei    | 2a44l       | -----             |          1 | ----          |         1
                   | rei    | 2a44l       | -----             |            | Sam Clark     |          
                   | rei    | 2a44l       | J.P. Sinclair     |            | -----         |          
 Bisagra Dcha.     | rei    | 2a44r       | -----             |          2 | ----          |         2
                   | rei    | 2a44r       | -----             |            | Dan Roberts   |          
                   | rei    | 2a44r       | Ian & Schmidt     |            | -----         |          
                   | rei    | 2a44r       | -----             |            | Larry Fitch   |          
                   | rei    | 2a44r       | Zetacorp          |            | -----         |          
 V Stago Trinquete | rei    | 2a45c       | -----             |          2 | ----          |         2
                   | rei    | 2a45c       | -----             |            | Sue Smith     |          
                   | rei    | 2a45c       | Fred Lewis Corp.  |            | -----         |          
                   | rei    | 2a45c       | Ace International |            | -----         |          
                   | rei    | 2a45c       | -----             |            | Tom Snyder    |          
(82 rows)


---------------------------------------------------------------------------------------------------------------------
EXERCICI 2:

training=# select oficina, ciudad, '----', count(num_pedido), sum(importe) from pedidos left join repventas on rep=num_empl left join oficinas on oficina_rep=oficina group by 1 union
select oficina, ciudad, empresa, count(num_pedido), sum(importe) from pedidos left join clientes on clie=num_clie left join repventas on rep=num_empl left join oficinas on oficina_rep=oficina group by 1,2,3 order by 1,3;
 oficina |   ciudad    |     ?column?      | count |   sum    
---------+-------------+-------------------+-------+----------
      11 | New York    | ----              |     4 | 40063.00
      11 | New York    | Holm & Landis     |     2 |  7105.00
      11 | New York    | Jones Mfg.        |     1 |  1458.00
      11 | New York    | J.P. Sinclair     |     1 | 31500.00
      12 | Chicago     | ----              |     5 | 29328.00
      12 | Chicago     | First Corp.       |     1 |  3978.00
      12 | Chicago     | Holm & Landis     |     1 |   150.00
      12 | Chicago     | Ian & Schmidt     |     1 | 22500.00
      12 | Chicago     | JCP Inc.          |     2 |  2700.00
      13 | Atlanta     | ----              |     5 | 39327.00
      13 | Atlanta     | Acme Mfg.         |     4 | 35582.00
      13 | Atlanta     | JCP Inc.          |     1 |  3745.00
      21 | Los Angeles | ----              |    11 | 81409.00
      21 | Los Angeles | Fred Lewis Corp.  |     2 |  4026.00
      21 | Los Angeles | Midwest Systems   |     4 |  3608.00
      21 | Los Angeles | Orion Corp        |     2 | 22100.00
      21 | Los Angeles | Rico Enterprises  |     1 |  3750.00
      21 | Los Angeles | Zetacorp          |     2 | 47925.00
      22 | Denver      | ----              |     3 | 34432.00
      22 | Denver      | Chen Associates   |     1 | 31350.00
      22 | Denver      | Peter Brothers    |     2 |  3082.00
         |             | ----              |     2 | 23132.00
         |             | Ace International |     2 | 23132.00
(23 rows)

------------------------------------------------------------------------------------------------------------------------------------
EXERCICI 3:

training=# select num_clie, empresa, num_empl, nombre, null, null from pedidos left join clientes on clie=num_clie left join repventas on rep_clie=num_empl union 
select num_clie, empresa, null, null, rep, nombre from pedidos left join clientes on clie=num_clie left join repventas on rep=num_empl order by 1,2,3;
 num_clie |      empresa      | num_empl |    nombre     | ?column? |   ?column?    
----------+-------------------+----------+---------------+----------+---------------
     2101 | Jones Mfg.        |      106 | Sam Clark     |          | 
     2101 | Jones Mfg.        |          |               |      106 | Sam Clark
     2102 | First Corp.       |      101 | Dan Roberts   |          | 
     2102 | First Corp.       |          |               |      101 | Dan Roberts
     2103 | Acme Mfg.         |      105 | Bill Adams    |          | 
     2103 | Acme Mfg.         |          |               |      105 | Bill Adams
     2106 | Fred Lewis Corp.  |      102 | Sue Smith     |          | 
     2106 | Fred Lewis Corp.  |          |               |      102 | Sue Smith
     2107 | Ace International |      110 | Tom Snyder    |          | 
     2107 | Ace International |          |               |      110 | Tom Snyder
     2108 | Holm & Landis     |      109 | Mary Jones    |          | 
     2108 | Holm & Landis     |          |               |      101 | Dan Roberts
     2108 | Holm & Landis     |          |               |      109 | Mary Jones
     2109 | Chen Associates   |      103 | Paul Cruz     |          | 
     2109 | Chen Associates   |          |               |      107 | Nancy Angelli
     2111 | JCP Inc.          |      103 | Paul Cruz     |          | 
     2111 | JCP Inc.          |          |               |      103 | Paul Cruz
     2111 | JCP Inc.          |          |               |      105 | Bill Adams
     2112 | Zetacorp          |      108 | Larry Fitch   |          | 
     2112 | Zetacorp          |          |               |      108 | Larry Fitch
     2113 | Ian & Schmidt     |      104 | Bob Smith     |          | 
     2113 | Ian & Schmidt     |          |               |      101 | Dan Roberts
     2114 | Orion Corp        |      102 | Sue Smith     |          | 
     2114 | Orion Corp        |          |               |      102 | Sue Smith
     2114 | Orion Corp        |          |               |      108 | Larry Fitch
     2117 | J.P. Sinclair     |      106 | Sam Clark     |          | 
     2117 | J.P. Sinclair     |          |               |      106 | Sam Clark
     2118 | Midwest Systems   |      108 | Larry Fitch   |          | 
     2118 | Midwest Systems   |          |               |      108 | Larry Fitch
     2120 | Rico Enterprises  |      102 | Sue Smith     |          | 
     2120 | Rico Enterprises  |          |               |      102 | Sue Smith
     2124 | Peter Brothers    |      107 | Nancy Angelli |          | 
     2124 | Peter Brothers    |          |               |      107 | Nancy Angelli
(33 rows)
