PRACTICA 12.  UNION  +  LEFT JOIN + WHERE
       
    1. Llistar els productes amb existències no superiors a 200 i no inferiors a 20 de la fàbrica aci o de la fabrica imm, i els nom dels clients que han comprat aquests productes. 
    Si algun producte no l'ha comprat ningú ha de sortir sense nom de client.
	
'	training=# select 'producto', id_fab, id_producto, existencias, empresa from productos left join pedidos on id_fab=fab and id_producto=producto left join clientes on clie=num_clie 
	where existencias<=200 and existencias>=20 and (id_fab='aci' or id_fab='imm') group by 2,3,4,5;
 ?column? | id_fab | id_producto | existencias |      empresa      
----------+--------+-------------+-------------+-------------------
 producto | aci    | 41002       |         167 | Acme Mfg.
 producto | aci    | 41002       |         167 | Midwest Systems
 producto | aci    | 41004       |         139 | Acme Mfg.
 producto | aci    | 41004       |         139 | First Corp.
 producto | aci    | 4100x       |          37 | Holm & Landis
 producto | aci    | 4100x       |          37 | JCP Inc.
 producto | aci    | 4100y       |          25 | Acme Mfg.
 producto | aci    | 4100z       |          28 | Ace International
 producto | aci    | 4100z       |          28 | Orion Corp
 producto | imm    | 773c        |          28 | Zetacorp
 producto | imm    | 887p        |          24 | 
 producto | imm    | 887x        |          32 | 
(12 rows)

#en vez de poner todos los campos del group by se pueden poner solos los campos identificativos(unicos) de cada tabla: num_clie, id_fab/id_producto, num_pedido, oficina, num_empl
------------------------------------------------------------------------
       
    2. Llistar la ciutat de cada oficina, el número total de treballadors, el nom del cap de l'oficina i el nom de cadascun dels treballadors (incloent al cap com a treballador).
 '   parte 1:
training=# select 'oficina', ciudad, count(*), null, null from oficinas right join repventas on oficina=oficina_rep group by 2;
 ?column? |   ciudad    | count | ?column? | ?column? 
----------+-------------+-------+----------+----------
 oficina  |             |     1 |          | 
 oficina  | Denver      |     1 |          | 
 oficina  | Atlanta     |     1 |          | 
 oficina  | Los Angeles |     2 |          | 
 oficina  | New York    |     2 |          | 
 oficina  | Chicago     |     3 |          | 
(6 rows)


parte 2:
training=# select 'nombre', ciudad, null, cap.nombre as "CAP", empleado.nombre as "TREBALLADOR"from oficinas right join repventas as empleado on oficina=empleado.oficina_rep left join repventas as cap on cap.num_empl=empleado.director;
 ?column? |   ciudad    | ?column? |     CAP     |  TREBALLADOR  
----------+-------------+----------+-------------+---------------
 nombre   | Atlanta     |          | Bob Smith   | Bill Adams
 nombre   | New York    |          | Sam Clark   | Mary Jones
 nombre   | Los Angeles |          | Larry Fitch | Sue Smith
 nombre   | New York    |          |             | Sam Clark
 nombre   | Chicago     |          | Sam Clark   | Bob Smith
 nombre   | Chicago     |          | Bob Smith   | Dan Roberts
 nombre   |             |          | Dan Roberts | Tom Snyder
 nombre   | Los Angeles |          | Sam Clark   | Larry Fitch
 nombre   | Chicago     |          | Bob Smith   | Paul Cruz
 nombre   | Denver      |          | Larry Fitch | Nancy Angelli
(10 rows)

parte 3:
training=# select 'oficina', ciudad, count(*), null, null from oficinas right join repventas on oficina=oficina_rep group by 2 union select 'nombre', ciudad, null, cap.nombre as "CAP", empleado.nombre as "TREBALLADOR"from oficinas right join repventas as empleado on oficina=empleado.oficina_rep left join repventas as cap on cap.num_empl=empleado.director order by 2,1;
 ?column? |   ciudad    | count |  ?column?   |   ?column?    
----------+-------------+-------+-------------+---------------
 nombre   | Atlanta     |       | Bob Smith   | Bill Adams
 oficina  | Atlanta     |     1 |             | 
 nombre   | Chicago     |       | Bob Smith   | Paul Cruz
 nombre   | Chicago     |       | Bob Smith   | Dan Roberts
 nombre   | Chicago     |       | Sam Clark   | Bob Smith
 oficina  | Chicago     |     3 |             | 
 nombre   | Denver      |       | Larry Fitch | Nancy Angelli
 oficina  | Denver      |     1 |             | 
 nombre   | Los Angeles |       | Larry Fitch | Sue Smith
 nombre   | Los Angeles |       | Sam Clark   | Larry Fitch
 oficina  | Los Angeles |     2 |             | 
 nombre   | New York    |       |             | Sam Clark
 nombre   | New York    |       | Sam Clark   | Mary Jones
 oficina  | New York    |     2 |             | 
 nombre   |             |       | Dan Roberts | Tom Snyder
 oficina  |             |     1 |             | 
(16 rows)
-----------------------------------------------------------------------------------------------------------
   
    3. Llistar els noms i preus dels productes de la fàbrica imm i de la fàbrica rei que contenen 'gr' o 'tr' en el seu nom i que valen entre 900 i 5000€, 
    i els noms dels venedors que han venut aquests productes. Si algun producte no l'ha comprat ningú ha de sortir sense nom venedor.
    
'training=# select 'producto', descripcion, id_fab, id_producto, precio, nombre from productos left join pedidos on id_fab=fab and id_producto=producto left join repventas on rep=num_empl where (descripcion ilike '%gr%' or descripcion ilike '%tr%') and (id_fab='imm' or id_fab='rei') and (precio>=900 and precio<=5000);
 ?column? |  descripcion   | id_fab | id_producto | precio  |    nombre     
----------+----------------+--------+-------------+---------+---------------
 producto | Bisagra Izqda. | rei    | 2a44l       | 4500.00 | Sam Clark
 producto | Bisagra Dcha.  | rei    | 2a44r       | 4500.00 | Larry Fitch
 producto | Riostra 1/2-Tm | imm    | 773c        |  975.00 | Larry Fitch
 producto | Riostra 1-Tm   | imm    | 775c        | 1425.00 | Nancy Angelli
 producto | Riostra 2-Tm   | imm    | 779c        | 1875.00 | Sue Smith
 producto | Riostra 2-Tm   | imm    | 779c        | 1875.00 | Mary Jones
 producto | Bisagra Dcha.  | rei    | 2a44r       | 4500.00 | Dan Roberts
(7 rows)

--------------------------------------------------------------------------------------------------

    4. Llistar els noms dels productes, el número total de ventes que s'ha fet d'aquell producte, la quantitat total d'unitats que s'han venut d'aquell producte,
     i el nom de cada client que l'ha comprat.
training=# select 'producto', id_fab, id_producto, descripcion, count(num_pedido), sum(cant), null from productos left join pedidos on id_fab=fab and id_producto=producto group by 2,3;
 ?column? | id_fab | id_producto |    descripcion    | count | sum | ?column? 
----------+--------+-------------+-------------------+-------+-----+----------
 producto | aci    | 41001       | Articulo Tipo 1   |     0 |     | 
 producto | aci    | 41002       | Articulo Tipo 2   |     2 |  64 | 
 producto | aci    | 41003       | Articulo Tipo 3   |     1 |  35 | 
 producto | aci    | 41004       | Articulo Tipo 4   |     3 |  68 | 
 producto | aci    | 4100x       | Ajustador         |     2 |  30 | 
 producto | aci    | 4100y       | Extractor         |     1 |  11 | 
 producto | aci    | 4100z       | Montador          |     2 |  15 | 
 producto | bic    | 41003       | Manivela          |     2 |   2 | 
 producto | bic    | 41089       | Retn              |     0 |     | 
 producto | bic    | 41672       | Plate             |     0 |     | 
 producto | fea    | 112         | Cubierta          |     1 |  10 | 
 producto | fea    | 114         | Bancada Motor     |     2 |  16 | 
 producto | imm    | 773c        | Riostra 1/2-Tm    |     1 |   3 | 
 producto | imm    | 775c        | Riostra 1-Tm      |     1 |  22 | 
 producto | imm    | 779c        | Riostra 2-Tm      |     2 |   5 | 
 producto | imm    | 887h        | Soporte Riostra   |     0 |     | 
 producto | imm    | 887p        | Perno Riostra     |     0 |     | 
 producto | imm    | 887x        | Retenedor Riostra |     0 |     | 
 producto | qsa    | xk47        | Reductor          |     3 |  28 | 
 producto | qsa    | xk48        | Reductor          |     0 |     | 
 producto | qsa    | xk48a       | Reductor          |     0 |     | 
 producto | rei    | 2a44g       | Pasador Bisagra   |     1 |   6 | 
 producto | rei    | 2a44l       | Bisagra Izqda.    |     1 |   7 | 
 producto | rei    | 2a44r       | Bisagra Dcha.     |     2 |  15 | 
 producto | rei    | 2a45c       | V Stago Trinquete |     2 |  32 | 
(25 rows)

     
     
training=# select 'cliente', id_fab, id_producto, descripcion, null, null, empresa from productos left join pedidos on id_fab=fab and id_producto=producto left join clientes on clie=num_clie group by 2,3,7;
 ?column? | id_fab | id_producto |    descripcion    | ?column? | ?column? |      empresa      
----------+--------+-------------+-------------------+----------+----------+-------------------
 cliente  | aci    | 41002       | Articulo Tipo 2   |          |          | Midwest Systems
 cliente  | rei    | 2a44r       | Bisagra Dcha.     |          |          | Ian & Schmidt
 cliente  | qsa    | xk48        | Reductor          |          |          | 
 cliente  | imm    | 887x        | Retenedor Riostra |          |          | 
 cliente  | imm    | 773c        | Riostra 1/2-Tm    |          |          | Zetacorp
 cliente  | aci    | 41004       | Articulo Tipo 4   |          |          | Acme Mfg.
 cliente  | aci    | 41002       | Articulo Tipo 2   |          |          | Acme Mfg.
 cliente  | qsa    | xk48a       | Reductor          |          |          | 
 cliente  | bic    | 41672       | Plate             |          |          | 
 cliente  | fea    | 112         | Cubierta          |          |          | Holm & Landis
 cliente  | fea    | 114         | Bancada Motor     |          |          | Peter Brothers
 cliente  | aci    | 41004       | Articulo Tipo 4   |          |          | First Corp.
 cliente  | bic    | 41003       | Manivela          |          |          | Peter Brothers
 cliente  | aci    | 4100x       | Ajustador         |          |          | JCP Inc.
 cliente  | aci    | 4100z       | Montador          |          |          | Orion Corp
 cliente  | rei    | 2a44l       | Bisagra Izqda.    |          |          | J.P. Sinclair
 cliente  | aci    | 4100x       | Ajustador         |          |          | Holm & Landis
 cliente  | rei    | 2a45c       | V Stago Trinquete |          |          | Fred Lewis Corp.
 cliente  | imm    | 779c        | Riostra 2-Tm      |          |          | Holm & Landis
 cliente  | rei    | 2a45c       | V Stago Trinquete |          |          | Ace International
 cliente  | aci    | 4100y       | Extractor         |          |          | Acme Mfg.
 cliente  | qsa    | xk47        | Reductor          |          |          | Orion Corp
 cliente  | imm    | 887p        | Perno Riostra     |          |          | 
 cliente  | rei    | 2a44r       | Bisagra Dcha.     |          |          | Zetacorp
 cliente  | imm    | 779c        | Riostra 2-Tm      |          |          | Rico Enterprises
 cliente  | aci    | 41001       | Articulo Tipo 1   |          |          | 
 cliente  | bic    | 41089       | Retn              |          |          | 
 cliente  | rei    | 2a44g       | Pasador Bisagra   |          |          | JCP Inc.
 cliente  | qsa    | xk47        | Reductor          |          |          | Midwest Systems
 cliente  | qsa    | xk47        | Reductor          |          |          | Fred Lewis Corp.
 cliente  | bic    | 41003       | Manivela          |          |          | Midwest Systems
 cliente  | aci    | 41003       | Articulo Tipo 3   |          |          | JCP Inc.
 cliente  | aci    | 4100z       | Montador          |          |          | Ace International
 cliente  | imm    | 887h        | Soporte Riostra   |          |          | 
 cliente  | imm    | 775c        | Riostra 1-Tm      |          |          | Chen Associates
 cliente  | fea    | 114         | Bancada Motor     |          |          | Jones Mfg.
(36 rows)


3 parte:
training=# select 'producto', id_fab, id_producto, descripcion, count(num_pedido), sum(cant), null from productos left join pedidos on id_fab=fab and id_producto=producto group by 2,3 union select 'cliente', id_fab, id_producto, descripcion, null, null, empresa from productos left join pedidos on id_fab=fab and id_producto=producto left join clientes on clie=num_clie group by 2,3,7 order by 2,3,1;
 ?column? | id_fab | id_producto |    descripcion    | count | sum |     ?column?      
----------+--------+-------------+-------------------+-------+-----+-------------------
 cliente  | aci    | 41001       | Articulo Tipo 1   |       |     | 
 producto | aci    | 41001       | Articulo Tipo 1   |     0 |     | 
 cliente  | aci    | 41002       | Articulo Tipo 2   |       |     | Acme Mfg.
 cliente  | aci    | 41002       | Articulo Tipo 2   |       |     | Midwest Systems
 producto | aci    | 41002       | Articulo Tipo 2   |     2 |  64 | 
 cliente  | aci    | 41003       | Articulo Tipo 3   |       |     | JCP Inc.
 producto | aci    | 41003       | Articulo Tipo 3   |     1 |  35 | 
 cliente  | aci    | 41004       | Articulo Tipo 4   |       |     | Acme Mfg.
 cliente  | aci    | 41004       | Articulo Tipo 4   |       |     | First Corp.
 producto | aci    | 41004       | Articulo Tipo 4   |     3 |  68 | 
 cliente  | aci    | 4100x       | Ajustador         |       |     | JCP Inc.
 cliente  | aci    | 4100x       | Ajustador         |       |     | Holm & Landis
 producto | aci    | 4100x       | Ajustador         |     2 |  30 | 
 cliente  | aci    | 4100y       | Extractor         |       |     | Acme Mfg.
 producto | aci    | 4100y       | Extractor         |     1 |  11 | 
 cliente  | aci    | 4100z       | Montador          |       |     | Orion Corp
 cliente  | aci    | 4100z       | Montador          |       |     | Ace International
 producto | aci    | 4100z       | Montador          |     2 |  15 | 
 cliente  | bic    | 41003       | Manivela          |       |     | Midwest Systems
 cliente  | bic    | 41003       | Manivela          |       |     | Peter Brothers
 producto | bic    | 41003       | Manivela          |     2 |   2 | 
 cliente  | bic    | 41089       | Retn              |       |     | 
 producto | bic    | 41089       | Retn              |     0 |     | 
 cliente  | bic    | 41672       | Plate             |       |     | 
 producto | bic    | 41672       | Plate             |     0 |     | 
 cliente  | fea    | 112         | Cubierta          |       |     | Holm & Landis
 producto | fea    | 112         | Cubierta          |     1 |  10 | 
 cliente  | fea    | 114         | Bancada Motor     |       |     | Peter Brothers
 cliente  | fea    | 114         | Bancada Motor     |       |     | Jones Mfg.
 producto | fea    | 114         | Bancada Motor     |     2 |  16 | 
 cliente  | imm    | 773c        | Riostra 1/2-Tm    |       |     | Zetacorp
 producto | imm    | 773c        | Riostra 1/2-Tm    |     1 |   3 | 
 cliente  | imm    | 775c        | Riostra 1-Tm      |       |     | Chen Associates
 producto | imm    | 775c        | Riostra 1-Tm      |     1 |  22 | 
 cliente  | imm    | 779c        | Riostra 2-Tm      |       |     | Holm & Landis
 cliente  | imm    | 779c        | Riostra 2-Tm      |       |     | Rico Enterprises
 producto | imm    | 779c        | Riostra 2-Tm      |     2 |   5 | 
 cliente  | imm    | 887h        | Soporte Riostra   |       |     | 
 producto | imm    | 887h        | Soporte Riostra   |     0 |     | 
 cliente  | imm    | 887p        | Perno Riostra     |       |     | 
 producto | imm    | 887p        | Perno Riostra     |     0 |     | 
 cliente  | imm    | 887x        | Retenedor Riostra |       |     | 
 producto | imm    | 887x        | Retenedor Riostra |     0 |     | 
 cliente  | qsa    | xk47        | Reductor          |       |     | Midwest Systems
 cliente  | qsa    | xk47        | Reductor          |       |     | Orion Corp
 cliente  | qsa    | xk47        | Reductor          |       |     | Fred Lewis Corp.
 producto | qsa    | xk47        | Reductor          |     3 |  28 | 
 cliente  | qsa    | xk48        | Reductor          |       |     | 
 producto | qsa    | xk48        | Reductor          |     0 |     | 
 cliente  | qsa    | xk48a       | Reductor          |       |     | 
 producto | qsa    | xk48a       | Reductor          |     0 |     | 
 cliente  | rei    | 2a44g       | Pasador Bisagra   |       |     | JCP Inc.
 producto | rei    | 2a44g       | Pasador Bisagra   |     1 |   6 | 
 cliente  | rei    | 2a44l       | Bisagra Izqda.    |       |     | J.P. Sinclair
 producto | rei    | 2a44l       | Bisagra Izqda.    |     1 |   7 | 
 cliente  | rei    | 2a44r       | Bisagra Dcha.     |       |     | Ian & Schmidt
 cliente  | rei    | 2a44r       | Bisagra Dcha.     |       |     | Zetacorp
 producto | rei    | 2a44r       | Bisagra Dcha.     |     2 |  15 | 
 cliente  | rei    | 2a45c       | V Stago Trinquete |       |     | Ace International
 cliente  | rei    | 2a45c       | V Stago Trinquete |       |     | Fred Lewis Corp.
 producto | rei    | 2a45c       | V Stago Trinquete |     2 |  32 | 
(61 rows)
  
     
---------------------------------------------------------------------------------------------------     


    5. Llistar els poductes que costen més de 1000 o no són ni de la fàbrica imm ni de la fàbrica rei, ni de la fàbrica ací, i el total que n'ha comprat cada client.
     Si algun producte no l'ha comprat ningú ha de sortir sense nom de client.
     
training=# select id_fab, id_producto, descripcion, precio, count(clie), empresa from productos left join pedidos on id_fab=fab and id_producto=producto left join clientes on clie=num_clie where precio>1000 or (id_fab!='imm' and id_fab!='rei' and id_fab!='aci') group by 1,2,6 order by 1,2,6;
 id_fab | id_producto |  descripcion   | precio  | count |      empresa      
--------+-------------+----------------+---------+-------+-------------------
 aci    | 4100y       | Extractor      | 2750.00 |     1 | Acme Mfg.
 aci    | 4100z       | Montador       | 2500.00 |     1 | Ace International
 aci    | 4100z       | Montador       | 2500.00 |     1 | Orion Corp
 bic    | 41003       | Manivela       |  652.00 |     1 | Midwest Systems
 bic    | 41003       | Manivela       |  652.00 |     1 | Peter Brothers
 bic    | 41089       | Retn           |  225.00 |     0 | 
 bic    | 41672       | Plate          |  180.00 |     0 | 
 fea    | 112         | Cubierta       |  148.00 |     1 | Holm & Landis
 fea    | 114         | Bancada Motor  |  243.00 |     1 | Jones Mfg.
 fea    | 114         | Bancada Motor  |  243.00 |     1 | Peter Brothers
 imm    | 775c        | Riostra 1-Tm   | 1425.00 |     1 | Chen Associates
 imm    | 779c        | Riostra 2-Tm   | 1875.00 |     1 | Holm & Landis
 imm    | 779c        | Riostra 2-Tm   | 1875.00 |     1 | Rico Enterprises
 qsa    | xk47        | Reductor       |  355.00 |     1 | Fred Lewis Corp.
 qsa    | xk47        | Reductor       |  355.00 |     1 | Midwest Systems
 qsa    | xk47        | Reductor       |  355.00 |     1 | Orion Corp
 qsa    | xk48        | Reductor       |  134.00 |     0 | 
 qsa    | xk48a       | Reductor       |  117.00 |     0 | 
 rei    | 2a44l       | Bisagra Izqda. | 4500.00 |     1 | J.P. Sinclair
 rei    | 2a44r       | Bisagra Dcha.  | 4500.00 |     1 | Ian & Schmidt
 rei    | 2a44r       | Bisagra Dcha.  | 4500.00 |     1 | Zetacorp
(21 rows)



    6. Llistar els codis de fabricants, el número total de productes d'aquell fabricant i el nom de cadascun dels productes.
 
 1 parte:
 
 'training=# select 'codis', id_fab, null, null, count(id_producto), null from productos group by 2;
 ?column? | id_fab | ?column? | ?column? | count | ?column? 
----------+--------+----------+----------+-------+----------
 codis    | imm    |          |          |     6 | 
 codis    | aci    |          |          |     7 | 
 codis    | fea    |          |          |     2 | 
 codis    | qsa    |          |          |     3 | 
 codis    | rei    |          |          |     4 | 
 codis    | bic    |          |          |     3 | 
(6 rows)

2parte:

training=# select 'producte', id_fab, id_producto, descripcion, null, null from productos group by 2,3;
 ?column? | id_fab | id_producto |    descripcion    | ?column? | ?column? 
----------+--------+-------------+-------------------+----------+----------
 producte | fea    | 114         | Bancada Motor     |          | 
 producte | qsa    | xk48        | Reductor          |          | 
 producte | qsa    | xk47        | Reductor          |          | 
 producte | rei    | 2a44g       | Pasador Bisagra   |          | 
 producte | bic    | 41003       | Manivela          |          | 
 producte | imm    | 887x        | Retenedor Riostra |          | 
 producte | rei    | 2a45c       | V Stago Trinquete |          | 
 producte | imm    | 773c        | Riostra 1/2-Tm    |          | 
 producte | imm    | 887p        | Perno Riostra     |          | 
 producte | rei    | 2a44r       | Bisagra Dcha.     |          | 
 producte | qsa    | xk48a       | Reductor          |          | 
 producte | aci    | 41001       | Articulo Tipo 1   |          | 
 producte | aci    | 41003       | Articulo Tipo 3   |          | 
 producte | aci    | 4100x       | Ajustador         |          | 
 producte | bic    | 41672       | Plate             |          | 
 producte | bic    | 41089       | Retn              |          | 
 producte | aci    | 4100z       | Montador          |          | 
 producte | imm    | 775c        | Riostra 1-Tm      |          | 
 producte | aci    | 41002       | Articulo Tipo 2   |          | 
 producte | imm    | 779c        | Riostra 2-Tm      |          | 
 producte | rei    | 2a44l       | Bisagra Izqda.    |          | 
 producte | aci    | 41004       | Articulo Tipo 4   |          | 
 producte | aci    | 4100y       | Extractor         |          | 
 producte | imm    | 887h        | Soporte Riostra   |          | 
 producte | fea    | 112         | Cubierta          |          | 
(25 rows)
   
   
   
 3 parte:
 training=# select 'codis', id_fab, null, null, count(id_producto), null from productos group by 2 union select 'producte', id_fab, id_producto, descripcion, null, null from productos group by 2,3 order by 2,1 desc;
 ?column? | id_fab | ?column? |     ?column?      | count | ?column? 
----------+--------+----------+-------------------+-------+----------
 producte | aci    | 41004    | Articulo Tipo 4   |       | 
 producte | aci    | 41002    | Articulo Tipo 2   |       | 
 producte | aci    | 4100x    | Ajustador         |       | 
 producte | aci    | 4100y    | Extractor         |       | 
 producte | aci    | 4100z    | Montador          |       | 
 producte | aci    | 41001    | Articulo Tipo 1   |       | 
 producte | aci    | 41003    | Articulo Tipo 3   |       | 
 codis    | aci    |          |                   |     7 | 
 producte | bic    | 41089    | Retn              |       | 
 producte | bic    | 41672    | Plate             |       | 
 producte | bic    | 41003    | Manivela          |       | 
 codis    | bic    |          |                   |     3 | 
 producte | fea    | 112      | Cubierta          |       | 
 producte | fea    | 114      | Bancada Motor     |       | 
 codis    | fea    |          |                   |     2 | 
 producte | imm    | 779c     | Riostra 2-Tm      |       | 
 producte | imm    | 773c     | Riostra 1/2-Tm    |       | 
 producte | imm    | 887x     | Retenedor Riostra |       | 
 producte | imm    | 775c     | Riostra 1-Tm      |       | 
 producte | imm    | 887p     | Perno Riostra     |       | 
 producte | imm    | 887h     | Soporte Riostra   |       | 
 codis    | imm    |          |                   |     6 | 
 producte | qsa    | xk47     | Reductor          |       | 
 producte | qsa    | xk48a    | Reductor          |       | 
 producte | qsa    | xk48     | Reductor          |       | 
 codis    | qsa    |          |                   |     3 | 
 producte | rei    | 2a44l    | Bisagra Izqda.    |       | 
 producte | rei    | 2a44g    | Pasador Bisagra   |       | 
 producte | rei    | 2a45c    | V Stago Trinquete |       | 
 producte | rei    | 2a44r    | Bisagra Dcha.     |       | 
 codis    | rei    |          |                   |     4 | 
(31 rows)
    
 ------------------------------------------------   
    
    
    


    7. Llistar els venedors i els seus imports totals de ventes, que tinguin més de 30 anys i treballin a l'oficina 12 i els que tinguin més de 20 anys i treballin a l'oficina 21.
     Llistar els clients a qui ha venut cadascun d'aquests venedors.'

1 parte:
training=# select 'vendedor', nombre, null, sum(importe) from repventas left join pedidos on num_empl=rep where (edad>30 and oficina_rep=12) or (edad>20 and oficina_rep=21) group by rep, nombre;
 ?column? |   nombre    | ?column? |   sum    
----------+-------------+----------+----------
 vendedor | Dan Roberts |          | 26628.00
 vendedor | Sue Smith   |          | 22776.00
 vendedor | Larry Fitch |          | 58633.00
 vendedor | Bob Smith   |          |         
(4 rows)


2 parte:
training=# select 'cliente', nombre, empresa, null from repventas left join pedidos on num_empl=rep left join clientes on clie=num_clie where (edad>30 and oficina_rep=12) or (edad>20 and oficina_rep=21) group by rep, nombre, empresa;
 ?column? |   nombre    |     empresa      | ?column? 
----------+-------------+------------------+----------
 cliente  | Dan Roberts | First Corp.      | 
 cliente  | Dan Roberts | Holm & Landis    | 
 cliente  | Dan Roberts | Ian & Schmidt    | 
 cliente  | Sue Smith   | Fred Lewis Corp. | 
 cliente  | Sue Smith   | Orion Corp       | 
 cliente  | Sue Smith   | Rico Enterprises | 
 cliente  | Larry Fitch | Midwest Systems  | 
 cliente  | Larry Fitch | Orion Corp       | 
 cliente  | Larry Fitch | Zetacorp         | 
 cliente  | Bob Smith   |                  | 
(10 rows)

3 parte:
training=# select 'vendedor', nombre, null, sum(importe) from repventas left join pedidos on num_empl=rep where (edad>30 and oficina_rep=12) or (edad>20 and oficina_rep=21) group by rep, nombre union select 'cliente', nombre, empresa, null from repventas left join pedidos on num_empl=rep left join clientes on clie=num_clie where (edad>30 and oficina_rep=12) or (edad>20 and oficina_rep=21) group by rep, nombre, empresa order by 2,1;
 ?column? |   nombre    |     ?column?     |   sum    
----------+-------------+------------------+----------
 cliente  | Bob Smith   |                  |         
 vendedor | Bob Smith   |                  |         
 cliente  | Dan Roberts | Holm & Landis    |         
 cliente  | Dan Roberts | Ian & Schmidt    |         
 cliente  | Dan Roberts | First Corp.      |         
 vendedor | Dan Roberts |                  | 26628.00
 cliente  | Larry Fitch | Orion Corp       |         
 cliente  | Larry Fitch | Midwest Systems  |         
 cliente  | Larry Fitch | Zetacorp         |         
 vendedor | Larry Fitch |                  | 58633.00
 cliente  | Sue Smith   | Orion Corp       |         
 cliente  | Sue Smith   | Rico Enterprises |         
 cliente  | Sue Smith   | Fred Lewis Corp. |         
 vendedor | Sue Smith   |                  | 22776.00
(14 rows)

