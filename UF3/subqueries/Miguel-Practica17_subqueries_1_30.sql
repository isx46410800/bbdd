-------------------------------------------------------------------------------
--          Subconsultes
-------------------------------------------------------------------------------


-- 5.1- Llista els venedors que tinguin una quota igual o inferior a l'objectiu de l'oficina de vendes d'Atlanta.
training=# select objetivo from oficinas where oficina=13;
 objetivo  
-----------
 350000.00
(1 row)

training=# select nombre from repventas where cuota<=(select objetivo from oficinas where oficina=13);
    nombre     
---------------
 Bill Adams
 Mary Jones
 Sue Smith
 Sam Clark
 Bob Smith
 Dan Roberts
 Larry Fitch
 Paul Cruz
 Nancy Angelli
(9 rows)




-- 5.2- Tots els clients, identificador i nom de l'empresa, que han estat atesos per (que han fet comanda amb) Bill Adams.
training=# select num_clie, empresa from clientes where num_clie in (select distinct(clie) from pedidos join repventas on rep=num_empl where num_empl=105);
 num_clie |  empresa  
----------+-----------
     2111 | JCP Inc.
     2103 | Acme Mfg.
(2 rows)



-- 5.3- Venedors amb quotes que siguin iguals o superiors a l'objectiu de llur oficina de vendes.
training=# select num_empl, nombre from repventas where cuota >= (select objetivo from oficinas where oficina=oficina_rep);
 num_empl |    nombre     
----------+---------------
      105 | Bill Adams
      107 | Nancy Angelli
(2 rows)



-- 5.4- Mostrar l'identificador de l'oficina i la ciutat de les oficines on l'objectiu de vendes de l'oficina excedeix la suma de quotes dels venedors d'aquella oficina.
training=# select oficina, ciudad from oficinas where objetivo > (select sum(cuota) from repventas where oficina_rep=oficina);
 oficina |   ciudad    
---------+-------------
      12 | Chicago
      21 | Los Angeles
(2 rows)



-- 5.5- Llista dels productes del fabricant amb identificador "aci" que les existències superen les existències del producte amb identificador de producte "41004" i identificador de fabricant "aci".
training=# select id_producto, id_fab, descripcion from productos where id_fab='aci' and existencias > (select existencias from productos where id_fab='aci' and id_producto='41004');
 id_producto | id_fab |   descripcion   
-------------+--------+-----------------
 41003       | aci    | Articulo Tipo 3
 41001       | aci    | Articulo Tipo 1
 41002       | aci    | Articulo Tipo 2
(3 rows)


-- 5.6- Llistar els venedors que han acceptat una comanda que representa més del 10% de la seva quota.
training=# select id_producto, id_fab, descripcion from productos where id_fab='aci' and existencias > (select existencias from productos where id_fab='aci' and id_producto='41004');
 id_producto | id_fab |   descripcion   
-------------+--------+-----------------
 41003       | aci    | Articulo Tipo 3
 41001       | aci    | Articulo Tipo 1
 41002       | aci    | Articulo Tipo 2
(3 rows)

**OTRA OPCION**

training=# select num_empl,nombre from repventas where num_empl in ( select rep from pedidos where rep=num_empl and importe>cuota*0.1);
 num_empl |    nombre     
----------+---------------
      106 | Sam Clark
      108 | Larry Fitch
      107 | Nancy Angelli
(3 rows)


-- 5.7- Llistar el nom i l'edat de totes les persones de l'equip de vendes que no dirigeixen una oficina.
#el IN se usa para indicar si cumple con la condicion de dentro, en este caso si algun num_empl está en la lista de dir de oficinas, en este caso es en negado

training=# select nombre, edad from repventas where num_empl not in (select dir from oficinas);
    nombre     | edad 
---------------+------
 Mary Jones    |   31
 Sue Smith     |   48
 Dan Roberts   |   45
 Tom Snyder    |   41
 Paul Cruz     |   29
 Nancy Angelli |   49
(6 rows)



-- 5.8- Llistar aquelles oficines, i els seus objectius, que tots els seus venedors tinguin unes vendes que superen el 50% de l'objectiu de l'oficina.
#el all se usa para decir que toda la condicion ha de cumplirse, en este caso, que todos los empleados tengan +50% que los objetivos, si alguno no, esa oficina no saldria.

training=# select oficina, ciudad, oficinas.objetivo from oficinas where oficinas.objetivo*0.5 < all ( select repventas.ventas from repventas where oficina_rep=oficina); 
 oficina |  ciudad  | objetivo  
---------+----------+-----------
      22 | Denver   | 300000.00
      11 | New York | 575000.00
      13 | Atlanta  | 350000.00
(3 rows)


-- 5.9- Llistar aquells clients que els seus representants de vendes estàn assignats a oficines de la regió est.
training=# select empresa from clientes where rep_clie in (select num_empl from repventas,oficinas where oficina_rep=oficina and region='Este');
     empresa     
-----------------
 JCP Inc.
 First Corp.
 Acme Mfg.
 Smithson Corp.
 Jones Mfg.
 QMA Assoc.
 Holm & Landis
 J.P. Sinclair
 Three-Way Lines
 Solomon Inc.
 Ian & Schmidt
 Chen Associates
 AAA Investments
(13 rows)

------otra opcion-------
training=# select num_clie, empresa from clientes where rep_clie in (select num_empl from repventas where oficina_rep in (select oficina from oficinas where region='Este'));
 num_clie |     empresa     
----------+-----------------
     2111 | JCP Inc.
     2102 | First Corp.
     2103 | Acme Mfg.
     2115 | Smithson Corp.
     2101 | Jones Mfg.
     2121 | QMA Assoc.
     2108 | Holm & Landis
     2117 | J.P. Sinclair
     2122 | Three-Way Lines
     2119 | Solomon Inc.
     2113 | Ian & Schmidt
     2109 | Chen Associates
     2105 | AAA Investments
(13 rows)



-- 5.10- Llistar els venedors que treballen en oficines que superen el seu objectiu.
training=# select nombre from repventas where ventas > (select objetivo from oficinas where oficina_rep=oficina);
   nombre   
------------
 Bill Adams
(1 row)

--o entendiendo que ventas mayor objetivo de la tabla oficinas--
training=# select num_empl, nombre from repventas where oficina_rep = any (select oficina from oficinas where ventas > objetivo);
 num_empl |   nombre    
----------+-------------
      105 | Bill Adams
      109 | Mary Jones
      102 | Sue Smith
      106 | Sam Clark
      108 | Larry Fitch
(5 rows)
--equivalentes
training=# select num_empl, nombre from repventas where oficina_rep in (select oficina from oficinas where ventas>objetivo);
 num_empl |   nombre    
----------+-------------
      105 | Bill Adams
      109 | Mary Jones
      102 | Sue Smith
      106 | Sam Clark
      108 | Larry Fitch
(5 rows)


-- 5.11- Llistar els venedors que treballen en oficines que superen el seu objectiu. 
training=# select nombre from repventas where ventas > (select objetivo from oficinas where oficina_rep=oficina);
   nombre   
------------
 Bill Adams
(1 row)

-- Mostrar també les següents dades de l'oficina: ciutat i la diferència entre les vendes i l'objectiu. Ordenar el resultat per aquest últim valor.
 Proposa dues sentències SQL, una amb subconsultes i una sense.

training=# select nombre, ciudad, oficina, (oficinas.ventas-objetivo) as "Diferencia" from repventas left join oficinas on oficina_rep=oficina where repventas.ventas > (select objetivo from oficinas where oficina_rep=oficina);
   nombre   | ciudad  | oficina | Diferencia 
------------+---------+---------+------------
 Bill Adams | Atlanta |      13 |   17911.00
(1 row)

training=# select num_empl, nombre, ciudad, oficinas.ventas - objetivo as dif from repventas join oficinas on oficina_rep=oficina where oficina_rep in (select oficina from oficinas where ventas > objetivo);
 num_empl |   nombre    |   ciudad    |    dif    
----------+-------------+-------------+-----------
      105 | Bill Adams  | Atlanta     |  17911.00
      109 | Mary Jones  | New York    | 117637.00
      102 | Sue Smith   | Los Angeles | 110915.00
      106 | Sam Clark   | New York    | 117637.00
      108 | Larry Fitch | Los Angeles | 110915.00
(5 rows)

--subqueries en los campos principales
training=# select num_empl, nombre, (select ciudad from oficinas where oficina=oficina_rep), (select ventas from oficinas where oficina=oficina_rep) - (select objetivo from oficinas where oficina=oficina_rep) from repventas where oficina_rep in (select oficina from oficinas where ventas > objetivo);
 num_empl |   nombre    |   ciudad    | ?column?  
----------+-------------+-------------+-----------
      105 | Bill Adams  | Atlanta     |  17911.00
      109 | Mary Jones  | New York    | 117637.00
      102 | Sue Smith   | Los Angeles | 110915.00
      106 | Sam Clark   | New York    | 117637.00
      108 | Larry Fitch | Los Angeles | 110915.00
(5 rows)


-- 5.12- Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. Sense usar consultes multitaula.
training=# select nombre from repventas where oficina_rep is null or oficina_rep not in (select oficina from oficinas where dir = (select num_empl from repventas where nombre = 'Larry Fitch'));
   nombre    
-------------
 Bill Adams
 Mary Jones
 Sam Clark
 Bob Smith
 Dan Roberts
 Tom Snyder
 Paul Cruz
(7 rows)


-- 5.13- Llistar els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. 
--Mostrant també la ciutat de l'oficina on treballa l'empleat i l'identificador del director de la oficina. Proposa dues sentències SQL, una amb subconsultes i una sense.
training=# select nombre, ciudad, director from repventas left join oficinas on oficina_rep=oficina where oficina_rep is null or oficina_rep not in (select oficina from oficinas where dir = (select num_empl from repventas where nombre = 'Larry Fitch'));
   nombre    |  ciudad  | director 
-------------+----------+----------
 Bill Adams  | Atlanta  |      104
 Mary Jones  | New York |      106
 Sam Clark   | New York |         
 Bob Smith   | Chicago  |      106
 Dan Roberts | Chicago  |      104
 Tom Snyder  |          |      101
 Paul Cruz   | Chicago  |      104
(7 rows)


-- 5.14- Llistar tots els clients que han realitzat comandes del productes de la família ACI Widgets entre gener i juny del 1990. 
--Els productes de la famíla ACI Widgets són aquells que tenen identificador de fabricant "aci" i que l'identificador del producte comença per "4100".
training=# select empresa from clientes where num_clie in (select distinct clie from pedidos where fab='aci' and producto like '4100%' and fecha_pedido>='1990-01-01' and fecha_pedido<='1990-06-30');
      empresa      
-------------------
 Acme Mfg.
 Ace International
 Holm & Landis
 JCP Inc.
(4 rows)

--con join-
training=# select distinct empresa from clientes left join pedidos on num_clie=clie where fab='aci' and producto like '4100%' and fecha_pedido>='1990-01-01' and fecha_pedido<='1990-06-30';
      empresa      
-------------------
 Ace International
 Acme Mfg.
 Holm & Landis
 JCP Inc.
(4 rows)


-- 5.15- Llistar els clients que no tenen cap comanda.
training=# select  
     empresa     
-----------------
 Carter & Sons
 Smithson Corp.
 QMA Assoc.
 Three-Way Lines
 Solomon Inc.
 AAA Investments
(6 rows)

--con join--
training=# select empresa, count(clie) from clientes left join pedidos on clie=num_clie group by 1 having count(clie)=0;
     empresa     | count 
-----------------+-------
 AAA Investments |     0
 Solomon Inc.    |     0
 Three-Way Lines |     0
 Carter & Sons   |     0
 QMA Assoc.      |     0
 Smithson Corp.  |     0
(6 rows)


-- 5.16- Llistar els clients que tenen assignat el venedor que porta més temps a contractat.

training=# select empresa from clientes where rep_clie = (select num_empl from repventas order by contrato limit 1);
     empresa     
-----------------
 First Corp.
 Smithson Corp.
 AAA Investments
(3 rows)



training=# select nombre from repventas order by contrato;
    nombre     
---------------
 Dan Roberts
 Sue Smith
 Paul Cruz
 Bob Smith
 Bill Adams
 Sam Clark
 Nancy Angelli
 Larry Fitch
 Mary Jones
 Tom Snyder
(10 rows)



-- 5.17- Llistar els clients assignats a Sue Smith que no han fet cap comanda amb un import superior a 30000. Proposa una sentència SQL sense usar multitaula i una altre en que s'usi multitaula i subconsultes.
training=# select num_clie, empresa from clientes where rep_clie = (select num_empl from repventas where num_empl=102) and num_clie not in (select clie from pedidos where importe>30000);
 num_clie |     empresa      
----------+------------------
     2123 | Carter & Sons
     2114 | Orion Corp
     2120 | Rico Enterprises
     2106 | Fred Lewis Corp.
(4 rows)

training=# select num_clie, empresa from clientes where rep_clie = (select num_empl from repventas where num_empl=102) and not exists (select clie from pedidos where importe>30000 and clie=num_clie);
 num_clie |     empresa      
----------+------------------
     2123 | Carter & Sons
     2114 | Orion Corp
     2120 | Rico Enterprises
     2106 | Fred Lewis Corp.
(4 rows)



-- 5.18- Llistar l'identificador i el nom dels directors d'empleats que tenen més de 40 anys i que dirigeixen un venedor que té unes vendes superiors a la seva pròpia quota.
training=# select director.num_empl, director.nombre from repventas as director where director.num_empl = any(select empleado.director from repventas as empleado where empleado.ventas > director.cuota);
 num_empl |   nombre    
----------+-------------
      106 | Sam Clark
      104 | Bob Smith
      108 | Larry Fitch
(3 rows)


-- 5.19- Llista d'oficines on hi hagi algun venedor tal que la seva quota representi més del 50% de l'objectiu de l'oficina
training=# select oficina, ciudad from oficinas where 0.5*oficinas.objetivo < any(select cuota from repventas where oficina=oficina_rep);
 oficina |  ciudad  
---------+----------
      22 | Denver
      11 | New York
      13 | Atlanta
(3 rows)

**any es para alguno. menor que alguno de estos < any

-- 5.20- Llista d'oficines on tots els venedors tinguin la seva quota superior al 55% de l'objectiu de l'oficina.
training=# select oficina, ciudad from oficinas where 0.55*oficinas.objetivo < all(select cuota from repventas where oficina=oficina_rep);
 oficina | ciudad  
---------+---------
      22 | Denver
      13 | Atlanta
(2 rows)

**all para todos los participantes que cumplan esto < all

training=# select oficina, ciudad from oficinas where oficina not in(select oficina_rep from repventas where oficina=oficina_rep and cuota <=oficinas.objetivo*0.55);
 oficina | ciudad  
---------+---------
      22 | Denver
      13 | Atlanta
(2 rows)


-- 5.21- Transforma el següent JOIN a una comanda amb subconsultes:
-- SELECT num_pedido, importe, clie, num_clie, limite_credito
-- FROM pedidos JOIN clientes
-- ON clie = num_clie;
training=# select num_pedido, clie, importe, (select empresa from clientes where clie=num_clie), (select limite_credito from clientes where clie=num_clie) from pedidos where clie in (select num_clie from clientes where clie=num_clie);
 num_pedido | clie | importe  |      empresa      | limite_credito 
------------+------+----------+-------------------+----------------
     112961 | 2117 | 31500.00 | J.P. Sinclair     |       35000.00
     113012 | 2111 |  3745.00 | JCP Inc.          |       50000.00
     112989 | 2101 |  1458.00 | Jones Mfg.        |       65000.00
     113051 | 2118 |  1420.00 | Midwest Systems   |       60000.00
     112968 | 2102 |  3978.00 | First Corp.       |       65000.00
     110036 | 2107 | 22500.00 | Ace International |       35000.00
     113045 | 2112 | 45000.00 | Zetacorp          |       50000.00
     112963 | 2103 |  3276.00 | Acme Mfg.         |       50000.00
     113013 | 2118 |   652.00 | Midwest Systems   |       60000.00
     113058 | 2108 |  1480.00 | Holm & Landis     |       55000.00
     112997 | 2124 |   652.00 | Peter Brothers    |       40000.00
     112983 | 2103 |   702.00 | Acme Mfg.         |       50000.00
     113024 | 2114 |  7100.00 | Orion Corp        |       20000.00
     113062 | 2124 |  2430.00 | Peter Brothers    |       40000.00
     112979 | 2114 | 15000.00 | Orion Corp        |       20000.00
     113027 | 2103 |  4104.00 | Acme Mfg.         |       50000.00
     113007 | 2112 |  2925.00 | Zetacorp          |       50000.00
     113069 | 2109 | 31350.00 | Chen Associates   |       25000.00
     113034 | 2107 |   632.00 | Ace International |       35000.00
     112992 | 2118 |   760.00 | Midwest Systems   |       60000.00
     112975 | 2111 |  2100.00 | JCP Inc.          |       50000.00
     113055 | 2108 |   150.00 | Holm & Landis     |       55000.00
     113048 | 2120 |  3750.00 | Rico Enterprises  |       50000.00
     112993 | 2106 |  1896.00 | Fred Lewis Corp.  |       65000.00
     113065 | 2106 |  2130.00 | Fred Lewis Corp.  |       65000.00
     113003 | 2108 |  5625.00 | Holm & Landis     |       55000.00
     113049 | 2118 |   776.00 | Midwest Systems   |       60000.00
     112987 | 2103 | 27500.00 | Acme Mfg.         |       50000.00
     113057 | 2111 |   600.00 | JCP Inc.          |       50000.00
     113042 | 2113 | 22500.00 | Ian & Schmidt     |       20000.00
(30 rows)

training=# select num_clie, empresa, limite_credito from clientes where num_clie in (select clie from pedidos where clie=num_clie);
 num_clie |      empresa      | limite_credito 
----------+-------------------+----------------
     2111 | JCP Inc.          |       50000.00
     2102 | First Corp.       |       65000.00
     2103 | Acme Mfg.         |       50000.00
     2107 | Ace International |       35000.00
     2101 | Jones Mfg.        |       65000.00
     2112 | Zetacorp          |       50000.00
     2114 | Orion Corp        |       20000.00
     2124 | Peter Brothers    |       40000.00
     2108 | Holm & Landis     |       55000.00
     2117 | J.P. Sinclair     |       35000.00
     2120 | Rico Enterprises  |       50000.00
     2106 | Fred Lewis Corp.  |       65000.00
     2118 | Midwest Systems   |       60000.00
     2113 | Ian & Schmidt     |       20000.00
     2109 | Chen Associates   |       25000.00
(15 rows)


-- 5.22- Transforma el següent JOIN a una comanda amb subconsultes:
-- SELECT empl.nombre, empl.cuota, dir.nombre, dir.cuota
-- FROM repventas AS empl JOIN repventas AS dir
-- ON empl.director = dir.num_empl
-- WHERE empl.cuota > dir.cuota;
training=# select empl.nombre, empl.cuota, (select dir.nombre from repventas as dir where empl.director=dir.num_empl), (select dir.cuota from repventas as dir where empl.director=dir.num_empl) 
from repventas as empl where empl.cuota > (select dir.cuota from repventas as dir where empl.director=dir.num_empl);
   nombre    |   cuota   |  nombre   |   cuota   
-------------+-----------+-----------+-----------
 Bill Adams  | 350000.00 | Bob Smith | 200000.00
 Mary Jones  | 300000.00 | Sam Clark | 275000.00
 Dan Roberts | 300000.00 | Bob Smith | 200000.00
 Larry Fitch | 350000.00 | Sam Clark | 275000.00
 Paul Cruz   | 275000.00 | Bob Smith | 200000.00
(5 rows)


-- 5.23- Transforma la següent consulta amb un ANY a una consulta amb un EXISTS i també en una altre consulta amb un ALL:
-- SELECT oficina FROM oficinas WHERE ventas*0.8 < ANY (SELECT ventas FROM repventas WHERE oficina_rep = oficina);
ANY ( donde haya alguna linea de las ventas de algun representante que superen el 80% de las ventas de su oficina)
SELECT oficina FROM oficinas WHERE ventas*0.8 < ANY (SELECT ventas FROM repventas WHERE oficina_rep = oficina);
 oficina 
---------
      22
      13
(2 rows)


EXISTS(donde exista que algun representante  superen con sus ventas las ventas*0.8 de su oficina)
training=# SELECT oficina FROM oficinas WHERE exists (SELECT ventas FROM repventas WHERE oficina_rep = oficina and oficinas.ventas*0.8<repventas.ventas);
 oficina 
---------
      22
      13
(2 rows)

ALL(selecciona todas las oficinas donde NO todos los representantes NO superen al 80% de las ventas de la oficina, por lo tanto, salen todos los representantes que superen el 80% de su oficina)
training=# SELECT oficina FROM oficinas WHERE not ventas*0.8 >= all (SELECT ventas FROM repventas WHERE oficina_rep = oficina);
 oficina 
---------
      22
      13
(2 rows)


-- 5.24- Transforma la següent consulta amb un ALL a una consutla amb un EXISTS i també en una altre consulta amb un ANY:
-- SELECT num_clie FROM clientes WHERE limite_credito < ALL (SELECT importe FROM pedidos WHERE num_clie = clie);
ALL( selecciona  los clientes que tienen en todos sus pedidos el importe mayor a su limite de credito)
training=# SELECT num_clie FROM clientes WHERE limite_credito < ALL (SELECT importe FROM pedidos WHERE num_clie = clie);
 num_clie 
----------
     2123
     2115
     2121
     2122
     2119
     2113
     2109
     2105
(8 rows)

EXISTS(selecciona los clientes que no tengan en sus pedidos el importe inferior a su limite de credito)
training=# select num_clie from clientes where not exists (select importe from pedidos where num_clie=clie and limite_credito >= importe);
 num_clie 
----------
     2123
     2115
     2121
     2122
     2119
     2113
     2109
     2105
(8 rows)

ANY(selecciona los clientes que no tengan en sus pedidos, el limite de credito mayor o igual a su importe)
training=# SELECT num_clie FROM clientes WHERE NOT limite_credito >= ANY (SELECT importe FROM pedidos WHERE num_clie = clie);
 num_clie 
----------
     2123
     2115
     2121
     2122
     2119
     2113
     2109
     2105
(8 rows)


-- 5.25- Transforma la següent consulta amb un EXISTS a una consulta amb un ALL i també a una altre consulta amb un ANY:
-- SELECT num_clie, empresa FROM clientes WHERE EXISTS (SELECT * FROM repventas WHERE rep_clie = num_empl AND edad BETWEEN 40 AND 50);
EXISTS(selecciona clientes que tengan un representante de ventas asignado con una edad entre 40 y 50 años)
training=# SELECT num_clie, empresa FROM clientes WHERE EXISTS (SELECT * FROM repventas WHERE rep_clie = num_empl AND edad BETWEEN 40 AND 50);
 num_clie |      empresa      
----------+-------------------
     2106 | Fred Lewis Corp.
     2120 | Rico Enterprises
     2114 | Orion Corp
     2123 | Carter & Sons
     2105 | AAA Investments
     2115 | Smithson Corp.
     2102 | First Corp.
     2107 | Ace International
     2124 | Peter Brothers
(9 rows)

ANY(un rep-clie igual a esa edad)
training=# SELECT num_clie, empresa FROM clientes WHERE rep_clie = any (SELECT num_empl FROM repventas WHERE rep_clie = num_empl AND edad BETWEEN 40 AND 50);
 num_clie |      empresa      
----------+-------------------
     2102 | First Corp.
     2123 | Carter & Sons
     2107 | Ace International
     2115 | Smithson Corp.
     2114 | Orion Corp
     2124 | Peter Brothers
     2120 | Rico Enterprises
     2106 | Fred Lewis Corp.
     2105 | AAA Investments
(9 rows)

ALL
training=# SELECT num_clie, empresa FROM clientes WHERE not rep_clie <> ALL (SELECT num_empl FROM repventas WHERE rep_clie = num_empl AND edad BETWEEN 40 AND 50);
 num_clie |      empresa      
----------+-------------------
     2102 | First Corp.
     2123 | Carter & Sons
     2107 | Ace International
     2115 | Smithson Corp.
     2114 | Orion Corp
     2124 | Peter Brothers
     2120 | Rico Enterprises
     2106 | Fred Lewis Corp.
     2105 | AAA Investments
(9 rows)




-- 5.26- Transforma la següent consulta amb subconsultes a una consulta sense subconsultes.
-- SELECT * FROM productos WHERE id_fab IN(SELECT fab FROM pedidos WHERE cant > 30) AND id_producto IN(SELECT producto FROM pedidos WHERE cant > 30);

--Selecciona los productos en que se hizo pedidos de 30 cantidad por cada fab/producto
training=# SELECT * FROM productos WHERE id_fab IN(SELECT fab FROM pedidos WHERE cant > 30) AND id_producto IN(SELECT producto FROM pedidos WHERE cant > 30);
 id_fab | id_producto |   descripcion   | precio | existencias 
--------+-------------+-----------------+--------+-------------
 aci    | 41003       | Articulo Tipo 3 | 107.00 |         207
 aci    | 41004       | Articulo Tipo 4 | 117.00 |         139
 aci    | 41002       | Articulo Tipo 2 |  76.00 |         167
(3 rows)

training=# select id_fab, id_producto, descripcion, precio, existencias from pedidos left join productos on id_fab=fab and id_producto=producto where cant>30;
 id_fab | id_producto |   descripcion   | precio | existencias 
--------+-------------+-----------------+--------+-------------
 aci    | 41002       | Articulo Tipo 2 |  76.00 |         167
 aci    | 41003       | Articulo Tipo 3 | 107.00 |         207
 aci    | 41004       | Articulo Tipo 4 | 117.00 |         139
(3 rows)


-- 5.27- Transforma la següent consulta amb subconsultes a una consulta sense subconsultes.
-- SELECT num_empl, nombre FROM repventas WHERE num_empl = ANY ( SELECT rep_clie FROM clientes WHERE empresa LIKE '%Inc.');

--Selecciona los representantes que acaban en %inc sus clientes
training=# SELECT num_empl, nombre FROM repventas WHERE num_empl = ANY ( SELECT rep_clie FROM clientes WHERE empresa LIKE '%Inc.');
 num_empl |   nombre   
----------+------------
      109 | Mary Jones
      103 | Paul Cruz
(2 rows)

training=# select num_empl, nombre from repventas left join clientes on rep_clie=num_empl where empresa like '%Inc.';
 num_empl |   nombre   
----------+------------
      103 | Paul Cruz
      109 | Mary Jones
(2 rows)


-- 5.28- Transforma la següent consulta amb un IN a una consulta amb un EXISTS i també a una altre consulta amb un ALL.
-- SELECT num_empl, nombre FROM repventas WHERE num_empl IN(SELECT director FROM repventas);

--Seleccionamos los representantes de ventas que son directores de vendedores
IN
training=# SELECT num_empl, nombre FROM repventas WHERE num_empl IN(SELECT director FROM repventas);
 num_empl |   nombre    
----------+-------------
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      108 | Larry Fitch
(4 rows)


EXISTS
training=# SELECT num_empl, nombre FROM repventas as rep WHERE exists (SELECT director FROM repventas as dir where rep.num_empl=dir.director);
 num_empl |   nombre    
----------+-------------
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      108 | Larry Fitch
(4 rows)


ANY
training=# SELECT num_empl, nombre FROM repventas WHERE num_empl = any(SELECT director FROM repventas);
 num_empl |   nombre    
----------+-------------
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      108 | Larry Fitch
(4 rows)

ALL
training=# SELECT num_empl, nombre FROM repventas WHERE not(num_empl !=all(SELECT director FROM repventas));
 num_empl |   nombre    
----------+-------------
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      108 | Larry Fitch
(4 rows)

-- 5.29- Modifica la següent consulta perquè mostri la ciutat de l'oficina, proposa una consulta simplificada.
-- SELECT num_pedido FROM pedidos WHERE rep IN (SELECT num_empl FROM repventas WHERE ventas > (SELECT avg(ventas) FROM repventas) AND oficina_rep IN (SELECT oficina FROM oficinas WHERE region ILIKE 'este'));

training=# select num_pedido, oficina, ciudad, region, num_empl from pedidos left join repventas on rep=num_empl left join oficinas on oficina_rep=oficina where region ilike 'este' 
and repventas.ventas > (select avg(ventas) from repventas);

 num_pedido | oficina |  ciudad  | region | num_empl 
------------+---------+----------+--------+----------
     112961 |      11 | New York | Este   |      106
     113012 |      13 | Atlanta  | Este   |      105
     112989 |      11 | New York | Este   |      106
     112968 |      12 | Chicago  | Este   |      101
     112963 |      13 | Atlanta  | Este   |      105
     113058 |      11 | New York | Este   |      109
     112983 |      13 | Atlanta  | Este   |      105
     113027 |      13 | Atlanta  | Este   |      105
     113055 |      12 | Chicago  | Este   |      101
     113003 |      11 | New York | Este   |      109
     112987 |      13 | Atlanta  | Este   |      105
     113042 |      12 | Chicago  | Este   |      101
(12 rows)


-- 5.30- Transforma la següent consulta amb subconsultes a una consulta amb les mínimes subconsultes possibles.
-- SELECT num_clie, empresa, (SELECT nombre FROM repventas WHERE rep_clie = num_empl) AS rep_nombre FROM clientes WHERE rep_clie = ANY (SELECT num_empl FROM repventas WHERE ventas > (SELECT MAX(cuota) FROM repventas));
training=# SELECT num_clie, empresa, (SELECT nombre FROM repventas WHERE rep_clie = num_empl) AS rep_nombre FROM clientes WHERE rep_clie = ANY (SELECT num_empl FROM repventas WHERE ventas > (SELECT MAX(cuota) FROM repventas));
 num_clie |     empresa      | rep_nombre  
----------+------------------+-------------
     2122 | Three-Way Lines  | Bill Adams
     2103 | Acme Mfg.        | Bill Adams
     2119 | Solomon Inc.     | Mary Jones
     2108 | Holm & Landis    | Mary Jones
     2106 | Fred Lewis Corp. | Sue Smith
     2120 | Rico Enterprises | Sue Smith
     2114 | Orion Corp       | Sue Smith
     2123 | Carter & Sons    | Sue Smith
     2118 | Midwest Systems  | Larry Fitch
     2112 | Zetacorp         | Larry Fitch
(10 rows)

training=# select num_clie, empresa, nombre from clientes left join repventas on rep_clie=num_empl where ventas > (SELECT MAX(cuota) FROM repventas);
 num_clie |     empresa      |   nombre    
----------+------------------+-------------
     2122 | Three-Way Lines  | Bill Adams
     2103 | Acme Mfg.        | Bill Adams
     2119 | Solomon Inc.     | Mary Jones
     2108 | Holm & Landis    | Mary Jones
     2106 | Fred Lewis Corp. | Sue Smith
     2120 | Rico Enterprises | Sue Smith
     2114 | Orion Corp       | Sue Smith
     2123 | Carter & Sons    | Sue Smith
     2118 | Midwest Systems  | Larry Fitch
     2112 | Zetacorp         | Larry Fitch
(10 rows)

