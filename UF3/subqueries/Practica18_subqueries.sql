-- 1- Quins són els productes amb un preu inferior al 10% del preu del producte més car? I els d'un preu inferior al 5% del producte més car?
--El producto mas caro
training=# select id_fab, id_producto, precio from productos order by precio desc limit 1;
 id_fab | id_producto | precio  
--------+-------------+---------
 rei    | 2a44l       | 4500.00
(1 row)
training=# select max(precio) from productos;
   max   
---------
 4500.00

--precio inferior al 10% del mas caro
training=# select id_fab, id_producto from productos where precio < any(select max(precio)*0.10 from productos);
 id_fab | id_producto 
--------+-------------
 rei    | 2a45c
 qsa    | xk47 
 bic    | 41672
 aci    | 41003
 aci    | 41004
 imm    | 887p 
 qsa    | xk48 
 fea    | 112  
 imm    | 887h 
 bic    | 41089
 aci    | 41001
 qsa    | xk48a
 aci    | 41002
 aci    | 4100x
 fea    | 114  
 rei    | 2a44g
(16 rows)

--precio inferior al 5% del mas caro
training=# select id_fab, id_producto from productos where precio < any(select max(precio)*0.05 from productos);
 id_fab | id_producto 
--------+-------------
 rei    | 2a45c
 bic    | 41672
 aci    | 41003
 aci    | 41004
 qsa    | xk48 
 fea    | 112  
 imm    | 887h 
 aci    | 41001
 qsa    | xk48a
 aci    | 41002
 aci    | 4100x
(11 rows)


-- 2- Quin és el 2n client que compra més (en import)?
training=# select clie, sum(importe) from pedidos group by clie order by 2 desc;
 clie |   sum    
------+----------
 2112 | 47925.00
 2103 | 35582.00
 2117 | 31500.00
 2109 | 31350.00
 2107 | 23132.00
 2113 | 22500.00
 2114 | 22100.00
 2108 |  7255.00
 2111 |  6445.00
 2106 |  4026.00
 2102 |  3978.00
 2120 |  3750.00
 2118 |  3608.00
 2124 |  3082.00
 2101 |  1458.00
(15 rows)

training=# select clie, sum(importe) from pedidos group by clie order by 2 desc offset 1 limit 1;
 clie |   sum    
------+----------
 2103 | 35582.00
 
--subquery--
(1 row)
training=# select num_clie from clientes where num_clie in(select clie from pedidos group by clie order by sum(importe) desc offset 1 limit 1);
 num_clie 
----------
     2103
(1 row)




-- 3- Quin és el 3r client que compra menys (en import)?
training=# select clie, sum(importe) from pedidos group by clie order by 2 offset 2 limit 1;
 clie |   sum   
------+---------
 2118 | 3608.00
(1 row)


-- 4- Dels 10 productes més venuts, quin és el més car?
--los 10 mas vendidos:
training=# select fab, producto, sum(cant), (select precio from productos where id_fab=fab and id_producto=producto) from pedidos group by 1,2 order by 3 desc limit 10;
 fab | producto | sum | precio  
-----+----------+-----+---------
 aci | 41004    |  68 |  117.00
 aci | 41002    |  64 |   76.00
 aci | 41003    |  35 |  107.00
 rei | 2a45c    |  32 |   79.00
 aci | 4100x    |  30 |   25.00
 qsa | xk47     |  28 |  355.00
 imm | 775c     |  22 | 1425.00
 fea | 114      |  16 |  243.00
 rei | 2a44r    |  15 | 4500.00---> el más caro
 aci | 4100z    |  15 | 2500.00
(10 rows)

--precios--
training=# select (select precio from productos where id_fab=fab and id_producto=producto) from pedidos group by fab, producto order by sum(cant) desc limit 10;
 precio  
---------
  117.00
   76.00
  107.00
   79.00
   25.00
  355.00
 1425.00
  243.00
 4500.00
 2500.00
(10 rows)

--selecciona cada fab y producto, su maximo de idfab idproducto del max precio
training=# select fab,producto from pedidos where 
	fab = (select id_fab from productos group by id_fab, id_producto order by max(precio) desc limit 1) 
	and 
	producto = (select id_producto from productos group by id_fab, id_producto order by max(precio) desc limit 1) 
	group by 1,2 order by sum(cant) desc limit 10;
	
 fab | producto 
-----+----------
 rei | 2a44r
 
--concatenado-- 
(1 row)
training=# select id_fab, id_producto, precio from productos where (id_fab||id_producto) in (select fab||producto from pedidos group by fab, producto order by sum(cant) desc limit 10) order by precio desc limit 1;
 id_fab | id_producto | precio  
--------+-------------+---------
 rei    | 2a44r       | 4500.00
(1 row)


-- 5- Llista els  clients que tenen un total d'import gastat amb una diferència màxima del 40% de l'import que ha gastat el millor client.

training=# select clie from pedidos group by clie having sum(importe) >= (select sum(importe)*0.5 from pedidos group by clie order by sum(importe) desc limit 1);
 clie 
------
 2112
 2109
 2117
 2103
(4 rows)
