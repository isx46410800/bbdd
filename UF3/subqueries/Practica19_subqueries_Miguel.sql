-- 1- Llistar els venedors que treballen en oficines que superen el seu objectiu. Mostrar també les següents dades de l'oficina: 
-- ciutat i la diferència entre les vendes i l'objectiu. Ordenar el resultat per aquest últim valor. Proposa dos sentències SQL,
-- una amb subconsultes i una sense.

-- a) barrejant multitaula i subconsulta:
training=# select num_empl, nombre, repventas.ventas-(select objetivo from oficinas where oficina_rep=oficina) from repventas where repventas.ventas > any (select oficinas.objetivo from oficinas left join repventas on oficina_rep=oficina) order by 3 desc limit 1;
 num_empl |   nombre   | ?column? 
----------+------------+----------
      105 | Bill Adams | 17911.00
(1 row)


-- b) només amb multitaula:

training=# select num_empl, nombre, oficina, ciudad, repventas.ventas-oficinas.objetivo from repventas right join oficinas on oficina_rep=oficina where repventas.ventas > oficinas.objetivo;
 num_empl |   nombre   | oficina | ciudad  | ?column? 
----------+------------+---------+---------+----------
      105 | Bill Adams |      13 | Atlanta | 17911.00

-- 2- Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. Sense usar consultes multitaula.
training=# select nombre from repventas where oficina_rep is null or oficina_rep in(select oficina from oficinas where dir != (select num_empl from repventas where nombre='Larry Fitch'));
   nombre    
-------------
 Bill Adams
 Mary Jones
 Sam Clark
 Bob Smith
 Dan Roberts
 Tom Snyder
 Paul Cruz
(7 rows)

-- para ver que no es lo mismo dir que director
training=# select num_empl, director, (select dir from oficinas where oficina_rep=oficina) from repventas;
 num_empl | director | dir 
----------+----------+-----
      105 |      104 | 105
      109 |      106 | 106
      102 |      108 | 108
      106 |          | 106
      104 |      106 | 104
      101 |      104 | 104
      110 |      101 |    
      108 |      106 | 108
      103 |      104 | 104
      107 |      108 | 108
(10 rows)

training=# select num_empl, director, (select dir from oficinas where oficina_rep=oficina) from repventas where director != (select dir from oficinas where oficina_rep=oficina);
 num_empl | director | dir 
----------+----------+-----
      105 |      104 | 105
      104 |      106 | 104
      108 |      106 | 108
(3 rows)


-- 3- Llistar els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. Mostrant també la ciutat de l'oficina
-- on treballa l'empleat i l'identificador del director de la oficina. Proposa dos sentències SQL, una amb subconsultes i una sense.

-- a) barrejant multitaula i subconsulta:
training=# select nombre, (select dir from oficinas where oficina_rep=oficina), (select ciudad from oficinas where oficina_rep=oficina) from repventas where oficina_rep is null or oficina_rep in(select oficina from oficinas where dir != (select num_empl from repventas where nombre='Larry Fitch'));
   nombre    | dir |  ciudad  
-------------+-----+----------
 Bill Adams  | 105 | Atlanta
 Mary Jones  | 106 | New York
 Sam Clark   | 106 | New York
 Bob Smith   | 104 | Chicago
 Dan Roberts | 104 | Chicago
 Tom Snyder  |     | 
 Paul Cruz   | 104 | Chicago
(7 rows)



-- b) només amb multitaula
training=# select num_empl, nombre, ciudad, dir from repventas left join oficinas on oficina_rep=oficina where dir!=108 or oficina_rep is null;
 num_empl |   nombre    |  ciudad  | dir 
----------+-------------+----------+-----
      105 | Bill Adams  | Atlanta  | 105
      109 | Mary Jones  | New York | 106
      106 | Sam Clark   | New York | 106
      104 | Bob Smith   | Chicago  | 104
      101 | Dan Roberts | Chicago  | 104
      110 | Tom Snyder  |          |    
      103 | Paul Cruz   | Chicago  | 104
(7 rows)



-- 4- Llistar tots els clients que han realitzat comandes del productes de la família ACI Widgets entre gener i juny del 1990. Els productes de la famíla 
-- ACI Widgets són aquells que tenen identificador de fabricant "aci" i que l'identificador del producte comença per "4100".
training=# select distinct(num_clie), empresa from clientes left join pedidos on clie=num_clie where fab='aci' and producto like '4100%' and fecha_pedido>='01-01-1990' and fecha_pedido<='30-06-1990';
 num_clie |      empresa      
----------+-------------------
     2103 | Acme Mfg.
     2107 | Ace International
     2108 | Holm & Landis
     2111 | JCP Inc.
(4 rows)

training=# select num_clie, empresa from clientes where num_clie in (select clie from pedidos where fab='aci' and producto like '4100%' and fecha_pedido>='01-01-1990' and fecha_pedido<='30-06-1990');
 num_clie |      empresa      
----------+-------------------
     2103 | Acme Mfg.
     2107 | Ace International
     2108 | Holm & Landis
     2111 | JCP Inc.
(4 rows)


-- 5- Llistar els clients que no tenen cap comanda.
training=# select num_clie, empresa, count(clie) from clientes left join pedidos on clie=num_clie group by 1 having count(clie)=0;
 num_clie |     empresa     | count 
----------+-----------------+-------
     2115 | Smithson Corp.  |     0
     2105 | AAA Investments |     0
     2123 | Carter & Sons   |     0
     2119 | Solomon Inc.    |     0
     2122 | Three-Way Lines |     0
     2121 | QMA Assoc.      |     0
(6 rows)

training=# select num_clie, empresa from clientes where num_clie not in (select clie from pedidos group by clie);
 num_clie |     empresa     
----------+-----------------
     2123 | Carter & Sons
     2115 | Smithson Corp.
     2121 | QMA Assoc.
     2122 | Three-Way Lines
     2119 | Solomon Inc.
     2105 | AAA Investments
(6 rows)

training=# select num_clie, empresa from clientes where not exists (select clie from pedidos where clie=num_clie);
 num_clie |     empresa     
----------+-----------------
     2123 | Carter & Sons
     2115 | Smithson Corp.
     2121 | QMA Assoc.
     2122 | Three-Way Lines
     2119 | Solomon Inc.
     2105 | AAA Investments
(6 rows)


-- 6- Llistar els clients que tenen assignat el venedor que porta més temps a contractat.
training=# select empresa from clientes where rep_clie = (select num_empl from repventas order by contrato limit 1);
     empresa     
-----------------
 First Corp.
 Smithson Corp.
 AAA Investments
(3 rows)



training=# select nombre from repventas order by contrato;
    nombre     
---------------
 Dan Roberts
 Sue Smith
 Paul Cruz
 Bob Smith
 Bill Adams
 Sam Clark
 Nancy Angelli
 Larry Fitch
 Mary Jones
 Tom Snyder
(10 rows)

-- 7- Llistar els clients assignats a Sue Smith que no han fet cap comanda amb un import superior a 30000. Proposa una sentència SQL sense usar multitaula i una altre en que s'usi multitaula i subconsultes.
training=# select num_clie, empresa from clientes where rep_clie = (select num_empl from repventas where num_empl=102) and num_clie not in (select clie from pedidos where importe>30000);
 num_clie |     empresa      
----------+------------------
     2123 | Carter & Sons
     2114 | Orion Corp
     2120 | Rico Enterprises
     2106 | Fred Lewis Corp.
(4 rows)

training=# select num_clie, empresa from clientes where rep_clie = (select num_empl from repventas where num_empl=102) and not exists (select clie from pedidos where importe>30000 and clie=num_clie);
 num_clie |     empresa      
----------+------------------
     2123 | Carter & Sons
     2114 | Orion Corp
     2120 | Rico Enterprises
     2106 | Fred Lewis Corp.
(4 rows)

-- 8- Llistar l'identificador i el nom dels directors d'empleats que tenen més de 40 anys i que dirigeixen un venedor que té unes vendes superiors a la seva pròpia quota.
training=# select director.num_empl, director.nombre from repventas as director where director.num_empl = any(select empleado.director from repventas as empleado where empleado.ventas > director.cuota);
 num_empl |   nombre    
----------+-------------
      106 | Sam Clark
      104 | Bob Smith
      108 | Larry Fitch
(3 rows)

-- 9- Llista d'oficines on hi hagi algun venedor tal que la seva quota representi més del 50% de l'objectiu de l'oficina
training=# select oficina, ciudad from oficinas where 0.5*oficinas.objetivo < any(select cuota from repventas where oficina=oficina_rep);
 oficina |  ciudad  
---------+----------
      22 | Denver
      11 | New York
      13 | Atlanta
(3 rows)

**any es para alguno. menor que alguno de estos < any

-- 10- Llista d'oficines on tots els venedors tinguin la seva quota superior al 55% de l'objectiu de l'oficina.
training=# select oficina, ciudad from oficinas where 0.55*oficinas.objetivo < all(select cuota from repventas where oficina=oficina_rep);
 oficina | ciudad  
---------+---------
      22 | Denver
      13 | Atlanta
(2 rows)

**all para todos los participantes que cumplan esto < all

training=# select oficina, ciudad from oficinas where oficina not in(select oficina_rep from repventas where oficina=oficina_rep and cuota <=oficinas.objetivo*0.55);
 oficina | ciudad  
---------+---------
      22 | Denver
      13 | Atlanta
(2 rows)





