#EXAMEN BASE DE DADES PART A

1)
1.1) CON SUBQUERIES
training=# select id_fab, id_producto, (select count(num_pedido) as num_comandes from pedidos where fab=id_fab and producto=id_producto), (select count(clie) as clientes from pedidos where fab=id_fab and producto=id_producto), (select sum(importe) from pedidos where fab=id_fab and producto=id_producto) from productos where id_fab in (select fab from pedidos group by fab) or id_producto in (select producto from pedidos group by producto) or id_fab not in (select fab from pedidos group by fab) or id_producto not in (select producto from pedidos group by producto) order by 1,2;
 id_fab | id_producto | num_comandes | clientes |   sum    
--------+-------------+--------------+----------+----------
 aci    | 41001       |            0 |        0 |         
 aci    | 41002       |            2 |        2 |  4864.00
 aci    | 41003       |            1 |        1 |  3745.00
 aci    | 41004       |            3 |        3 |  7956.00
 aci    | 4100x       |            2 |        2 |   750.00
 aci    | 4100y       |            1 |        1 | 27500.00
 aci    | 4100z       |            2 |        2 | 37500.00
 bic    | 41003       |            2 |        2 |  1304.00
 bic    | 41089       |            0 |        0 |         
 bic    | 41672       |            0 |        0 |         
 fea    | 112         |            1 |        1 |  1480.00
 fea    | 114         |            2 |        2 |  3888.00
 imm    | 773c        |            1 |        1 |  2925.00
 imm    | 775c        |            1 |        1 | 31350.00
 imm    | 779c        |            2 |        2 |  9375.00
 imm    | 887h        |            0 |        0 |         
 imm    | 887p        |            0 |        0 |         
 imm    | 887x        |            0 |        0 |         
 qsa    | xk47        |            3 |        3 | 10006.00
 qsa    | xk48        |            0 |        0 |         
 qsa    | xk48a       |            0 |        0 |         
 rei    | 2a44g       |            1 |        1 |  2100.00
 rei    | 2a44l       |            1 |        1 | 31500.00
 rei    | 2a44r       |            2 |        2 | 67500.00
 rei    | 2a45c       |            2 |        2 |  2528.00
(25 rows)


1.2) CON JOINS
training=# select id_fab, id_producto, count(num_pedido) as num_comandes, count(clie) as clientes, sum(importe) from productos left join pedidos on id_fab=fab and id_producto=producto group by 1,2 order by 1,2;
 id_fab | id_producto | num_comandes | clientes |   sum    
--------+-------------+--------------+----------+----------
 aci    | 41001       |            0 |        0 |         
 aci    | 41002       |            2 |        2 |  4864.00
 aci    | 41003       |            1 |        1 |  3745.00
 aci    | 41004       |            3 |        3 |  7956.00
 aci    | 4100x       |            2 |        2 |   750.00
 aci    | 4100y       |            1 |        1 | 27500.00
 aci    | 4100z       |            2 |        2 | 37500.00
 bic    | 41003       |            2 |        2 |  1304.00
 bic    | 41089       |            0 |        0 |         
 bic    | 41672       |            0 |        0 |         
 fea    | 112         |            1 |        1 |  1480.00
 fea    | 114         |            2 |        2 |  3888.00
 imm    | 773c        |            1 |        1 |  2925.00
 imm    | 775c        |            1 |        1 | 31350.00
 imm    | 779c        |            2 |        2 |  9375.00
 imm    | 887h        |            0 |        0 |         
 imm    | 887p        |            0 |        0 |         
 imm    | 887x        |            0 |        0 |         
 qsa    | xk47        |            3 |        3 | 10006.00
 qsa    | xk48        |            0 |        0 |         
 qsa    | xk48a       |            0 |        0 |         
 rei    | 2a44g       |            1 |        1 |  2100.00
 rei    | 2a44l       |            1 |        1 | 31500.00
 rei    | 2a44r       |            2 |        2 | 67500.00
 rei    | 2a45c       |            2 |        2 |  2528.00
(25 rows)


2)
2.1) CON SUBQUERIES
training=# select distinct(num_clie), empresa, (select count(num_pedido) as num_aci_imm from pedidos where clie=num_clie and fab='aci' or fab='imm'), (select count(clie) from pedidos where clie=num_clie) from clientes where num_clie in (select clie from pedidos where clie=num_clie);
 num_clie |      empresa      | num_aci_imm | count 
----------+-------------------+-------------+-------
     2101 | Jones Mfg.        |           4 |     1
     2102 | First Corp.       |           5 |     1
     2103 | Acme Mfg.         |           8 |     4
     2106 | Fred Lewis Corp.  |           4 |     2
     2107 | Ace International |           5 |     2
     2108 | Holm & Landis     |           5 |     3
     2109 | Chen Associates   |           4 |     1
     2111 | JCP Inc.          |           6 |     3
     2112 | Zetacorp          |           4 |     2
     2113 | Ian & Schmidt     |           4 |     1
     2114 | Orion Corp        |           5 |     2
     2117 | J.P. Sinclair     |           4 |     1
     2118 | Midwest Systems   |           5 |     4
     2120 | Rico Enterprises  |           4 |     1
     2124 | Peter Brothers    |           4 |     2
(15 rows)

2.2) CON JOINS I SUBQUERIES
training=# select num_clie, empresa, (select sum(cant) as aci_imm from pedidos where num_clie=clie and (fab='aci' or fab='imm')), sum(cant) as total from clientes right join pedidos on clie=num_clie group by 1;
 num_clie |      empresa      | aci_imm | total 
----------+-------------------+---------+-------
     2109 | Chen Associates   |      22 |    22
     2118 | Midwest Systems   |      10 |    17
     2108 | Holm & Landis     |       9 |    19
     2112 | Zetacorp          |       3 |    13
     2101 | Jones Mfg.        |         |     6
     2114 | Orion Corp        |       6 |    26
     2124 | Peter Brothers    |         |    11
     2103 | Acme Mfg.         |      99 |    99
     2113 | Ian & Schmidt     |         |     5
     2102 | First Corp.       |      34 |    34
     2107 | Ace International |       9 |    17
     2117 | J.P. Sinclair     |         |     7
     2111 | JCP Inc.          |      59 |    65
     2120 | Rico Enterprises  |       2 |     2
     2106 | Fred Lewis Corp.  |         |    30
(15 rows)

3)
para ver los productos que no tienen ningun pedido:

NOT IN
training=# select id_fab from productos where id_fab not in (select fab from pedidos where id_fab=fab and id_producto=producto) group by 1;
 id_fab 
--------
 aci
 bic
 imm
 qsa
(4 rows)


NOT EXISTS
training=# select id_fab from productos where not exists (select fab from pedidos where id_fab=fab and id_producto=producto) group by 1;
 id_fab 
--------
 imm
 aci
 qsa
 bic
(4 rows)


3.1)
3.2)
training=# select id_fab, (select count(*) from productos p2 where p1.id_fab=p2.id_fab) as num_productes_fabrica, (select count(distinct producto) from pedidos where id_fab=fab) as num_productes_venuts, (select sum(existencias) from productos p3 where p1.id_fab=p3.id_fab) as stock, (select sum(cant) from pedidos ped where id_fab=ped.fab) as unitats_venudes from productos p1 where not exists (select fab from pedidos where p1.id_fab=fab and p1.id_producto=producto) group by 1;
 id_fab | num_productes_fabrica | num_productes_venuts | stock | unitats_venudes 
--------+-----------------------+----------------------+-------+-----------------
 imm    |                     6 |                    3 |   321 |              30
 aci    |                     7 |                    6 |   880 |             223
 qsa    |                     3 |                    2 |   278 |              32
 bic    |                     3 |                    1 |    81 |               2
(4 rows)

4)
4.1)
4.2) JOINS Y SUBQUERIES
training=# select nombre, count(num_pedido) as num_comandas, sum(cant) as num_unitats, count(distinct clie) as num_clients_venuts, (select count(rep_clie) as num_clients_representats from clientes where rep_clie=num_empl) from pedidos join repventas on rep=num_empl left join clientes on num_empl=rep_clie where oficina_rep=11 or oficina_rep=12 group by num_empl,rep;
   nombre    | num_comandas | num_unitats | num_clients_venuts | num_clients_representats 
-------------+--------------+-------------+--------------------+--------------------------
 Dan Roberts |            9 |         135 |                  3 |                        3
 Paul Cruz   |            6 |          90 |                  1 |                        3
 Sam Clark   |            4 |          26 |                  2 |                        2
 Mary Jones  |            4 |          26 |                  1 |                        2
(4 rows)

5)
5.1)
5.2)


