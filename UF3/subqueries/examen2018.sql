Examen per fer a l'ordinador. Cal entregar-lo al directori M2 de gandhi en un fitxer amb nom_cognoms.sql

Cada exercici val 2,5 punts.

1. Volem comprovar si tots els productes que s'han venut s'han cobrat segons el preu de catàleg (el de la taula productos) o si s'han cobrat a un preu inferior o superior.
Per això cal una consulta on surti el num_pedido, la descripció del producte venut, el preu que s'ha cobrat a la comanda, el preu de catàleg i la diferència entre aquests dos preus.

Fer el SELECT sense JOINS

 num_pedido |    descripcion    |     preu_comanda      | preu_cataleg |     diferencia     
 -----------------+---------------------+---------------------------+-------------------+--------------------
	...
       13003 | Riostra 2-Tm      | 1875.0000000000000000 |      1875.00 | 0.0000000000000000
     113069 | Riostra 1-Tm      | 1425.0000000000000000 |      1425.00 | 0.0000000000000000
     113007 | Riostra 1/2-Tm   |  975.0000000000000000  |       975.00  | 0.0000000000000000
	...


training=# select num_pedido, (select descripcion from productos where id_fab=fab and id_producto=producto), (select precio from productos where id_fab=fab and id_producto=producto), importe/cant as comanda, (importe/cant)-(select precio from productos where id_fab=fab and id_producto=producto) as diferencia from pedidos order by 5 desc;
 num_pedido |    descripcion    | precio  |        comanda        |      diferencia       
------------+-------------------+---------+-----------------------+-----------------------
     113051 |                   |         |  355.0000000000000000 |                      
     113049 | Reductor          |  355.00 |  388.0000000000000000 |   33.0000000000000000
     112989 | Bancada Motor     |  243.00 |  243.0000000000000000 |    0.0000000000000000
     112968 | Articulo Tipo 4   |  117.00 |  117.0000000000000000 |    0.0000000000000000
     110036 | Montador          | 2500.00 | 2500.0000000000000000 |    0.0000000000000000
     113045 | Bisagra Dcha.     | 4500.00 | 4500.0000000000000000 |    0.0000000000000000
     112963 | Articulo Tipo 4   |  117.00 |  117.0000000000000000 |    0.0000000000000000
     113013 | Manivela          |  652.00 |  652.0000000000000000 |    0.0000000000000000
     113058 | Cubierta          |  148.00 |  148.0000000000000000 |    0.0000000000000000
     112997 | Manivela          |  652.00 |  652.0000000000000000 |    0.0000000000000000
     112983 | Articulo Tipo 4   |  117.00 |  117.0000000000000000 |    0.0000000000000000
     113024 | Reductor          |  355.00 |  355.0000000000000000 |    0.0000000000000000
     113062 | Bancada Motor     |  243.00 |  243.0000000000000000 |    0.0000000000000000
     112979 | Montador          | 2500.00 | 2500.0000000000000000 |    0.0000000000000000
     113027 | Articulo Tipo 2   |   76.00 |   76.0000000000000000 |    0.0000000000000000
     113007 | Riostra 1/2-Tm    |  975.00 |  975.0000000000000000 |    0.0000000000000000
     113069 | Riostra 1-Tm      | 1425.00 | 1425.0000000000000000 |    0.0000000000000000
     113034 | V Stago Trinquete |   79.00 |   79.0000000000000000 |    0.0000000000000000
     112992 | Articulo Tipo 2   |   76.00 |   76.0000000000000000 |    0.0000000000000000
     112975 | Pasador Bisagra   |  350.00 |  350.0000000000000000 |    0.0000000000000000
     113055 | Ajustador         |   25.00 |   25.0000000000000000 |    0.0000000000000000
     113048 | Riostra 2-Tm      | 1875.00 | 1875.0000000000000000 |    0.0000000000000000
     112993 | V Stago Trinquete |   79.00 |   79.0000000000000000 |    0.0000000000000000
     113065 | Reductor          |  355.00 |  355.0000000000000000 |    0.0000000000000000
     113003 | Riostra 2-Tm      | 1875.00 | 1875.0000000000000000 |    0.0000000000000000
     113042 | Bisagra Dcha.     | 4500.00 | 4500.0000000000000000 |    0.0000000000000000
     113012 | Articulo Tipo 3   |  107.00 |  107.0000000000000000 |    0.0000000000000000
     112961 | Bisagra Izqda.    | 4500.00 | 4500.0000000000000000 |    0.0000000000000000
     113057 | Ajustador         |   25.00 |   25.0000000000000000 |    0.0000000000000000
     112987 | Extractor         | 2750.00 | 2500.0000000000000000 | -250.0000000000000000
(30 rows)








2. Volem veure quin client ha comprat més amb el total de les seves comandes i quin client ha comprat menys.
Per això volem una consulta amb 5 columnes :
Num_client, nom empresa, codi del seu representant, nom del representant i total d'import del client
Ordenar des del que ha comprat més al que ha comprat menys

Fer el SELECT sense JOINS

      2112 | Zetacorp   |      108 | Larry Fitch | 47925.00
     2101  | Jones Mfg. |      106 | Sam Clark  |  1458.00
--maximo
training=# select clie, sum(importe) from pedidos group by clie order by 2 desc limit 1;
 clie |   sum    
------+----------
 2112 | 47925.00
(1 row)

--minimo
training=# select clie, sum(importe) from pedidos group by clie order by 2 limit 1;
 clie |   sum   
------+---------
 2101 | 1458.00
(1 row)


training=# select num_clie, empresa, rep_clie, (select nombre from repventas where rep_clie=num_empl), (select sum(importe) from pedidos where clie=num_clie) from clientes where num_clie = (select clie from pedidos group by clie order by sum(importe) desc limit 1);
 num_clie | empresa  | rep_clie |   nombre    |   sum    
----------+----------+----------+-------------+----------
     2112 | Zetacorp |      108 | Larry Fitch | 47925.00
(1 row)
training=# select num_clie, empresa, rep_clie, (select nombre from repventas where rep_clie=num_empl), (select sum(importe) from pedidos where clie=num_clie) from clientes where num_clie = (select clie from pedidos group by clie order by sum(importe) limit 1);
 num_clie |  empresa   | rep_clie |  nombre   |   sum   
----------+------------+----------+-----------+---------
     2101 | Jones Mfg. |      106 | Sam Clark | 1458.00
(1 row)

--TODO EN UNO--
num_clie = (select...) or num_clie = (select...)
training=# select num_clie, empresa, rep_clie, (select nombre from repventas where rep_clie=num_empl), (select sum(importe) from pedidos where clie=num_clie) from clientes where num_clie = (select clie from pedidos group by clie order by sum(importe) desc limit 1) or num_clie = (select clie from pedidos group by clie order by sum(importe) limit 1) order by 5 desc;
 num_clie |  empresa   | rep_clie |   nombre    |   sum    
----------+------------+----------+-------------+----------
     2112 | Zetacorp   |      108 | Larry Fitch | 47925.00
     2101 | Jones Mfg. |      106 | Sam Clark   |  1458.00
(2 rows)



3- Volem veure, pels cadascun dels clients que han fet més d'una comanda, les seves comandes  ordenades pel client, pel representant assignat al client,
 pel representant que ha fet la comanda i per l'import de comanda.ha fet el representant que ha fet la venta.
Així, la consulta mostrarà les columnes:
num_client, empresa, codi del representant assignat, nom del representant assignat, representant que ha fet la venta, nom del representant que ha fet la venta, import

Fer el SELECT sense JOINS


...
 2107 | Ace International |      110 | Tom Snyder    | 110 | Tom Snyder    | 22500.00
 2108 | Holm & Landis     |      109 | Mary Jones    | 101 | Dan Roberts   |   150.00
 2108 | Holm & Landis     |      109 | Mary Jones    | 109 | Mary Jones    |  1480.00
 2108 | Holm & Landis     |      109 | Mary Jones    | 109 | Mary Jones    |  5625.00
...

--miramos los clientes con mas de una comannda ordenadas por clientes 
training=# select clie, count(clie) from pedidos group by clie having count(clie)>1 order by 1;
 clie | count 
------+-------
 2103 |     4
 2106 |     2
 2107 |     2
 2108 |     3
 2111 |     3
 2112 |     2
 2114 |     2
 2118 |     4
 2124 |     2
(9 rows)

--HAY QUE CAMBIAR LA TABLA MADRE DE CLIENTES A PEDIDOS--
training=# select num_clie, empresa, rep_clie from clientes where num_clie in (select clie from pedidos group by clie having count(clie)>1) order by num_clie;
 num_clie |      empresa      | rep_clie 
----------+-------------------+----------
     2103 | Acme Mfg.         |      105
     2106 | Fred Lewis Corp.  |      102
     2107 | Ace International |      110
     2108 | Holm & Landis     |      109
     2111 | JCP Inc.          |      103
     2112 | Zetacorp          |      108
     2114 | Orion Corp        |      102
     2118 | Midwest Systems   |      108
     2124 | Peter Brothers    |      107
(9 rows)

training=# select num_clie, empresa, rep_clie, (select nombre from repventas where rep_clie=num_empl), (select rep from pedidos where rep=num_empl), (select nombre from repventas where num_empl=select rep_clie from clientes where clie=num_clie), (select importe from pedidos where clie=num_clie) from clientes where num_clie in (select clie from pedidos group by clie having count(clie)>1) order by num_clie;

















4-  Mostrar el total de comandes venut a cada oficina comptant només les comandes dels venedors que n'han fet més de dues.
Mostrar el codi d'oficina, la ciutat i l'import total per oficina

Fer el SELECT sense JOINS


...
      12 | Chicago     | 26628.00
      13 | Atlanta      | 42077.00
...



