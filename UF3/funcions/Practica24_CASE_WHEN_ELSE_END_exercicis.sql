-- --------------------------------------------
--    Funcions d'usuari
-- --------------------------------------------

-- 1.- Llistar l'identificador i el nom dels representants de vendes. Mostrar un camp anomenat "result" que mostri 0 si la quota és inferior a les vendes, 
--en cas contrari ha de mostrar 1 a no ser que sigui director d'oficina, en aquest cas ha de mostrar 2.

training=# select num_empl, nombre, case when cuota<ventas then 0 when num_empl in(select dir from oficinas) then 2 else 1 end from repventas;
 num_empl |    nombre     | case 
----------+---------------+------
      105 | Bill Adams    |    0
      109 | Mary Jones    |    0
      102 | Sue Smith     |    0
      106 | Sam Clark     |    0
      104 | Bob Smith     |    2
      101 | Dan Roberts   |    0
      110 | Tom Snyder    |    1
      108 | Larry Fitch   |    0
      103 | Paul Cruz     |    0
      107 | Nancy Angelli |    1
(10 rows)

-- 2.- Llistar tots els productes amb totes les seves dades afegint un nou camp anomenat "div". El camp div ha de contenir el resultat de la divisió entre el preu i les existències. 
--En cas de divisió per zero, es canviarà el resultat a 0.

training=# select productos.*, case when existencias=0 then 0 else round(precio/existencias,2) end as div from productos;
 id_fab | id_producto |    descripcion    | precio  | existencias |  div   
--------+-------------+-------------------+---------+-------------+--------
 rei    | 2a45c       | V Stago Trinquete |   79.00 |         210 |   0.38
 aci    | 4100y       | Extractor         | 2750.00 |          25 | 110.00
 qsa    | xk47        | Reductor          |  355.00 |          38 |   9.34
 bic    | 41672       | Plate             |  180.00 |           0 |      0
 imm    | 779c        | Riostra 2-Tm      | 1875.00 |           9 | 208.33
 aci    | 41003       | Articulo Tipo 3   |  107.00 |         207 |   0.52
 aci    | 41004       | Articulo Tipo 4   |  117.00 |         139 |   0.84
 bic    | 41003       | Manivela          |  652.00 |           3 | 217.33
 imm    | 887p        | Perno Riostra     |  250.00 |          24 |  10.42
 qsa    | xk48        | Reductor          |  134.00 |         203 |   0.66
 rei    | 2a44l       | Bisagra Izqda.    | 4500.00 |          12 | 375.00
 fea    | 112         | Cubierta          |  148.00 |         115 |   1.29
 imm    | 887h        | Soporte Riostra   |   54.00 |         223 |   0.24
 bic    | 41089       | Retn              |  225.00 |          78 |   2.88
 aci    | 41001       | Articulo Tipo 1   |   55.00 |         277 |   0.20
 imm    | 775c        | Riostra 1-Tm      | 1425.00 |           5 | 285.00
 aci    | 4100z       | Montador          | 2500.00 |          28 |  89.29
 qsa    | xk48a       | Reductor          |  117.00 |          37 |   3.16
 aci    | 41002       | Articulo Tipo 2   |   76.00 |         167 |   0.46
 rei    | 2a44r       | Bisagra Dcha.     | 4500.00 |          12 | 375.00
 imm    | 773c        | Riostra 1/2-Tm    |  975.00 |          28 |  34.82
 aci    | 4100x       | Ajustador         |   25.00 |          37 |   0.68
 fea    | 114         | Bancada Motor     |  243.00 |          15 |  16.20
 imm    | 887x        | Retenedor Riostra |  475.00 |          32 |  14.84
 rei    | 2a44g       | Pasador Bisagra   |  350.00 |          14 |  25.00
(25 rows)



-- 3.- Afegir una condició a la sentència de l'exercici anterior per tal de nomès mostrar aquells productes que el valor del camp div és menor a 1.

training=# select productos.*, case when existencias=0 then 0 else round(precio/existencias,2) end as div from productos where case when existencias=0 then 0 else precio/existencias end <1;
 id_fab | id_producto |    descripcion    | precio | existencias | div  
--------+-------------+-------------------+--------+-------------+------
 rei    | 2a45c       | V Stago Trinquete |  79.00 |         210 | 0.38
 bic    | 41672       | Plate             | 180.00 |           0 |    0
 aci    | 41003       | Articulo Tipo 3   | 107.00 |         207 | 0.52
 aci    | 41004       | Articulo Tipo 4   | 117.00 |         139 | 0.84
 qsa    | xk48        | Reductor          | 134.00 |         203 | 0.66
 imm    | 887h        | Soporte Riostra   |  54.00 |         223 | 0.24
 aci    | 41001       | Articulo Tipo 1   |  55.00 |         277 | 0.20
 aci    | 41002       | Articulo Tipo 2   |  76.00 |         167 | 0.46
 aci    | 4100x       | Ajustador         |  25.00 |          37 | 0.68
(9 rows)


-- 4.- Donat l'identificador d'un client que retorni la importància del client, és a dir, el percentatge dels imports de les comandes del client respecte al total dels imports de les comandes.
training=# select sum(importe) / (select sum(importe) from pedidos) *100 from pedidos group by clie order by 1 desc;
        ?column?         
-------------------------
 19.34870463601826469300
 14.36547956930207395500
 12.71745844620918806100
 12.65689912027485859400
  9.33905551675272819800
  9.08389889014942004400
  8.92240735432454146500
  2.92905273102373521800
  2.60203237097835609700
  1.62541230807740289300
  1.60603332377841746400
  1.51398314835823667400
  1.45665365314040477900
  1.24429228353068944800
  0.58863664808168241900
(15 rows)

training=# create function importancia(cliente smallint) returns numeric as $$ select sum(importe) / (select sum(importe) from pedidos) *100 from pedidos where clie=$1; $$ language sql;
CREATE FUNCTION
training=# select importancia(2113::smallint);
      importancia       
------------------------
 9.08389889014942004400
(1 row)

-- 5.- Calcular el que s'ha deixat de cobrar per a un producte determinat.
-- És a dir, la diferència que hi ha entre el que val i el que li hem cobrat
-- al total de clients.

training=# create function descuento(f char(3), p char(5)) returns numeric as $$ 
select precio * sum(cant) - sum(importe) from pedidos 
join productos on fab=id_fab and producto=id_producto where fab=$1 and producto=$2 
group by id_fab, id_producto; 
$$ language sql;
CREATE FUNCTION
--ponemos id_fab, id_producto dentro de la funcion y asi coge todos los campos de esa tabla
training=# select id_fab, id_producto, descuento(id_fab,id_producto) from productos;
 id_fab | id_producto | descuento 
--------+-------------+-----------
 rei    | 2a45c       |      0.00
 aci    | 4100y       |   2750.00
 qsa    | xk47        |    -66.00
 bic    | 41672       |          
 imm    | 779c        |      0.00
 aci    | 41003       |      0.00
 aci    | 41004       |      0.00
 bic    | 41003       |      0.00
 imm    | 887p        |          
 qsa    | xk48        |          
 rei    | 2a44l       |      0.00
 fea    | 112         |      0.00
 imm    | 887h        |          
 bic    | 41089       |          
 aci    | 41001       |          
 imm    | 775c        |      0.00
 aci    | 4100z       |      0.00
 qsa    | xk48a       |          
 aci    | 41002       |      0.00
 rei    | 2a44r       |      0.00
 imm    | 773c        |      0.00
 aci    | 4100x       |      0.00
 fea    | 114         |      0.00
 imm    | 887x        |          
 rei    | 2a44g       |      0.00
(25 rows)

--llamandolo solo para un producto
training=# select descuento('rei'::text, '2a45c'::text);
 descuento 
-----------
      0.00
(1 row)



-- 6.- Crea una funció que si li passem dos columnes com les de "ventas" i "cuota". Ha de retornar una columna amb el valor de restar la quota a les ventes.
training=# create function resta(ventas numeric, cuota numeric) returns numeric as $$ select $1 -$2 $$ language sql;
CREATE FUNCTION
training=# select resta(ventas, cuota) from repventas;
   resta    
------------
   17911.00
   92725.00
  124050.00
   24912.00
  -57406.00
    5673.00
           
   11865.00
   11775.00
 -113958.00
(10 rows)

training=# create function resta2(ventas numeric, cuota numeric) returns numeric as $$ select ventas - cuota from repventas where ventas=$1 and cuota=$2 $$ language sql;
CREATE FUNCTION
training=# select resta2(ventas, cuota) from repventas;
   resta2   
------------
   17911.00
   92725.00
  124050.00
   24912.00
  -57406.00
    5673.00
           
   11865.00
   11775.00
 -113958.00
(10 rows)





-- 7.- Fes una funció que donat un identificador de representant de vendes retorni l'identificador dels clients que té assignats amb el seu límit de crèdit.
training=# create function identificador(rep_clie integer) returns numeric as $$ select limite_credito from clientes where rep_clie=$1 $$ language sql;
CREATE FUNCTION

training=# select identificador(rep_clie) from clientes;

 identificador 
---------------
      50000.00
      65000.00
      50000.00
      40000.00
      35000.00
      65000.00
      65000.00
      50000.00
      50000.00
      40000.00
      40000.00
      55000.00
      65000.00
      50000.00
      40000.00
      40000.00
      55000.00
      50000.00
      20000.00
      50000.00
      65000.00
(21 rows)

training=# create table cliente_credito_table (cliente smallint, credito numeric(8,2));
CREATE TABLE
training=# create function clientes_rep(smallint) returns setof cliente_credito_table as $$ select num_clie, limite_credito from clientes where rep_clie=$1; $$ language sql;
CREATE FUNCTION

training=# select clientes_rep(num_empl) from repventas;
  clientes_rep   
-----------------
 (2103,50000.00)
 (2122,30000.00)
 (2108,55000.00)
 (2119,25000.00)
 (2123,40000.00)
 (2114,20000.00)
 (2120,50000.00)
 (2106,65000.00)
 (2101,65000.00)
 (2117,35000.00)
 (2113,20000.00)
 (2102,65000.00)
 (2115,20000.00)
 (2105,45000.00)
 (2107,35000.00)
 (2112,50000.00)
 (2118,60000.00)
 (2111,50000.00)
 (2121,45000.00)
 (2109,25000.00)
 (2124,40000.00)
(21 rows)

CREATE TYPE
training=# create function clientes_rep_type(smallint) returns setof cliente_credito_type as $$ select num_clie, limite_credito from clientes where rep_clie=$1; $$ language sql;
CREATE FUNCTION
training=# select clientes_rep_type(num_empl) from repventas;
 clientes_rep_type 
-------------------
 (2103,50000.00)
 (2122,30000.00)
 (2108,55000.00)
 (2119,25000.00)
 (2123,40000.00)
 (2114,20000.00)
 (2120,50000.00)
 (2106,65000.00)
 (2101,65000.00)
 (2117,35000.00)
 (2113,20000.00)
 (2102,65000.00)
 (2115,20000.00)
 (2105,45000.00)
 (2107,35000.00)
 (2112,50000.00)
 (2118,60000.00)
 (2111,50000.00)
 (2121,45000.00)
 (2109,25000.00)
 (2124,40000.00)
(21 rows)



-- 8.- Crear una funció promig_anual(venedor, anyp) que retorni el promig d'imports de comandes del venedor durant aquell any.
training=# create function promig_anual(vendedor smallint, anyo int) returns numeric as $$ select avg(importe) from pedidos where rep=$1 and date_part('year',fecha_pedido)=$2; $$ language sql;
CREATE FUNCTION
training=# select promig_anual(108::smallint,1990);
     promig_anual      
-----------------------
 9645.5000000000000000
(1 row)


-- 9.- Crear una funció max_promig_anual(anyp) que retorni el màxim dels promitjos anuals de tots els venedors. Useu la funció de l'exercici anterior.


-- 10.- Creeu una funció promig_anual_tots(anyp) que retorni el promig anual de cada venedor durant l'any indicat. Useu funcions creades en els exercicis anteriors.


-- 11.- Feu una funció que retorni tots els codis dels clients que no hagin comprat res durant el mes introduït.


-- 12.- Funció anomenada MaximMes a la que se li passa un any i retorna el mes en el que hi ha hagut les màximes vendes (imports totals del mes).


-- 13.- a) Crear una funció baixa_rep que doni de baixa el venedor que se li passa per paràmetre i reassigni tots els seus clients al venedor que tingui menys clients assignats (si hi ha empat, a qualsevol d'ells).
--      b) Com usarieu la funció per donar de baixa els 3 venedors que fa més temps que no han fet cap venda?


-- 14.- Crear una funció anomenada "nclientes" que donat un identificador d'un representant de vendes ens retorni el nombre de clients que te assignats. Si l'entrada és nul·la s'ha de retornar un valor nul. 


-- 15.- Crear una funció anomenada "natendidos" que donat un identificador d'un representant de vendes ens retorni el nombre de clients diferents que ha atès. Si l'entrada és nul·la s'ha de retornar un valor nul.


-- 16.- Crear una funció anomenada "totalimportes" que donat un identificador d'un representant de vendes ens retorni la suma dels imports de les seves comandes. Si l'entrada és nul·la s'ha de retornar un valor nul.


-- 17.- Crear una funció anomenada "informe_rep" que ens retorni una taula amb l'identificador del representant de vendes,
-- el resultat de "nclientes", "natendidos" i "totalimportes". Si a la funció se li passa un identificador de representant
-- de vendes només ha de retornar la informació relativa a aquest representant de vendes. En cas de passar un valor nul a
-- la funció aquesta ha de donar la informació de tots els representants de vendes.   


-- 18.- Crear una funció i les funcions auxiliars convenients per obtenir la següent informació:
-- Donat l'identificador d'un producte obtenir una taula amb les següents dades:
--  * Identificador de producte i fabricant.
--  * Nombre de representants de vendes que han venut aquest producte.
--  * Nombre de clients que han comprat el producte.
--  * Mitjana de l'import de les comandes d'aquest producte.
--  * Quantitat mínima i quantitat màxima que s'ha demanat del producte en una sola comanda.


-- 19.- Crear una funció que donada una oficina ens retorni una taula identificant els productes i mostrant la quantitat d'aquest producte que s'ha venut a l'oficina.


-- 20.- Crear les funcions necessàries per aconseguir el següent resultat:
-- Cridant resumen_cliente() ha de retornar una taula amb la amb els identificadors dels clients, la suma de les seves compres, nombre de comandes realitzades i nombre de representants de vendes que l'han atès.
-- Cridant resumen_cliente(num_clie) ha de retornar una taula amb els identificador dels representants de vendes i el nombre de comandes que ha realitzat el client amb aquest representant de vendes.
-- Cridant resumen_cliente(num_clie, num_empl) ha de retornar una taula amb el nombre de productes diferents que ha demanat el client especificat al representant de vendes especificat i la mitja de l'import de les comandes que ha realitzat el client especificat al representant de vendes especificat. 


-- 21.- Crear una funció el_millor() que retorni totes les dades del venedor que ha venut més (major total d'imports) durant l'any en curs.


-- 22.- Calcular el descompte fet a un client concret, respecte totes les comandes del client.
-- Cal crear dos funcions auxiliars:
-- La primera obtindrà el total dels imports de les comandes d'un client determinat.
-- La segona serà una funció preu de "comanda abstracta" que necessitarà el producte en qüestió i la quantitat de productes demanats.


