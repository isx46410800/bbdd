--1. Donar data i hora actual del sistema en format dd/MM/aaaa-hh:mm:ss

training=# select to_char(current_timestamp, 'dd/MM/YYYY-hh:mi:ss');
       to_char       
---------------------
 10/04/2019-11:54:58

training=# select to_char(clock_timestamp(), 'dd/MM/YYYY-hh:mi:ss');
       to_char       
---------------------
 10/04/2019-11:55:35
(1 row)

--2. Donar hora actual en format hh:mm:ss
training=# select to_char(current_timestamp, 'hh:mi:ss');
 to_char  
----------
 11:56:07
(1 row)


--3. Determinar la durada de cada contracte fins avui en dies. S'ha de mostrar el nom del treballador, la data del contracte i la durada.
training=# select nombre, contrato, age(contrato) from repventas;
    nombre     |  contrato  |           age            
---------------+------------+--------------------------
 Bill Adams    | 1988-02-12 | 31 years 1 mon 27 days
 Mary Jones    | 1989-10-12 | 29 years 5 mons 29 days
 Sue Smith     | 1986-12-10 | 32 years 4 mons
 Sam Clark     | 1988-06-14 | 30 years 9 mons 26 days
 Bob Smith     | 1987-05-19 | 31 years 10 mons 22 days
 Dan Roberts   | 1986-10-20 | 32 years 5 mons 21 days
 Tom Snyder    | 1990-01-13 | 29 years 2 mons 28 days
 Larry Fitch   | 1989-10-12 | 29 years 5 mons 29 days
 Paul Cruz     | 1987-03-01 | 32 years 1 mon 9 days
 Nancy Angelli | 1988-11-14 | 30 years 4 mons 26 days
(10 rows)

--##solo dias
training=# select nombre, contrato, current_date-contrato as dias from repventas;
    nombre     |  contrato  | dias  
---------------+------------+-------
 Bill Adams    | 1988-02-12 | 11380
 Mary Jones    | 1989-10-12 | 10772
 Sue Smith     | 1986-12-10 | 11809
 Sam Clark     | 1988-06-14 | 11257
 Bob Smith     | 1987-05-19 | 11649
 Dan Roberts   | 1986-10-20 | 11860
 Tom Snyder    | 1990-01-13 | 10679
 Larry Fitch   | 1989-10-12 | 10772
 Paul Cruz     | 1987-03-01 | 11728
 Nancy Angelli | 1988-11-14 | 11104
(10 rows)


--4. Determinar la suma total de les durades de tots els contractes dels treballadors en dies fins avui.
training=# select sum(current_timestamp - contrato) from repventas;
            sum             
----------------------------
 113150 days 93:11:36.34737
(1 row)

training=# select sum(current_date - contrato) from repventas;
  sum   
--------
 113150
(1 row)


--5. Determinar la diferència de temps treballat entre cadascun dels treballadors amb el treballador més antic. S'ha de mostrar el nom del treballador, la data del contracte i la diferència.
training=# select nombre, contrato, contrato-(select min(contrato) as diferencia from repventas) from repventas;
    nombre     |  contrato  | ?column? 
---------------+------------+----------
 Bill Adams    | 1988-02-12 |      480
 Mary Jones    | 1989-10-12 |     1088
 Sue Smith     | 1986-12-10 |       51
 Sam Clark     | 1988-06-14 |      603
 Bob Smith     | 1987-05-19 |      211
 Dan Roberts   | 1986-10-20 |        0
 Tom Snyder    | 1990-01-13 |     1181
 Larry Fitch   | 1989-10-12 |     1088
 Paul Cruz     | 1987-03-01 |      132
 Nancy Angelli | 1988-11-14 |      756
(10 rows)

training=# select nombre, contrato, age(contrato,(select min(contrato) as diferencia from repventas)) from repventas;
    nombre     |  contrato  |           age           
---------------+------------+-------------------------
 Bill Adams    | 1988-02-12 | 1 year 3 mons 23 days
 Mary Jones    | 1989-10-12 | 2 years 11 mons 23 days
 Sue Smith     | 1986-12-10 | 1 mon 21 days
 Sam Clark     | 1988-06-14 | 1 year 7 mons 25 days
 Bob Smith     | 1987-05-19 | 6 mons 30 days
 Dan Roberts   | 1986-10-20 | 00:00:00
 Tom Snyder    | 1990-01-13 | 3 years 2 mons 24 days
 Larry Fitch   | 1989-10-12 | 2 years 11 mons 23 days
 Paul Cruz     | 1987-03-01 | 4 mons 12 days
 Nancy Angelli | 1988-11-14 | 2 years 25 days
(10 rows)

--para que no salga la persona que cogemos referencia (0:0:0)
training=# select nombre, contrato, age(contrato,(select min(contrato) as diferencia from repventas)) from repventas where contrato <> (select min(contrato) from repventas);
    nombre     |  contrato  |           age           
---------------+------------+-------------------------
 Bill Adams    | 1988-02-12 | 1 year 3 mons 23 days
 Mary Jones    | 1989-10-12 | 2 years 11 mons 23 days
 Sue Smith     | 1986-12-10 | 1 mon 21 days
 Sam Clark     | 1988-06-14 | 1 year 7 mons 25 days
 Bob Smith     | 1987-05-19 | 6 mons 30 days
 Tom Snyder    | 1990-01-13 | 3 years 2 mons 24 days
 Larry Fitch   | 1989-10-12 | 2 years 11 mons 23 days
 Paul Cruz     | 1987-03-01 | 4 mons 12 days
 Nancy Angelli | 1988-11-14 | 2 years 25 days
(9 rows)


--6. Calcular el nombre de comandes fetes el desembre pels representants de vendes contractats el mes de febrer.
training=# select count(num_pedido) from pedidos left join repventas on rep=num_empl where extract(month from fecha_pedido) = 12 and extract(month from contrato) = 2;
 count 
-------
     3
(1 row)

training=# select nombre, count(num_pedido) from pedidos left join repventas on rep=num_empl where extract(month from fecha_pedido) = 12 and extract(month from contrato) = 2 group by 1;
   nombre   | count 
------------+-------
 Bill Adams |     3
(1 row)

training=# select count(num_pedido) from pedidos left join repventas on rep=num_empl where date_part('month',fecha_pedido) = 12 and date_part('month',contrato) = 2;
 count 
-------
     3
(1 row)

--7. Llistar el número de treballadors que s'han contractat per a cada mes de l'any. El llistat ha d'estar ordenat pel mes. Ha de tenir el següent format:

 mes_de_contractació | numero_contractes 
---------------------+------------------
                   1 |                1
                   2 |                1
                   3 |                1
...

training=# select extract(month from contrato) as mes_contratacio, count(*) as numero_contractes from repventas group by 1 order by 1;
 mes_contratacio | numero_contractes 
-----------------+-------------------
               1 |                 1
               2 |                 1
               3 |                 1
               5 |                 1
               6 |                 1
              10 |                 3
              11 |                 1
              12 |                 1
(8 rows)


--8.- Mostrar el nom dels venedors i la data del seu contracte. La data s'ha de mostrar usant el següent format:
	dia_de_la_setmana  dia_numèric, mes_numeric del any_4digits
	per exemple:
	   Bill Adams    | Friday 12, 02 del 1988
	   
training=# select nombre, to_char(contrato, 'Day dd, mm "del" yyyy') from repventas;
    nombre     |          to_char          
---------------+---------------------------
 Bill Adams    | Friday    12, 02 del 1988
 Mary Jones    | Thursday  12, 10 del 1989
 Sue Smith     | Wednesday 10, 12 del 1986
 Sam Clark     | Tuesday   14, 06 del 1988
 Bob Smith     | Tuesday   19, 05 del 1987
 Dan Roberts   | Monday    20, 10 del 1986
 Tom Snyder    | Saturday  13, 01 del 1990
 Larry Fitch   | Thursday  12, 10 del 1989
 Paul Cruz     | Sunday    01, 03 del 1987
 Nancy Angelli | Monday    14, 11 del 1988
(10 rows)

--9.- LListar el nom dels treballadors i la data del seu contracte mostrant-la amb el següent format:
		Dia_de_la_setmana dia_mes_numeric, mes_text del any_4digits
	per exemple
                Bill Adams    | Friday    12, February del 1988
                
training=# select nombre, to_char(contrato, 'Day dd, Month "del" yyyy') from repventas;
    nombre     |             to_char              
---------------+----------------------------------
 Bill Adams    | Friday    12, February  del 1988
 Mary Jones    | Thursday  12, October   del 1989
 Sue Smith     | Wednesday 10, December  del 1986
 Sam Clark     | Tuesday   14, June      del 1988
 Bob Smith     | Tuesday   19, May       del 1987
 Dan Roberts   | Monday    20, October   del 1986
 Tom Snyder    | Saturday  13, January   del 1990
 Larry Fitch   | Thursday  12, October   del 1989
 Paul Cruz     | Sunday    01, March     del 1987
 Nancy Angelli | Monday    14, November  del 1988
(10 rows)

training=# select nombre, to_char(contrato, 'Day dd, month "del" yyyy') from repventas;
    nombre     |             to_char              
---------------+----------------------------------
 Bill Adams    | Friday    12, february  del 1988
 Mary Jones    | Thursday  12, october   del 1989
 Sue Smith     | Wednesday 10, december  del 1986
 Sam Clark     | Tuesday   14, june      del 1988
 Bob Smith     | Tuesday   19, may       del 1987
 Dan Roberts   | Monday    20, october   del 1986
 Tom Snyder    | Saturday  13, january   del 1990
 Larry Fitch   | Thursday  12, october   del 1989
 Paul Cruz     | Sunday    01, march     del 1987
 Nancy Angelli | Monday    14, november  del 1988
(10 rows)

training=# select nombre, to_char(contrato, 'Day dd, Mon "del" yyyy') from repventas;
    nombre     |          to_char           
---------------+----------------------------
 Bill Adams    | Friday    12, Feb del 1988
 Mary Jones    | Thursday  12, Oct del 1989
 Sue Smith     | Wednesday 10, Dec del 1986
 Sam Clark     | Tuesday   14, Jun del 1988
 Bob Smith     | Tuesday   19, May del 1987
 Dan Roberts   | Monday    20, Oct del 1986
 Tom Snyder    | Saturday  13, Jan del 1990
 Larry Fitch   | Thursday  12, Oct del 1989
 Paul Cruz     | Sunday    01, Mar del 1987
 Nancy Angelli | Monday    14, Nov del 1988
(10 rows)
                
                
--10.-  Cal fer un llistat dels productes dels quals les existències  són inferiors al total d'unitats venudes d'aquell producte els darrers 60 dies, a comptar des de la data actual. 
Cal mostrar els codis de fabricant i de producte, les existències, i les unitats totals venudes dels darrers 60 dies.

--solo cambiar el numero segun los dias que queramos
training=# select *, (select sum(cant) from pedidos where id_fab=fab and id_producto=producto and (current_date - fecha_pedido) < 10900) from productos where existencias < (select sum(cant) from pedidos where id_fab=fab and id_producto=producto and (current_date - fecha_pedido) < 10900);
 id_fab | id_producto |  descripcion  | precio  | existencias | sum 
--------+-------------+---------------+---------+-------------+-----
 imm    | 775c        | Riostra 1-Tm  | 1425.00 |           5 |  22
 rei    | 2a44r       | Bisagra Dcha. | 4500.00 |          12 |  15
 fea    | 114         | Bancada Motor |  243.00 |          15 |  16
(3 rows)


--11- Com l'exercici 29 però en comptes de 60 dies ara es volen dos mesos naturals: productes venuts durant el mes 
actual o durant l'anterior (si no som a fi de mes seràn menys de 60 dies).'

--calculo de mes actual y el anterior
training=# select date_part('month', current_date), date_part('month', current_date)-1;
 date_part | ?column? 
-----------+----------
         4 |        3
(1 row)


training=# select *, (select sum(cant) from pedidos where id_fab=fab and id_producto=producto) from productos 
where existencias < (select sum(cant) from pedidos where id_fab=fab and id_producto=producto) and 
(extract(month from current_date)>1 and (extract(month from fecha_pedido)=extract(month from current_date)
 or extract(month from fecha_pedido)=extract(month from current_date)-1)) 
 or (extract(month from current_date)=1 and (extract(month from fecha_pedido)=12 or extract(month from fecha_pedido)=1))));


training=# select *, (select sum(cant) from pedidos where id_fab=fab and id_producto=producto) from productos 
where existencias < (select sum(cant) from pedidos where id_fab=fab and id_producto=producto) and 
(extract(month from current_date)>1 and (extract(month from fecha_pedido)=extract(month from current_date) or 
extract(month from fecha_pedido)=extract(month from current_date)-1)) or (extract(month from current_date)=1 and (extract(month from fecha_pedido)=12 or extract(month from fecha_pedido)=1))));
