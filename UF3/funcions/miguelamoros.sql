--EXAMEN BASE DE DADES--
--MIGUEL AMOROS ISX46410800--

--EXERCCICI 1--
training=# select to_char(fecha_pedido,'Day "("d")"') as nom_dia, sum(importe) as import_total from pedidos group by 1, to_char(fecha_pedido, 'd') order by to_char(fecha_pedido, 'd');
    nom_dia    | import_total 
---------------+--------------
 Sunday    (1) |     63528.00
 Monday    (2) |      8313.00
 Tuesday   (3) |     26730.00
 Wednesday (4) |      4056.00
 Thursday  (5) |     28498.00
 Friday    (6) |    100330.00
 Saturday  (7) |     16236.00
(7 rows)


--EXERCICI 2--
training=# select (select num_clie from clientes where num_clie=clie) as codi_client, (select empresa from clientes where num_clie=clie) as nom_empresa, upper(id_fab || '-' || id_producto) as codi_producte, substr(descripcion,1,10) as descripcion, round(importe/cant,2) as mitja_promig_client, (select round(sum(importe)/sum(cant),2) from pedidos where fab=id_fab and producto=id_producto) as mitja_aprox_tots, precio as precio_cataleg from productos join pedidos on id_fab=fab and id_producto=producto order by id_fab, id_producto;
 codi_client |    nom_empresa    | codi_producte | descripcion | mitja_promig_client | mitja_aprox_tots | precio_cataleg 
-------------+-------------------+---------------+-------------+---------------------+------------------+----------------
        2103 | Acme Mfg.         | ACI-41002     | Articulo T  |               76.00 |            76.00 |          76.00
        2118 | Midwest Systems   | ACI-41002     | Articulo T  |               76.00 |            76.00 |          76.00
        2111 | JCP Inc.          | ACI-41003     | Articulo T  |              107.00 |           107.00 |         107.00
        2103 | Acme Mfg.         | ACI-41004     | Articulo T  |              117.00 |           117.00 |         117.00
        2103 | Acme Mfg.         | ACI-41004     | Articulo T  |              117.00 |           117.00 |         117.00
        2102 | First Corp.       | ACI-41004     | Articulo T  |              117.00 |           117.00 |         117.00
        2108 | Holm & Landis     | ACI-4100X     | Ajustador   |               25.00 |            25.00 |          25.00
        2111 | JCP Inc.          | ACI-4100X     | Ajustador   |               25.00 |            25.00 |          25.00
        2103 | Acme Mfg.         | ACI-4100Y     | Extractor   |             2500.00 |          2500.00 |        2750.00
        2107 | Ace International | ACI-4100Z     | Montador    |             2500.00 |          2500.00 |        2500.00
        2114 | Orion Corp        | ACI-4100Z     | Montador    |             2500.00 |          2500.00 |        2500.00
        2124 | Peter Brothers    | BIC-41003     | Manivela    |              652.00 |           652.00 |         652.00
        2118 | Midwest Systems   | BIC-41003     | Manivela    |              652.00 |           652.00 |         652.00
        2108 | Holm & Landis     | FEA-112       | Cubierta    |              148.00 |           148.00 |         148.00
        2124 | Peter Brothers    | FEA-114       | Bancada Mo  |              243.00 |           243.00 |         243.00
        2101 | Jones Mfg.        | FEA-114       | Bancada Mo  |              243.00 |           243.00 |         243.00
        2112 | Zetacorp          | IMM-773C      | Riostra 1/  |              975.00 |           975.00 |         975.00
        2109 | Chen Associates   | IMM-775C      | Riostra 1-  |             1425.00 |          1425.00 |        1425.00
        2120 | Rico Enterprises  | IMM-779C      | Riostra 2-  |             1875.00 |          1875.00 |        1875.00
        2108 | Holm & Landis     | IMM-779C      | Riostra 2-  |             1875.00 |          1875.00 |        1875.00
        2106 | Fred Lewis Corp.  | QSA-XK47      | Reductor    |              355.00 |           357.36 |         355.00
        2114 | Orion Corp        | QSA-XK47      | Reductor    |              355.00 |           357.36 |         355.00
        2118 | Midwest Systems   | QSA-XK47      | Reductor    |              388.00 |           357.36 |         355.00
        2111 | JCP Inc.          | REI-2A44G     | Pasador Bi  |              350.00 |           350.00 |         350.00
        2117 | J.P. Sinclair     | REI-2A44L     | Bisagra Iz  |             4500.00 |          4500.00 |        4500.00
        2112 | Zetacorp          | REI-2A44R     | Bisagra Dc  |             4500.00 |          4500.00 |        4500.00
        2113 | Ian & Schmidt     | REI-2A44R     | Bisagra Dc  |             4500.00 |          4500.00 |        4500.00
        2106 | Fred Lewis Corp.  | REI-2A45C     | V Stago Tr  |               79.00 |            79.00 |          79.00
        2107 | Ace International | REI-2A45C     | V Stago Tr  |               79.00 |            79.00 |          79.00
(29 rows)

--EXERCICI 3--
training=# select date_part('year', fecha_pedido) as any, date_part('month', fecha_pedido) as mes, count(distinct clie) as clients, count(num_pedido) as comandes, sum(importe) as total from pedidos group by 2,1 order by 1,2;
 any  | mes | clients | comandes |  total   
------+-----+---------+----------+----------
 1989 |   1 |       1 |        1 |  1896.00
 1989 |  10 |       2 |        2 | 18978.00
 1989 |  11 |       1 |        1 |   760.00
 1989 |  12 |       3 |        5 | 65078.00
 1990 |   1 |       9 |       10 | 49393.00
 1990 |   2 |       8 |       10 | 80236.00
 1990 |   3 |       1 |        1 | 31350.00
(7 rows)



--EXERCICI 4--
training=# select id_fab, id_producto, descripcion, substr(descripcion,1,1) as primera_lletra, right(descripcion,1) as ultima_lletra, case when sum(cant)>0 then sum(cant) else 0 end as unitats_venudes from productos left join pedidos on id_fab=fab and id_producto=producto where lower(substr(descripcion,1,1))=right(descripcion,1) group by id_fab, id_producto;
 id_fab | id_producto | descripcion | primera_lletra | ultima_lletra | unitats_venudes 
--------+-------------+-------------+----------------+---------------+-----------------
 qsa    | xk47        | Reductor    | R              | r             |              28
 qsa    | xk48        | Reductor    | R              | r             |               0
 qsa    | xk48a       | Reductor    | R              | r             |               0
(3 rows)




