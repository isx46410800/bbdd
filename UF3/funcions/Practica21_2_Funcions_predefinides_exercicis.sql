-- --------------------------------------------
--    Funcions predefinides
-- --------------------------------------------

-- 10.1- Mostreu la longitud de la cadena "hola que tal"
training=# select length('hola que tal');
training=# select char_length('hola que tal');
 char_length 
-------------
          12
(1 row)


-- 10.2- Mostreu la longitud dels valors del camp "id_producto".
training=# select char_length(id_producto) from productos;
 char_length 
-------------
           5
           5
           4
           5
           4
           5
           5
           5
           4
           4
           5
           3
           4
           5
           5
           4
           5
           5
           5
           5
           4
           5
           3
           4
           5
(25 rows)


-- 10.3- Mostrar la longitud dels valors del camp "descripcion".
training=# select char_length(descripcion) from productos;
 char_length 
-------------
          17
           9
           8
           5
          12
          15
          15
           8
          13
           8
          14
           8
          15
           4
          15
          12
           8
           8
          15
          13
          14
           9
          13
          17
          15
(25 rows)


-- 10.4- Mostreu els noms dels venedors en majúscules.
training=# select upper(nombre) from repventas;
     upper     
---------------
 BILL ADAMS
 MARY JONES
 SUE SMITH
 SAM CLARK
 BOB SMITH
 DAN ROBERTS
 TOM SNYDER
 LARRY FITCH
 PAUL CRUZ
 NANCY ANGELLI
(10 rows)


-- 10.5- Mostreu els noms dels venedors en minúscules.
training=# select lower(nombre) from repventas;
     lower     
---------------
 bill adams
 mary jones
 sue smith
 sam clark
 bob smith
 dan roberts
 tom snyder
 larry fitch
 paul cruz
 nancy angelli
(10 rows)


-- 10.6- Trobeu on és la posició de l'espai en blanc de la cadena 'potser 7'.
training=# select position(' ' in 'pot ser');
 position 
----------
        4
(1 row)

training=# select strpos('pot ser', ' ');
 strpos 
--------
      4
(1 row)



-- 10.7- Volem mostrar el nom, només el nom dels venedors sense el cognom, en majúscules.
training=# select split_part(nombre, ' ', 1) from repventas;
 split_part 
------------
 Bill
 Mary
 Sue
 Sam
 Bob
 Dan
 Tom
 Larry
 Paul
 Nancy
(10 rows)

training=# select upper(split_part(nombre, ' ', 1)) from repventas;
 upper 
-------
 BILL
 MARY
 SUE
 SAM
 BOB
 DAN
 TOM
 LARRY
 PAUL
 NANCY
(10 rows)

training=# select upper (left(nombre, strpos(nombre, ' '))) from repventas;
 upper  
--------
 BILL 
 MARY 
 SUE 
 SAM 
 BOB 
 DAN 
 TOM 
 LARRY 
 PAUL 
 NANCY 
(10 rows)

training=# select substr (nombre, 1, strpos(nombre, ' ')) from repventas; <----------  del 1 hasta el espacio de nombre
 substr 
--------
 Bill 
 Mary 
 Sue 
 Sam 
 Bob 
 Dan 
 Tom 
 Larry 
 Paul 
 Nancy 
(10 rows)



-- 10.8- Crear una vista que mostri l'identificador dels representants de vendes i en columnes separades el nom i el cognom.
training=# select num_empl, split_part(nombre, ' ', 1) as nombre, split_part(nombre, ' ', 2) as apellidos from repventas;
 num_empl | nombre | apellidos 
----------+--------+-----------
      105 | Bill   | Adams
      109 | Mary   | Jones
      102 | Sue    | Smith
      106 | Sam    | Clark
      104 | Bob    | Smith
      101 | Dan    | Roberts
      110 | Tom    | Snyder
      108 | Larry  | Fitch
      103 | Paul   | Cruz
      107 | Nancy  | Angelli
(10 rows)

training=# select num_empl, left(nombre, strpos(nombre, ' ')), right(nombre, -strpos(nombre, ' ')) from repventas;
--#negativo el right para que coja bien el apellido
training=# select num_empl, left(nombre, strpos(nombre, ' ')), right(nombre, -strpos(nombre, ' ')) from repventas;
 num_empl |  left  |  right  
----------+--------+---------
      105 | Bill   | Adams
      109 | Mary   | Jones
      102 | Sue    | Smith
      106 | Sam    | Clark
      104 | Bob    | Smith
      101 | Dan    | Roberts
      110 | Tom    | Snyder
      108 | Larry  | Fitch
      103 | Paul   | Cruz
      107 | Nancy  | Angelli
(10 rows)

training=# select num_empl, substr(nombre, 1, strpos(nombre, ' ')), substr(nombre, strpos(nombre, ' ')+1, length(nombre)) from repventas;
--+1 porque es desde la posicion inicial(no contar el espacio) hasta el final
 num_empl | substr | substr  
----------+--------+---------
      105 | Bill   | Adams
      109 | Mary   | Jones
      102 | Sue    | Smith
      106 | Sam    | Clark
      104 | Bob    | Smith
      101 | Dan    | Roberts
      110 | Tom    | Snyder
      108 | Larry  | Fitch
      103 | Paul   | Cruz
      107 | Nancy  | Angelli
(10 rows)


-- 10.9- Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'B. Adams'.
 training=# select substr(nombre, 1,1) || '. ' || substr(nombre, strpos(nombre, ' '), length(nombre)) from repventas;
  ?column?   
-------------
 B.  Adams
 M.  Jones
 S.  Smith
 S.  Clark
 B.  Smith
 D.  Roberts
 T.  Snyder
 L.  Fitch
 P.  Cruz
 N.  Angelli
(10 rows)

training=# select left(left(nombre, strpos(nombre, ' ')), 1) || '. ' || right(nombre, -strpos(nombre, ' ')) from repventas;
  ?column?  
------------
 B. Adams
 M. Jones
 S. Smith
 S. Clark
 B. Smith
 D. Roberts
 T. Snyder
 L. Fitch
 P. Cruz
 N. Angelli
(10 rows)


-- 10.10- Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'Adams, Bill'.
--CON SPLIT--
training=# select split_part(nombre, ' ', 2) || ', ' || split_part(nombre, ' ',1) from repventas;
    ?column?     
-----------------
  Adams, Bill
  Jones, Mary
  Smith, Sue
  Clark, Sam
  Smith, Bob
  Roberts, Dan
  Snyder, Tom
  Fitch, Larry
  Cruz, Paul
  Angelli, Nancy
(10 rows)

--CON SUBSTR--
training=# select substr(nombre, strpos(nombre, ' '), length(nombre)) || ', ' || substr(nombre, 1, strpos(nombre, ' ')) from repventas;
     ?column?     
------------------
  Adams, Bill 
  Jones, Mary 
  Smith, Sue 
  Clark, Sam 
  Smith, Bob 
  Roberts, Dan 
  Snyder, Tom 
  Fitch, Larry 
  Cruz, Paul 
  Angelli, Nancy 
(10 rows)

--CON RIGHT/LEFT--
training=# select right(nombre, -strpos(nombre, ' ')) || ', ' || left(nombre, strpos(nombre, ' ')) from repventas;
    ?column?     
-----------------
 Adams, Bill 
 Jones, Mary 
 Smith, Sue 
 Clark, Sam 
 Smith, Bob 
 Roberts, Dan 
 Snyder, Tom 
 Fitch, Larry 
 Cruz, Paul 
 Angelli, Nancy 
(10 rows)



POSICION DEL ESPACIO: strpos(nombre, ' ')
SUBSTRING: COGER UN RANGO DE CARACTERES SEGUN UNA POSICION INDICADA


-- 10.11- Volem mostrar el camp descripcion de la taula productos però que en comptes de sortir espais en blanc, volem subratllats ('_').
training=# select replace(descripcion, ' ', '_') from productos;
      replace      
-------------------
 V_Stago_Trinquete
 Extractor
 Reductor
 Plate
 Riostra_2-Tm
 Articulo_Tipo_3
 Articulo_Tipo_4
 Manivela
 Perno_Riostra
 Reductor
 Bisagra_Izqda.
 Cubierta
 Soporte_Riostra
 Retn
 Articulo_Tipo_1
 Riostra_1-Tm
 Montador
 Reductor
 Articulo_Tipo_2
 Bisagra_Dcha.
 Riostra_1/2-Tm
 Ajustador
 Bancada_Motor
 Retenedor_Riostra
 Pasador_Bisagra
(25 rows)


-- 10.12- Volem treure per pantalla una columna, que conté el nom i les vendes, amb els següent estil:
--   vendes dels empleats
-- ---------------------------
--  Bill Adams..... 367911,00
--  Mary Jones..... 392725,00
--  Sue Smith...... 474050,00
--  Sam Clark...... 299912,00
--  Bob Smith...... 142594,00
--  Dan Roberts.... 305673,00
--  Tom Snyder.....  75985,00
--  Larry Fitch.... 361865,00
--  Paul Cruz...... 286775,00
--  Nancy Angelli.. 186042,00
-- (10 rows)
training=# select rpad(nombre, 20, '.') || ventas from repventas;
--rellenado con RPAD
           ?column?            
-------------------------------
 Bill Adams..........367911.00
 Mary Jones..........392725.00
 Sue Smith...........474050.00
 Sam Clark...........299912.00
 Bob Smith...........142594.00
 Dan Roberts.........305673.00
 Tom Snyder..........75985.00
 Larry Fitch.........361865.00
 Paul Cruz...........286775.00
 Nancy Angelli.......186042.00
(10 rows)

--alineado con LPAD
training=# select rpad(nombre, 20, '.') || lpad(ventas::text, 15, '.') from repventas;
              ?column?               
-------------------------------------
 Bill Adams................367911.00
 Mary Jones................392725.00
 Sue Smith.................474050.00
 Sam Clark.................299912.00
 Bob Smith.................142594.00
 Dan Roberts...............305673.00
 Tom Snyder.................75985.00
 Larry Fitch...............361865.00
 Paul Cruz.................286775.00
 Nancy Angelli.............186042.00
(10 rows)


-- 10.13- Treieu per pantalla el temps total que fa que estan contractats els treballadors, ordenat pels cognoms dels treballadors amb un estil semblant al següent:
--       nombre  |     tiempo_trabajando
--    -----------+-------------------------
--    Mary Jones | 13 years 4 months 6 days
training=# select nombre, age(contrato::timestamp) from repventas order by split_part(nombre, ' ', 2);
    nombre     |           age            
---------------+--------------------------
 Bill Adams    | 31 years 1 mon 20 days
 Nancy Angelli | 30 years 4 mons 19 days
 Sam Clark     | 30 years 9 mons 19 days
 Paul Cruz     | 32 years 1 mon 2 days
 Larry Fitch   | 29 years 5 mons 22 days
 Mary Jones    | 29 years 5 mons 22 days
 Dan Roberts   | 32 years 5 mons 14 days
 Sue Smith     | 32 years 3 mons 24 days
 Bob Smith     | 31 years 10 mons 15 days
 Tom Snyder    | 29 years 2 mons 21 days
(10 rows)

 
-- 10.14- Cal fer un llistat dels productes dels quals les existències són inferiors al total d'unitats venudes d'aquell producte els darrers 60 dies, 
--a comptar des de la data actual. Cal mostrar els codis de fabricant i de producte, les existències, i les unitats totals venudes dels darrers 60 dies.
--SUBQUERIES
training=# select *, (select sum(cant) from pedidos where id_fab=fab and id_producto=producto and (current_date-fecha_pedido)<10670) from productos where existencias < (select sum(cant) from pedidos where id_producto=producto and id_fab=fab);
 id_fab | id_producto |  descripcion  | precio  | existencias | sum 
--------+-------------+---------------+---------+-------------+-----
 imm    | 775c        | Riostra 1-Tm  | 1425.00 |           5 |  22
 rei    | 2a44r       | Bisagra Dcha. | 4500.00 |          12 |  15
(2 rows)

--CON JOINS
training=# select productos.*, sum(cant) from productos join pedidos on id_fab=fab and id_producto=producto where (current_date-fecha_pedido)<10670 group by id_fab, id_producto having existencias < sum(cant);
 id_fab | id_producto |  descripcion  | precio  | existencias | sum 
--------+-------------+---------------+---------+-------------+-----
 imm    | 775c        | Riostra 1-Tm  | 1425.00 |           5 |  22
 rei    | 2a44r       | Bisagra Dcha. | 4500.00 |          12 |  15
(2 rows)



-- 10.15- Com l'exercici anterior però en comptes de 60 dies ara es volen aquells productes venuts durant el mes actual o durant l'anterior.
training=# select pedidos.* from pedidos where extract(month from fecha_pedido) = 4 or extract(month from fecha_pedido) = 3;
 num_pedido | fecha_pedido | clie | rep | fab | producto | cant | importe  
------------+--------------+------+-----+-----+----------+------+----------
     113069 | 1990-03-02   | 2109 | 107 | imm | 775c     |   22 | 31350.00
(1 row)


training=# select pedidos.* from pedidos where extract(month from fecha_pedido) = extract(month from current_date) or extract(month from fecha_pedido) = extract(month from current_date)-1;
 num_pedido | fecha_pedido | clie | rep | fab | producto | cant | importe  
------------+--------------+------+-----+-----+----------+------+----------
     113069 | 1990-03-02   | 2109 | 107 | imm | 775c     |   22 | 31350.00
     
training=# select productos.*, sum(cant) from productos join pedidos on id_fab=fab and id_producto=producto where extract(month from fecha_pedido) = extract(month from current_date) or extract(month from fecha_pedido) = extract(month from current_date)-1 group by id_fab, id_producto having existencias < sum(cant);;
 id_fab | id_producto | descripcion  | precio  | existencias | sum 
--------+-------------+--------------+---------+-------------+-----
 imm    | 775c        | Riostra 1-Tm | 1425.00 |           5 |  22
(1 row)


-- 10.16- Per fer un estudi previ de la campanya de Nadal es vol un llistat on, per cada any del qual hi hagi comandes a la base de dades, 
--el nombre de clients diferents que hagin fet comandes en el mes de desembre d'aquell any. Cal mostrar l'any i el número de clients, ordenat ascendent per anys.
--para ver los años y meses y las comandas
training=# select extract(year from fecha_pedido), extract(month from fecha_pedido), clie from pedidos order by clie,1,2;
 date_part | date_part | clie 
-----------+-----------+------
      1990 |         1 | 2101
      1989 |        10 | 2102
      1989 |        12 | 2103
      1989 |        12 | 2103
      1989 |        12 | 2103
      1990 |         1 | 2103
      1989 |         1 | 2106
      1990 |         2 | 2106
      1990 |         1 | 2107
      1990 |         1 | 2107
      1990 |         1 | 2108
      1990 |         2 | 2108
      1990 |         2 | 2108
      1990 |         3 | 2109
      1989 |        12 | 2111
      1990 |         1 | 2111
      1990 |         2 | 2111
      1990 |         1 | 2112
      1990 |         2 | 2112
      1990 |         2 | 2113
      1989 |        10 | 2114
      1990 |         1 | 2114
      1989 |        12 | 2117
      1989 |        11 | 2118
      1990 |         1 | 2118
      1990 |         2 | 2118
      1990 |         2 | 2118
      1990 |         2 | 2120
      1990 |         1 | 2124
      1990 |         2 | 2124
(30 rows)

--solo mes 12
training=# select extract(year from fecha_pedido), extract(month from fecha_pedido), clie from pedidos where extract(month from fecha_pedido)=12 order by clie,1,2;
 date_part | date_part | clie 
-----------+-----------+------
      1989 |        12 | 2103
      1989 |        12 | 2103
      1989 |        12 | 2103
      1989 |        12 | 2111
      1989 |        12 | 2117
(5 rows)

--unicos de dos maneras, distinct o group by
training=# select distinct extract(year from fecha_pedido), extract(month from fecha_pedido), clie from pedidos where extract(month from fecha_pedido)=12 order by clie,1,2;
 date_part | date_part | clie 
-----------+-----------+------
      1989 |        12 | 2103
      1989 |        12 | 2111
      1989 |        12 | 2117
(3 rows)
training=# select extract(year from fecha_pedido), extract(month from fecha_pedido), clie from pedidos where extract(month from fecha_pedido)=12 group by 1,2,3 order by clie,1,2;
 date_part | date_part | clie 
-----------+-----------+------
      1989 |        12 | 2103
      1989 |        12 | 2111
      1989 |        12 | 2117
(3 rows)


--resultado final
training=# select distinct extract(year from fecha_pedido), (select count(distinct clie) from pedidos where extract(month from fecha_pedido) = 12 and extract(year from fecha_pedido)=extract(year from ped.fecha_pedido)) from pedidos ped order by 1;
 date_part | count 
-----------+-------
      1989 |     3
      1990 |     0
(2 rows)

--con DATE_PART
training=# select distinct date_part('year',fecha_pedido), (select count(distinct clie) from pedidos where date_part('month', fecha_pedido) = 12 and date_part('year', fecha_pedido)=date_part('year', ped.fecha_pedido)) from pedidos ped order by 1;
 date_part | count 
-----------+-------
      1989 |     3
      1990 |     0
(2 rows)

training=# select date_part('year',fecha_pedido), count(distinct clie) from pedidos where date_part('month',fecha_pedido)=12 group by 1;
 date_part | count 
-----------+-------
      1989 |     3
(1 row)


-- 10.17- Llisteu codi(s) i descripció dels productes. La descripció ha d'aperèixer en majúscules. Ha d'estar ordenat per la longitud de les descripcions (les més curtes primer).
training=# select id_fab, id_producto, upper(descripcion), length(descripcion) from productos order by length(descripcion);
 id_fab | id_producto |       upper       | length 
--------+-------------+-------------------+--------
 bic    | 41089       | RETN              |      4
 bic    | 41672       | PLATE             |      5
 qsa    | xk48a       | REDUCTOR          |      8
 qsa    | xk47        | REDUCTOR          |      8
 bic    | 41003       | MANIVELA          |      8
 qsa    | xk48        | REDUCTOR          |      8
 fea    | 112         | CUBIERTA          |      8
 aci    | 4100z       | MONTADOR          |      8
 aci    | 4100x       | AJUSTADOR         |      9
 aci    | 4100y       | EXTRACTOR         |      9
 imm    | 779c        | RIOSTRA 2-TM      |     12
 imm    | 775c        | RIOSTRA 1-TM      |     12
 imm    | 887p        | PERNO RIOSTRA     |     13
 fea    | 114         | BANCADA MOTOR     |     13
 rei    | 2a44r       | BISAGRA DCHA.     |     13
 imm    | 773c        | RIOSTRA 1/2-TM    |     14
 rei    | 2a44l       | BISAGRA IZQDA.    |     14
 rei    | 2a44g       | PASADOR BISAGRA   |     15
 aci    | 41003       | ARTICULO TIPO 3   |     15
 aci    | 41004       | ARTICULO TIPO 4   |     15
 imm    | 887h        | SOPORTE RIOSTRA   |     15
 aci    | 41001       | ARTICULO TIPO 1   |     15
 aci    | 41002       | ARTICULO TIPO 2   |     15
 imm    | 887x        | RETENEDOR RIOSTRA |     17
 rei    | 2a45c       | V STAGO TRINQUETE |     17
(25 rows)



-- 10.18- LListar el nom dels treballadors i la data del seu contracte mostrant-la amb el següent format:
-- Dia_de_la_setmana dia_mes_numeric, mes_text del any_4digits
-- per exemple:
-- Bill Adams    | Friday    12, February del 1988
training=# select nombre, to_char(contrato, 'day DD, month') || 'del ' || to_char(contrato, 'YYYY') from repventas;
    nombre     |            ?column?             
---------------+---------------------------------
 Bill Adams    | friday    12, february del 1988
 Mary Jones    | thursday  12, october  del 1989
 Sue Smith     | wednesday 10, december del 1986
 Sam Clark     | tuesday   14, june     del 1988
 Bob Smith     | tuesday   19, may      del 1987
 Dan Roberts   | monday    20, october  del 1986
 Tom Snyder    | saturday  13, january  del 1990
 Larry Fitch   | thursday  12, october  del 1989
 Paul Cruz     | sunday    01, march    del 1987
 Nancy Angelli | monday    14, november del 1988
(10 rows)



-- 10.19- Modificar els imports de les comandes que s'han realitzat durant l'estiu augmentant-lo un 20% i arrodonint a l'alça el resultat.
--cambiamos los meses para que den resultados
training=# select importe, ceiling(importe + importe*0.2) from pedidos where extract(month from fecha_pedido) in (2,7,8);
 importe  | ceiling 
----------+---------
  1420.00 |    1704
 45000.00 |   54000
  1480.00 |    1776
  2430.00 |    2916
   150.00 |     180
  3750.00 |    4500
  2130.00 |    2556
   776.00 |     932
   600.00 |     720
 22500.00 |   27000
(10 rows)


-- 10.20- Mostar les dades de les oficines llistant en primera instància aquelles oficines que tenen una desviació entre vendes i objectius més gran.
training=# select *, round(objetivo/ventas,2) from oficinas order by objetivo/ventas;
 oficina |   ciudad    | region | dir | objetivo  |  ventas   | round 
---------+-------------+--------+-----+-----------+-----------+-------
      11 | New York    | Este   | 106 | 575000.00 | 692637.00 |  0.83
      21 | Los Angeles | Oeste  | 108 | 725000.00 | 835915.00 |  0.87
      13 | Atlanta     | Este   | 105 | 350000.00 | 367911.00 |  0.95
      12 | Chicago     | Este   | 104 | 800000.00 | 735042.00 |  1.09
      22 | Denver      | Oeste  | 108 | 300000.00 | 186042.00 |  1.61
(5 rows)


-- 10.21- Llistar les dades d'aquells representants de vendes que tenen un identificador senar i són directors d'algun representants de vendes.
training=# select dir.* from repventas rep join repventas dir on (rep.director=dir.num_empl) where mod(dir.num_empl,2)=1 group by dir.num_empl;
 num_empl |   nombre    | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+-------------+------+-------------+------------+------------+----------+-----------+-----------
      101 | Dan Roberts |   45 |          12 | Rep Ventas | 1986-10-20 |      104 | 300000.00 | 305673.00
(1 row)


