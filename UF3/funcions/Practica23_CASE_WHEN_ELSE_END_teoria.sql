# Funcions d'usuari

## CASE - WHEN - ELSE - END

Especifica una estructura condicional dins d'una sentència SQL.

**Exemples**

Llistar els noms dels venedors i una columna que dessigni si són majors de 40 anys o no amb el text "Adulto" o "Joven" respectivament

```SQL
SELECT nombre, 
	CASE 
		WHEN edad>40 THEN 'Adulto' 
		ELSE 'Joven'
    END
FROM repventas;
```
    nombre     |  case  
---------------+--------
 Bill Adams    | Joven
 Mary Jones    | Joven
 Sue Smith     | Adulto
 Sam Clark     | Adulto
 Bob Smith     | Joven
 Dan Roberts   | Adulto
 Tom Snyder    | Adulto
 Larry Fitch   | Adulto
 Paul Cruz     | Joven
 Nancy Angelli | Adulto
(10 rows)


Llistar els codis dels productes denotant si el preu és menor a 100 ('muy barato'), entre 100 i 1000 ('barato'), entre 1000 i 3000 ('caro') o més gran a 3000 ('muy caro').

```SQL
SELECT id_fab, id_producto, precio,
	CASE 
		WHEN precio < 100 THEN 'muy barato' 
		WHEN precio < 1000 THEN 'barato' 
        WHEN precio < 3000 THEN 'caro' 
        ELSE 'Muy caro' 
    END 
FROM productos;
```
 id_fab | id_producto | precio  |    case    
--------+-------------+---------+------------
 rei    | 2a45c       |   79.00 | muy barato
 aci    | 4100y       | 2750.00 | caro
 qsa    | xk47        |  355.00 | barato
 bic    | 41672       |  180.00 | barato
 imm    | 779c        | 1875.00 | caro
 aci    | 41003       |  107.00 | barato
 aci    | 41004       |  117.00 | barato
 bic    | 41003       |  652.00 | barato
 imm    | 887p        |  250.00 | barato
 qsa    | xk48        |  134.00 | barato
 rei    | 2a44l       | 4500.00 | Muy caro
 fea    | 112         |  148.00 | barato
 imm    | 887h        |   54.00 | muy barato
 bic    | 41089       |  225.00 | barato
 aci    | 41001       |   55.00 | muy barato
 imm    | 775c        | 1425.00 | caro
 aci    | 4100z       | 2500.00 | caro
 qsa    | xk48a       |  117.00 | barato
 aci    | 41002       |   76.00 | muy barato
 rei    | 2a44r       | 4500.00 | Muy caro
 imm    | 773c        |  975.00 | barato
 aci    | 4100x       |   25.00 | muy barato
 fea    | 114         |  243.00 | barato
 imm    | 887x        |  475.00 | barato
 rei    | 2a44g       |  350.00 | barato
(25 rows)


Llistar els productes i especificar si són molt venuts (més de 2 comandes) o no. Ho especificarem posant el text 'muy vendido' o 'poco vendido'.

```SQL
SELECT id_fab, id_producto, 
	CASE 
		WHEN count(*) > 2 THEN 'muy vendido' 
		ELSE 'poco vendido' 
    END 
FROM productos 
JOIN pedidos ON id_fab = fab AND id_producto = producto 
GROUP BY id_fab, id_producto;
```
 id_fab | id_producto | count |     case     
--------+-------------+-------+--------------
 aci    | 41001       |     1 | poco vendido
 fea    | 112         |     1 | poco vendido
 imm    | 773c        |     1 | poco vendido
 imm    | 775c        |     1 | poco vendido
 imm    | 887h        |     1 | poco vendido
 imm    | 887p        |     1 | poco vendido
 imm    | 887x        |     1 | poco vendido
 qsa    | xk48        |     1 | poco vendido
 qsa    | xk48a       |     1 | poco vendido
 rei    | 2a44g       |     1 | poco vendido
 rei    | 2a44l       |     1 | poco vendido
 aci    | 41003       |     1 | poco vendido
 aci    | 4100y       |     1 | poco vendido
 bic    | 41089       |     1 | poco vendido
 bic    | 41672       |     1 | poco vendido
 aci    | 41002       |     2 | poco vendido
 fea    | 114         |     2 | poco vendido
 rei    | 2a45c       |     2 | poco vendido
 imm    | 779c        |     2 | poco vendido
 aci    | 4100x       |     2 | poco vendido
 rei    | 2a44r       |     2 | poco vendido
 aci    | 4100z       |     2 | poco vendido
 bic    | 41003       |     2 | poco vendido
 aci    | 41004       |     3 | muy vendido
 qsa    | xk47        |     3 | muy vendido
(25 rows)


Llisteu nom, edat i vendes dels venedors que compleixin el següent si tenen menys de 40 anys només llistarem els venedors que tinguin vendes superiors a 200000, 
si tenen 40 anys o més només llistarem els venedors que tinguin unes vendes superiors a 300000.

```SQL
SELECT nombre, edad, ventas 
FROM repventas 
WHERE 
CASE 
    WHEN edad < 40 THEN ventas > 200000 
    ELSE ventas > 300000 
END;
```
   nombre    | edad |  ventas   
-------------+------+-----------
 Bill Adams  |   37 | 367911.00
 Mary Jones  |   31 | 392725.00
 Sue Smith   |   48 | 474050.00
 Dan Roberts |   45 | 305673.00
 Larry Fitch |   62 | 361865.00
 Paul Cruz   |   29 | 286775.00
(6 rows)


Establiu la quota a 0 als menors de 35 anys i pugeu un 5% a la resta

```
training=# select num_empl, cuota, CASE 
                WHEN edad < 35 THEN 0 
                ELSE cuota * 1.05
            END from repventas;
 num_empl |   cuota   |    case     
----------+-----------+-------------
      105 | 350000.00 | 367500.0000
      109 | 300000.00 |           0
      102 | 350000.00 | 367500.0000
      106 | 275000.00 | 288750.0000
      104 | 200000.00 |           0
      101 | 300000.00 | 315000.0000
      110 |           |            
      108 | 350000.00 | 367500.0000
      103 | 275000.00 |           0
      107 | 300000.00 | 315000.0000
(10 rows)

 num_empl |   cuota   |   round   
----------+-----------+-----------
      105 | 350000.00 | 367500.00
      109 | 300000.00 |         0
      102 | 350000.00 | 367500.00
      106 | 275000.00 | 288750.00
      104 | 200000.00 |         0
      101 | 300000.00 | 315000.00
      110 |           |          
      108 | 350000.00 | 367500.00
      103 | 275000.00 |         0
      107 | 300000.00 | 315000.00
(10 rows)



