EXAMEN PART 2

1)
training=# select empresa, clie, sum(importe) from clientes join pedidos on num_clie=clie where empresa not like 'I%' and empresa not like 'J%' group by empresa, clie having sum(importe)>20000;
 clie 
------
 2102
 2111
 2112
 2109
 2101
 2114
 2120
 2106
 2107
 2124
 2117
 2118
 2108
 2103
 2113
(15 rows)

2)
training=# select num_pedido, importe, clie, empresa, rep, repventas.nombre, oficinas.oficina, rep_clie, repcli.nombre, ofi.oficina from pedidos join clientes on clie=num_clie 
join repventas on rep=num_empl join repventas repcli on clientes.rep_clie = repcli.num_empl join oficinas on repventas.oficina_rep=oficina 
join oficinas as ofi on ofi.oficina=repcli.oficina_rep where rep!=rep_clie;
 num_pedido | importe  | clie |     empresa     | rep |    nombre     | oficina | rep_clie |   nombre   | oficina 
------------+----------+------+-----------------+-----+---------------+---------+----------+------------+---------
     113012 |  3745.00 | 2111 | JCP Inc.        | 105 | Bill Adams    |      13 |      103 | Paul Cruz  |      12
     113024 |  7100.00 | 2114 | Orion Corp      | 108 | Larry Fitch   |      21 |      102 | Sue Smith  |      21
     113069 | 31350.00 | 2109 | Chen Associates | 107 | Nancy Angelli |      22 |      103 | Paul Cruz  |      12
     113055 |   150.00 | 2108 | Holm & Landis   | 101 | Dan Roberts   |      12 |      109 | Mary Jones |      11
     113042 | 22500.00 | 2113 | Ian & Schmidt   | 101 | Dan Roberts   |      12 |      104 | Bob Smith  |      12
(5 rows)


3)
training=# select fab, producto, sum(cant), count(*) from pedidos left join clientes on clie=num_clie group by fab, producto;
 fab | producto | sum | count 
-----+----------+-----+-------
 aci | 41002    |  64 |     2
 aci | 41003    |  35 |     1
 aci | 41004    |  68 |     3
 aci | 4100x    |  30 |     2
 aci | 4100y    |  11 |     1
 aci | 4100z    |  15 |     2
 bic | 41003    |   2 |     2
 fea | 112      |  10 |     1
 fea | 114      |  16 |     2
 imm | 773c     |   3 |     1
 imm | 775c     |  22 |     1
 imm | 779c     |   5 |     2
 qsa | k47      |   4 |     1
 qsa | xk47     |  28 |     3
 rei | 2a44g    |   6 |     1
 rei | 2a44l    |   7 |     1
 rei | 2a44r    |  15 |     2
 rei | 2a45c    |  32 |     2
(18 rows)


4)
training=# select empleado.num_empl, empleado.nombre, empleado.director, jefe.nombre, empleado.oficina_rep, ciudad, dir, cap_oficina.nombre from repventas as empleado left join repventas as jefe on empleado.director=jefe.num_empl left join oficinas on empleado.oficina_rep=oficina left join repventas as cap_oficina on dir=cap_oficina.num_empl;
 num_empl |    nombre     | director |   nombre    | oficina_rep |   ciudad    | dir |   nombre    
----------+---------------+----------+-------------+-------------+-------------+-----+-------------
      105 | Bill Adams    |      104 | Bob Smith   |          13 | Atlanta     | 105 | Bill Adams
      109 | Mary Jones    |      106 | Sam Clark   |          11 | New York    | 106 | Sam Clark
      102 | Sue Smith     |      108 | Larry Fitch |          21 | Los Angeles | 108 | Larry Fitch
      106 | Sam Clark     |          |             |          11 | New York    | 106 | Sam Clark
      104 | Bob Smith     |      106 | Sam Clark   |          12 | Chicago     | 104 | Bob Smith
      101 | Dan Roberts   |      104 | Bob Smith   |          12 | Chicago     | 104 | Bob Smith
      110 | Tom Snyder    |      101 | Dan Roberts |             |             |     | 
      108 | Larry Fitch   |      106 | Sam Clark   |          21 | Los Angeles | 108 | Larry Fitch
      103 | Paul Cruz     |      104 | Bob Smith   |          12 | Chicago     | 104 | Bob Smith
      107 | Nancy Angelli |      108 | Larry Fitch |          22 | Denver      | 108 | Larry Fitch
(10 rows)


5)
training=# select ciudad, nombre, empresa, count(pedidos), sum(importe) from pedidos join repventas on rep=num_empl join clientes on rep_clie=rep join oficinas on oficina_rep=oficina 
group by ciudad, nombre, empresa order by 1,2,3;
   ciudad    |    nombre     |     empresa      | count |   sum    
-------------+---------------+------------------+-------+----------
 Atlanta     | Bill Adams    | Acme Mfg.        |     5 | 39327.00
 Atlanta     | Bill Adams    | Three-Way Lines  |     5 | 39327.00
 Chicago     | Dan Roberts   | AAA Investments  |     3 | 26628.00
 Chicago     | Dan Roberts   | First Corp.      |     3 | 26628.00
 Chicago     | Dan Roberts   | Smithson Corp.   |     3 | 26628.00
 Chicago     | Paul Cruz     | Chen Associates  |     2 |  2700.00
 Chicago     | Paul Cruz     | JCP Inc.         |     2 |  2700.00
 Chicago     | Paul Cruz     | QMA Assoc.       |     2 |  2700.00
 Denver      | Nancy Angelli | Peter Brothers   |     3 | 34432.00
 Los Angeles | Larry Fitch   | Midwest Systems  |     7 | 58633.00
 Los Angeles | Larry Fitch   | Zetacorp         |     7 | 58633.00
 Los Angeles | Sue Smith     | Carter & Sons    |     4 | 22776.00
 Los Angeles | Sue Smith     | Fred Lewis Corp. |     4 | 22776.00
 Los Angeles | Sue Smith     | Orion Corp       |     4 | 22776.00
 Los Angeles | Sue Smith     | Rico Enterprises |     4 | 22776.00
 New York    | Mary Jones    | Holm & Landis    |     2 |  7105.00
 New York    | Mary Jones    | Solomon Inc.     |     2 |  7105.00
 New York    | Sam Clark     | Jones Mfg.       |     2 | 32958.00
 New York    | Sam Clark     | J.P. Sinclair    |     2 | 32958.00
(19 rows)


6)
training=# select oficina_rep, count(*) from repventas where oficina_rep is not null group by oficina_rep;
 oficina_rep | count 
-------------+-------
          13 |     1
          22 |     1
          21 |     2
          11 |     2
          12 |     3
(5 rows)


7)
training=# select region, count(num_empl) from oficinas right join repventas on oficina=oficina_rep where (oficina_rep is null or ciudad ilike '%n%') 
and (ciudad ilike '%a%' or ciudad ilike '%m%') group by region;
 region | count 
--------+-------
 Este   |     1
 Oeste  |     2
(2 rows)


8)
training=# select empresa, fab, producto, precio, representant.nombre as representant, dir.nombre as director from pedidos join productos on fab=id_fab and producto=id_producto 
join clientes on clie=num_clie join repventas as representant on rep=representant.num_empl left join repventas as dir on representant.director=dir.num_empl where (fab='imm' or fab='rei') 
and (precio<1500 or precio>2000);

      empresa      | fab | producto | precio  | representant  |  director   
-------------------+-----+----------+---------+---------------+-------------
 Fred Lewis Corp.  | rei | 2a45c    |   79.00 | Sue Smith     | Larry Fitch
 Ace International | rei | 2a45c    |   79.00 | Tom Snyder    | Dan Roberts
 Chen Associates   | imm | 775c     | 1425.00 | Nancy Angelli | Larry Fitch
 Ian & Schmidt     | rei | 2a44r    | 4500.00 | Dan Roberts   | Bob Smith
 Zetacorp          | rei | 2a44r    | 4500.00 | Larry Fitch   | Sam Clark
 Zetacorp          | imm | 773c     |  975.00 | Larry Fitch   | Sam Clark
 JCP Inc.          | rei | 2a44g    |  350.00 | Paul Cruz     | Bob Smith
(7 rows)


9)
training=# select num_pedido, oficina, ciudad, nombre, importe, empresa from pedidos left join repventas on rep=num_empl left join oficinas on oficina_rep=oficina left join clientes on clie=num_clie order by 3, 4;
 num_pedido | oficina |   ciudad    |    nombre     | importe  |      empresa      
------------+---------+-------------+---------------+----------+-------------------
     112983 |      13 | Atlanta     | Bill Adams    |   702.00 | Acme Mfg.
     112963 |      13 | Atlanta     | Bill Adams    |  3276.00 | Acme Mfg.
     112987 |      13 | Atlanta     | Bill Adams    | 27500.00 | Acme Mfg.
     113027 |      13 | Atlanta     | Bill Adams    |  4104.00 | Acme Mfg.
     113012 |      13 | Atlanta     | Bill Adams    |  3745.00 | JCP Inc.
     113042 |      12 | Chicago     | Dan Roberts   | 22500.00 | Ian & Schmidt
     112968 |      12 | Chicago     | Dan Roberts   |  3978.00 | First Corp.
     113055 |      12 | Chicago     | Dan Roberts   |   150.00 | Holm & Landis
     112975 |      12 | Chicago     | Paul Cruz     |  2100.00 | JCP Inc.
     113057 |      12 | Chicago     | Paul Cruz     |   600.00 | JCP Inc.
     113062 |      22 | Denver      | Nancy Angelli |  2430.00 | Peter Brothers
     113069 |      22 | Denver      | Nancy Angelli | 31350.00 | Chen Associates
     112997 |      22 | Denver      | Nancy Angelli |   652.00 | Peter Brothers
     113045 |      21 | Los Angeles | Larry Fitch   | 45000.00 | Zetacorp
     113024 |      21 | Los Angeles | Larry Fitch   |  7100.00 | Orion Corp
     113051 |      21 | Los Angeles | Larry Fitch   |  1420.00 | Midwest Systems
     113007 |      21 | Los Angeles | Larry Fitch   |  2925.00 | Zetacorp
     112992 |      21 | Los Angeles | Larry Fitch   |   760.00 | Midwest Systems
     113049 |      21 | Los Angeles | Larry Fitch   |   776.00 | Midwest Systems
     113013 |      21 | Los Angeles | Larry Fitch   |   652.00 | Midwest Systems
     112979 |      21 | Los Angeles | Sue Smith     | 15000.00 | Orion Corp
     113048 |      21 | Los Angeles | Sue Smith     |  3750.00 | Rico Enterprises
     112993 |      21 | Los Angeles | Sue Smith     |  1896.00 | Fred Lewis Corp.
     113065 |      21 | Los Angeles | Sue Smith     |  2130.00 | Fred Lewis Corp.
     113058 |      11 | New York    | Mary Jones    |  1480.00 | Holm & Landis
     113003 |      11 | New York    | Mary Jones    |  5625.00 | Holm & Landis
     112989 |      11 | New York    | Sam Clark     |  1458.00 | Jones Mfg.
     112961 |      11 | New York    | Sam Clark     | 31500.00 | J.P. Sinclair
     113034 |         |             | Tom Snyder    |   632.00 | Ace International
     110036 |         |             | Tom Snyder    | 22500.00 | Ace International
(30 rows)
