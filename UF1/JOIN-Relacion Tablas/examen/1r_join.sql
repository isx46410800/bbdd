--1.
select num_pedido,region, nombre, importe, empresa
from pedidos
join repventas on rep=num_empl
left join oficinas on oficina_rep=oficina 
join clientes on clie=num_clie
order by 2,3;

--2.
select ciudad, nombre, empresa, count(*), sum (importe) 
from pedidos
join repventas on rep=num_empl 
left join oficinas on oficina_rep=oficina
join clientes on clie=num_clie
group by nombre, ciudad, empresa
order by 1,2,3;

--3.
select region, count(num_empl)
from oficinas 
right join repventas on oficina_rep=oficina
where oficina_rep is null
or
(ciudad ilike '%n%' and (nombre ilike '%m%' or nombre ilike '%n%'))
group by region ;

--4.
select region, ciudad, count(distinct num_empl), count(distinct num_clie)
from oficinas
join repventas on oficina=oficina_rep
join clientes on rep_clie=num_empl
group by region, ciudad;

--no hace falta poner distinct en el num_clie porque ya sabemos que no se repetirán
--en cambio en el num_empl si porque puede se repitan por cliente.

--5.
select empresa, id_fab, id_producto, precio, empleado.nombre, director.nombre
from pedidos
join clientes on clie=num_clie
join productos on fab=id_fab and producto=id_producto
join repventas empleado on rep=empleado.num_empl
left join repventas director on empleado.director=director.num_empl
where (fab='imm' or fab='rei')
and 
(precio < 80 or precio > 1000);

--tendria que haber un left porque hay un empleado que no tiene jefe (sam clark).

--6.
select nombre, sum(importe)
from repventas 
join pedidos on num_empl=rep
where nombre not like 'I%' and nombre not like 'J%' 
group by nombre 
having sum(importe) > 25000;

--7.
select num_pedido, importe, empresa, comanda.nombre, comanda.oficina_rep, assignat.nombre, 
assignat.oficina_rep
from pedidos
join clientes on clie=num_clie
join repventas comanda on rep=num_empl
join repventas assignat on num_clie=assignat.num_empl
where not comanda.num_empl=assignat.num_empl;

--8.
select id_fab, id_producto, descripcion, precio, existencias, sum(cant), count(distinct rep)
from productos 
left join pedidos on id_fab=fab and id_producto=producto 
group by id_fab, id_producto
order by 4;

--9.
select num_clie, assignat.nombre, director.nombre, cap.nombre, cap_director.nombre
from clientes 
join repventas as assignat on rep_clie=num_empl
left join repventas as director on assignat.director=director.num_empl
left join oficinas as assignat_oficinas on assignat.oficina_rep=assignat_oficinas.oficina
left join repventas as cap on assignat_oficinas.dir=cap.num_empl
left join oficinas as director_oficinas on director.oficina_rep=director_oficinas.oficina
left join repventas as cap_director on director_oficinas.dir=cap_director.num_empl;
