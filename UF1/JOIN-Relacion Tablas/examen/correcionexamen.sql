CORRECION PARTE B EXAMEN

1)
training=# select empresa, clie, sum(importe) from clientes join pedidos on num_clie=clie where empresa not like 'I%' and empresa not like 'J%' group by empresa, clie having sum(importe)>20000;
      empresa      | clie |   sum    
-------------------+------+----------
 Orion Corp        | 2114 | 22100.00
 Zetacorp          | 2112 | 47925.00
 Chen Associates   | 2109 | 31350.00
 Acme Mfg.         | 2103 | 35582.00
 Ace International | 2107 | 23132.00
(5 rows)

2)
select num_pedido, importe, empresa, comanda.nombre, comanda.oficina_rep, assignat.nombre, assignat.oficina_rep from pedidos join clientes on clie=num_clie join repventas as comanda
on rep=num_empl join repventas as assignat on rep_clie=assignat.num_empl where not comanda.num_empl=assignat.num_empl;

training=# select num_pedido, importe, clie, empresa, rep, repventas.nombre, oficinas.oficina, rep_clie, repcli.nombre, ofi.oficina from pedidos join clientes on clie=num_clie 
join repventas on rep=num_empl join repventas repcli on clientes.rep_clie = repcli.num_empl join oficinas on repventas.oficina_rep=oficina 
join oficinas as ofi on ofi.oficina=repcli.oficina_rep where rep!=rep_clie;
 num_pedido | importe  | clie |     empresa     | rep |    nombre     | oficina | rep_clie |   nombre   | oficina 
------------+----------+------+-----------------+-----+---------------+---------+----------+------------+---------
     113012 |  3745.00 | 2111 | JCP Inc.        | 105 | Bill Adams    |      13 |      103 | Paul Cruz  |      12
     113024 |  7100.00 | 2114 | Orion Corp      | 108 | Larry Fitch   |      21 |      102 | Sue Smith  |      21
     113069 | 31350.00 | 2109 | Chen Associates | 107 | Nancy Angelli |      22 |      103 | Paul Cruz  |      12
     113055 |   150.00 | 2108 | Holm & Landis   | 101 | Dan Roberts   |      12 |      109 | Mary Jones |      11
     113042 | 22500.00 | 2113 | Ian & Schmidt   | 101 | Dan Roberts   |      12 |      104 | Bob Smith  |      12
(5 rows)

3)
training=# select productos.*, sum(cant), count(distinct clie) from productos left join pedidos on id_producto=producto and id_fab=fab group by id_fab, id_producto order by precio;
 id_fab | id_producto |    descripcion    | precio  | existencias | sum | count 
--------+-------------+-------------------+---------+-------------+-----+-------
 aci    | 4100x       | Ajustador         |   25.00 |          37 |  30 |     2
 imm    | 887h        | Soporte Riostra   |   54.00 |         223 |     |     0
 aci    | 41001       | Articulo Tipo 1   |   55.00 |         277 |     |     0
 aci    | 41002       | Articulo Tipo 2   |   76.00 |         167 |  64 |     2
 rei    | 2a45c       | V Stago Trinquete |   79.00 |         210 |  32 |     2
 aci    | 41003       | Articulo Tipo 3   |  107.00 |         207 |  35 |     1
 aci    | 41004       | Articulo Tipo 4   |  117.00 |         139 |  68 |     2
 qsa    | xk48a       | Reductor          |  117.00 |          37 |     |     0
 qsa    | xk48        | Reductor          |  134.00 |         203 |     |     0
 fea    | 112         | Cubierta          |  148.00 |         115 |  10 |     1
 bic    | 41672       | Plate             |  180.00 |           0 |     |     0
 bic    | 41089       | Retn              |  225.00 |          78 |     |     0
 fea    | 114         | Bancada Motor     |  243.00 |          15 |  16 |     2
 imm    | 887p        | Perno Riostra     |  250.00 |          24 |     |     0
 rei    | 2a44g       | Pasador Bisagra   |  350.00 |          14 |   6 |     1
 qsa    | xk47        | Reductor          |  355.00 |          38 |  28 |     3
 imm    | 887x        | Retenedor Riostra |  475.00 |          32 |     |     0
 bic    | 41003       | Manivela          |  652.00 |           3 |   2 |     2
 imm    | 773c        | Riostra 1/2-Tm    |  975.00 |          28 |   3 |     1
 imm    | 775c        | Riostra 1-Tm      | 1425.00 |           5 |  22 |     1
 imm    | 779c        | Riostra 2-Tm      | 1875.00 |           9 |   5 |     2
 aci    | 4100z       | Montador          | 2500.00 |          28 |  15 |     2
 aci    | 4100y       | Extractor         | 2750.00 |          25 |  11 |     1
 rei    | 2a44l       | Bisagra Izqda.    | 4500.00 |          12 |   7 |     1
 rei    | 2a44r       | Bisagra Dcha.     | 4500.00 |          12 |  15 |     2
(25 rows)

4)
select num_clie, assignat.nombre, director.nombre, cap.nombre, cap_director.nombre
from clientes 
join repventas as assignat on rep_clie=num_empl
left join repventas as director on assignat.director=director.num_empl
left join oficinas as assignat_oficinas on assignat.oficina_rep=assignat_oficinas.oficina
left join repventas as cap on assignat_oficinas.dir=cap.num_empl
left join oficinas as director_oficinas on director.oficina_rep=director_oficinas.oficina
left join repventas as cap_director on director_oficinas.dir=cap_director.num_empl;


5)
training=# select region, nombre, empresa, count(rep), sum(cant) from pedidos left join repventas on rep=num_empl join clientes on rep_clie=rep join oficinas on oficina_rep=oficina 
group by region, nombre, empresa order by 1,2,3;
 region |    nombre     |     empresa      | count | sum 
--------+---------------+------------------+-------+-----
 Este   | Bill Adams    | Acme Mfg.        |     5 | 134
 Este   | Bill Adams    | Three-Way Lines  |     5 | 134
 Este   | Dan Roberts   | AAA Investments  |     3 |  45
 Este   | Dan Roberts   | First Corp.      |     3 |  45
 Este   | Dan Roberts   | Smithson Corp.   |     3 |  45
 Este   | Mary Jones    | Holm & Landis    |     2 |  13
 Este   | Mary Jones    | Solomon Inc.     |     2 |  13
 Este   | Paul Cruz     | Chen Associates  |     2 |  30
 Este   | Paul Cruz     | JCP Inc.         |     2 |  30
 Este   | Paul Cruz     | QMA Assoc.       |     2 |  30
 Este   | Sam Clark     | Jones Mfg.       |     2 |  13
 Este   | Sam Clark     | J.P. Sinclair    |     2 |  13
 Oeste  | Larry Fitch   | Midwest Systems  |     7 |  50
 Oeste  | Larry Fitch   | Zetacorp         |     7 |  50
 Oeste  | Nancy Angelli | Peter Brothers   |     3 |  33
 Oeste  | Sue Smith     | Carter & Sons    |     4 |  38
 Oeste  | Sue Smith     | Fred Lewis Corp. |     4 |  38
 Oeste  | Sue Smith     | Orion Corp       |     4 |  38
 Oeste  | Sue Smith     | Rico Enterprises |     4 |  38
(19 rows)



-- 6

Mostrar la quantitat de representants de ventes que hi ha a les diferents ciutats-oficines
 i la quantitat de comandes que han fet aquests representants d''aquestes ciutats-oficines.
Mostrar la regió, la ciutat, el número de representants i el número de comandes.

SELECT ofi.region, ofi.ciudad, COUNT(DISTINCT(rep.num_empl)), COUNT(ped.rep) 
FROM repventas rep 
LEFT JOIN oficinas ofi ON (rep.oficina_rep=ofi.oficina)
LEFT JOIN pedidos ped ON (rep.num_empl=ped.rep)
GROUP BY ofi.region, ofi.ciudad;

 region |   ciudad    | count | count 
--------+-------------+-------+-------
 Este   | Atlanta     |     1 |     5
 Este   | Chicago     |     3 |     5
 Este   | New York    |     2 |     4
 Oeste  | Denver      |     1 |     3
 Oeste  | Los Angeles |     2 |    11
        |             |     1 |     2
(6 rows)


-- 7
Mostrar la quantitat de representants de ventes que hi ha a la regió Este, la quantitat de 
representants de ventes que hi ha a la regió Oeste i la quantitat de representants que no estan assignats a cap regió. 
Només es vol comptar als representants que no tenen ciutat assignada o que tenen un 'N' minúscula o majúscula al nom de la seva ciutat  i tenen una 'm' o una 'a' minúscula o majúscula al seu nom.


SELECT ofi.region, COUNT(rep.num_empl)
FROM repventas rep LEFT JOIN oficinas ofi ON (rep.oficina_rep=ofi.oficina)
WHERE (rep.oficina_rep IS NULL OR ofi.ciudad ILIKE '%n%') AND (rep.nombre ILIKE '%m%' OR rep.nombre ILIKE '%a%')
GROUP BY ofi.region;

 region | count 
--------+-------
        |     1
 Este   |     3
 Oeste  |     3
(3 rows)


-- 8

Mostrar els noms de les empreses que han comprat productes de les fàbriques imm i rei amb un 
preu de catàleg (no de compra) inferior a 1500 o superior a 2000. 
Mostrar l'empresa' el fabricant, el codi de producte, el preu, el nom del
 representant de ventes que ha fet la comanda i el nom del seu director.

SELECT cli.empresa, pro.id_fab, pro.id_producto, pro.precio, rep1.nombre AS "representant", rep2.nombre AS "director"
FROM pedidos ped JOIN clientes cli ON (ped.clie=cli.num_clie)
JOIN productos pro ON (ped.fab=pro.id_fab AND ped.producto=pro.id_producto)
JOIN repventas rep1 ON (ped.rep=rep1.num_empl)
LEFT JOIN repventas rep2 ON (rep1.director=rep2.num_empl)
WHERE (pro.id_fab='imm' OR pro.id_fab='rei') AND (pro.precio<1500 OR pro.precio>2000);

      empresa      | id_fab | id_producto | precio  | representant  |  director   
-------------------+--------+-------------+---------+---------------+-------------
 J.P. Sinclair     | rei    | 2a44l       | 4500.00 | Sam Clark     | 
 Zetacorp          | rei    | 2a44r       | 4500.00 | Larry Fitch   | Sam Clark
 Zetacorp          | imm    | 773c        |  975.00 | Larry Fitch   | Sam Clark
 Chen Associates   | imm    | 775c        | 1425.00 | Nancy Angelli | Larry Fitch
 Ace International | rei    | 2a45c       |   79.00 | Tom Snyder    | Dan Roberts
 JCP Inc.          | rei    | 2a44g       |  350.00 | Paul Cruz     | Bob Smith
 Fred Lewis Corp.  | rei    | 2a45c       |   79.00 | Sue Smith     | Larry Fitch
 Ian & Schmidt     | rei    | 2a44r       | 4500.00 | Dan Roberts   | Bob Smith
(8 rows)

-- 9
Per cada comanda que tenim, visualitzar la ciutat de l'oficina del representant de ventes  que l'ha fet,  
el nom del representant que l'ha fet, l'import i el nom de l'empresa que ha comprat el producte.
Mostrar-ho  ordenat pels 2 primers camps demanats.'

SELECT ofi.ciudad, rep.nombre, ped.importe, cli.empresa
FROM pedidos ped JOIN clientes cli ON (ped.clie=cli.num_clie)
    JOIN repventas rep ON (ped.rep=rep.num_empl)
    LEFT JOIN oficinas ofi ON (rep.oficina_rep=ofi.oficina)
ORDER BY ofi.ciudad, rep.nombre;

   ciudad    |    nombre     | importe  |      empresa      
-------------+---------------+----------+-------------------
 Atlanta     | Bill Adams    |   702.00 | Acme Mfg.
 Atlanta     | Bill Adams    |  3276.00 | Acme Mfg.
 Atlanta     | Bill Adams    | 27500.00 | Acme Mfg.
 Atlanta     | Bill Adams    |  4104.00 | Acme Mfg.
 Atlanta     | Bill Adams    |  3745.00 | JCP Inc.
 Chicago     | Dan Roberts   | 22500.00 | Ian & Schmidt
 Chicago     | Dan Roberts   |  3978.00 | First Corp.
 Chicago     | Dan Roberts   |   150.00 | Holm & Landis
 Chicago     | Paul Cruz     |  2100.00 | JCP Inc.
 Chicago     | Paul Cruz     |   600.00 | JCP Inc.
 Denver      | Nancy Angelli |  2430.00 | Peter Brothers
 Denver      | Nancy Angelli | 31350.00 | Chen Associates
 Denver      | Nancy Angelli |   652.00 | Peter Brothers
 Los Angeles | Larry Fitch   | 45000.00 | Zetacorp
 Los Angeles | Larry Fitch   |  7100.00 | Orion Corp
 Los Angeles | Larry Fitch   |  1420.00 | Midwest Systems
 Los Angeles | Larry Fitch   |  2925.00 | Zetacorp
 Los Angeles | Larry Fitch   |   760.00 | Midwest Systems
 Los Angeles | Larry Fitch   |   776.00 | Midwest Systems
 Los Angeles | Larry Fitch   |   652.00 | Midwest Systems
 Los Angeles | Sue Smith     | 15000.00 | Orion Corp
 Los Angeles | Sue Smith     |  3750.00 | Rico Enterprises
 Los Angeles | Sue Smith     |  1896.00 | Fred Lewis Corp.
 Los Angeles | Sue Smith     |  2130.00 | Fred Lewis Corp.
 New York    | Mary Jones    |  1480.00 | Holm & Landis
 New York    | Mary Jones    |  5625.00 | Holm & Landis
 New York    | Sam Clark     |  1458.00 | Jones Mfg.
 New York    | Sam Clark     | 31500.00 | J.P. Sinclair
             | Tom Snyder    |   632.00 | Ace International
             | Tom Snyder    | 22500.00 | Ace International
(30 rows)

--------------------------------------------------------------


6)
select region, ciudad, count(distinct num_empl), count(rep) from repventas left join oficinas on oficina=oficina_rep join pedidos on num_empl=rep group by region, ciudad;
 region |   ciudad    | count | count 
--------+-------------+-------+-------
 Este   | Atlanta     |     1 |     5
 Este   | Chicago     |     2 |     5
 Este   | New York    |     2 |     4
 Oeste  | Denver      |     1 |     3
 Oeste  | Los Angeles |     2 |    11
        |             |     1 |     2
(6 rows)


8)
training=# select empresa, fab, producto, precio, representant.nombre as representant, dir.nombre as director from pedidos join productos on fab=id_fab and producto=id_producto 
join clientes on clie=num_clie join repventas as representant on rep=representant.num_empl left join repventas as dir on representant.director=dir.num_empl where (fab='imm' or fab='rei') 
and (precio<1500 or precio>2000);
   empresa      | fab | producto | precio  | representant  |  director   
-------------------+-----+----------+---------+---------------+-------------
 Fred Lewis Corp.  | rei | 2a45c    |   79.00 | Sue Smith     | Larry Fitch
 Ace International | rei | 2a45c    |   79.00 | Tom Snyder    | Dan Roberts
 J.P. Sinclair     | rei | 2a44l    | 4500.00 | Sam Clark     | 
 Chen Associates   | imm | 775c     | 1425.00 | Nancy Angelli | Larry Fitch
 Ian & Schmidt     | rei | 2a44r    | 4500.00 | Dan Roberts   | Bob Smith
 Zetacorp          | rei | 2a44r    | 4500.00 | Larry Fitch   | Sam Clark
 Zetacorp          | imm | 773c     |  975.00 | Larry Fitch   | Sam Clark
 JCP Inc.          | rei | 2a44g    |  350.00 | Paul Cruz     | Bob Smith
(8 rows)

#se ha de poner un left join en repventas director porque hay uno que no tiene y tienen que salir todos.

9) PER CADA COMANDA QUE TENIM, VISUALITZAR LA CIUTAT DE OFICINA DEL REPRESENTANT DE VENTES QUE L'HA FET, EL NOM DEL REPRESENTAT QUE L'HA FET, L'IMPORT I EL NOM DE L'EMPRESA
QUE HA COMPRAT EL PRODUCTE. MOSTRAR ORDENAT PELS DOS PRIMERS CAMPS DEMANATS.

training=# select num_pedido, oficina, ciudad, nombre, importe, empresa from pedidos left join repventas on rep=num_empl left join oficinas on oficina_rep=oficina left join clientes on clie=num_clie order by 3, 4;
 num_pedido | oficina |   ciudad    |    nombre     | importe  |      empresa      
------------+---------+-------------+---------------+----------+-------------------
     112983 |      13 | Atlanta     | Bill Adams    |   702.00 | Acme Mfg.
     112963 |      13 | Atlanta     | Bill Adams    |  3276.00 | Acme Mfg.
     112987 |      13 | Atlanta     | Bill Adams    | 27500.00 | Acme Mfg.
     113027 |      13 | Atlanta     | Bill Adams    |  4104.00 | Acme Mfg.
     113012 |      13 | Atlanta     | Bill Adams    |  3745.00 | JCP Inc.
     113042 |      12 | Chicago     | Dan Roberts   | 22500.00 | Ian & Schmidt
     112968 |      12 | Chicago     | Dan Roberts   |  3978.00 | First Corp.
     113055 |      12 | Chicago     | Dan Roberts   |   150.00 | Holm & Landis
     112975 |      12 | Chicago     | Paul Cruz     |  2100.00 | JCP Inc.
     113057 |      12 | Chicago     | Paul Cruz     |   600.00 | JCP Inc.
     113062 |      22 | Denver      | Nancy Angelli |  2430.00 | Peter Brothers
     113069 |      22 | Denver      | Nancy Angelli | 31350.00 | Chen Associates
     112997 |      22 | Denver      | Nancy Angelli |   652.00 | Peter Brothers
     113045 |      21 | Los Angeles | Larry Fitch   | 45000.00 | Zetacorp
     113024 |      21 | Los Angeles | Larry Fitch   |  7100.00 | Orion Corp
     113051 |      21 | Los Angeles | Larry Fitch   |  1420.00 | Midwest Systems
     113007 |      21 | Los Angeles | Larry Fitch   |  2925.00 | Zetacorp
     112992 |      21 | Los Angeles | Larry Fitch   |   760.00 | Midwest Systems
     113049 |      21 | Los Angeles | Larry Fitch   |   776.00 | Midwest Systems
     113013 |      21 | Los Angeles | Larry Fitch   |   652.00 | Midwest Systems
     112979 |      21 | Los Angeles | Sue Smith     | 15000.00 | Orion Corp
     113048 |      21 | Los Angeles | Sue Smith     |  3750.00 | Rico Enterprises
     112993 |      21 | Los Angeles | Sue Smith     |  1896.00 | Fred Lewis Corp.
     113065 |      21 | Los Angeles | Sue Smith     |  2130.00 | Fred Lewis Corp.
     113058 |      11 | New York    | Mary Jones    |  1480.00 | Holm & Landis
     113003 |      11 | New York    | Mary Jones    |  5625.00 | Holm & Landis
     112989 |      11 | New York    | Sam Clark     |  1458.00 | Jones Mfg.
     112961 |      11 | New York    | Sam Clark     | 31500.00 | J.P. Sinclair
     113034 |         |             | Tom Snyder    |   632.00 | Ace International
     110036 |         |             | Tom Snyder    | 22500.00 | Ace International
(30 rows)

