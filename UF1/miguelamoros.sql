EXAMEN BASE DE DADES PART 4

1)

training=# select id_fab, id_producto, ciudad, count(num_pedido) from productos left join pedidos on id_fab=fab and id_producto=producto left join repventas on rep=num_empl left join oficinas on oficina_rep=oficina group by id_fab, id_producto, ciudad having count(*)>=0 order by 1,2,3;
 id_fab | id_producto |   ciudad    | count 
--------+-------------+-------------+-------
 aci    | 41001       |             |     0
 aci    | 41002       | Atlanta     |     1
 aci    | 41002       | Los Angeles |     1
 aci    | 41003       | Atlanta     |     1
 aci    | 41004       | Atlanta     |     2
 aci    | 41004       | Chicago     |     1
 aci    | 4100x       | Chicago     |     2
 aci    | 4100y       | Atlanta     |     1
 aci    | 4100z       | Los Angeles |     1
 aci    | 4100z       |             |     1
 bic    | 41003       | Denver      |     1
 bic    | 41003       | Los Angeles |     1
 bic    | 41089       |             |     0
 bic    | 41672       |             |     0
 fea    | 112         | New York    |     1
 fea    | 114         | Denver      |     1
 fea    | 114         | New York    |     1
 imm    | 773c        | Los Angeles |     1
 imm    | 775c        | Denver      |     1
 imm    | 779c        | Los Angeles |     1
 imm    | 779c        | New York    |     1
 imm    | 887h        |             |     0
 imm    | 887p        |             |     0
 imm    | 887x        |             |     0
 qsa    | xk47        | Los Angeles |     3
 qsa    | xk48        |             |     0
 qsa    | xk48a       |             |     0
 rei    | 2a44g       | Chicago     |     1
 rei    | 2a44l       | New York    |     1
 rei    | 2a44r       | Chicago     |     1
 rei    | 2a44r       | Los Angeles |     1
 rei    | 2a45c       | Los Angeles |     1
 rei    | 2a45c       |             |     1
(33 rows)


2)
training=# select empleado.num_empl as "Empleado", empleado.nombre as "Director", dir.num_empl, dir.nombre, count(distinct fab) from repventas as empleado left join repventas as dir on dir.num_empl=empleado.director left join pedidos on dir.num_empl=rep group by empleado.num_empl, empleado.nombre, dir.num_empl, dir.nombre;
 Empleado |   Director    | num_empl |   nombre    | count 
----------+---------------+----------+-------------+-------
      101 | Dan Roberts   |      104 | Bob Smith   |     0
      102 | Sue Smith     |      108 | Larry Fitch |     5
      103 | Paul Cruz     |      104 | Bob Smith   |     0
      104 | Bob Smith     |      106 | Sam Clark   |     2
      105 | Bill Adams    |      104 | Bob Smith   |     0
      106 | Sam Clark     |          |             |     0
      107 | Nancy Angelli |      108 | Larry Fitch |     5
      108 | Larry Fitch   |      106 | Sam Clark   |     2
      109 | Mary Jones    |      106 | Sam Clark   |     2
      110 | Tom Snyder    |      101 | Dan Roberts |     2
(10 rows)




3)
training=# select oficina, ciudad, sum(importe) from oficinas join repventas on oficina=oficina_rep join pedidos on num_empl=rep join productos on fab=id_fab and producto=id_producto where (id_fab like '%a' or id_fab like '%e' or id_fab like '%i' or id_fab like '%o' or id_fab like '%u') and ( descripcion like '%0%' or descripcion like '%1%' or descripcion like '%2%') group by oficina, ciudad order by 3 desc limit 1;
 oficina | ciudad  |   sum   
---------+---------+---------
      13 | Atlanta | 4104.00
(1 row)

