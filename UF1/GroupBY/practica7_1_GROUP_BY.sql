    1. Quantes comandes s'han fet, quina és la suma total dels imports d'aquestes comandes i quina és la mitjana de l'import de la comada : 
    les comandes amb una quantitat d'entre 20 i 30 productes i de les fàbriques que continguin una 'i' o una 'a'.

training=# select count(*), sum(importe), avg(importe) from pedidos where cant>=20 and cant<=30 and (fab ilike '%i%' or fab ilike '%a%');
 count |   sum    |          avg          
-------+----------+-----------------------
     5 | 44222.00 | 8844.4000000000000000
(1 row)



    2. De quina fàbrica s'han venut menys unitats de producte (alerta : no menys comandes, menys unitats)
    
'training=# select fab, sum(cant) from pedidos group by fab;
 fab | sum 
-----+-----
 imm |  30
 aci | 223
 fea |  26
 qsa |  32
 rei |  60
 bic |   2
(6 rows)
#Para saber el de menos, ordenamos ascendente y ponemos limit 1;

training=# select fab, sum(cant) from pedidos group by fab order by 2 asc limit 1;
 fab | sum 
-----+-----
 bic |   2
(1 row)



    3. Quins són els 3 venedors que han fet més comandes?

training=# select rep, count(*) from pedidos group by rep order by 2 desc limit 3;
 rep | count 
-----+-------
 108 |     7
 105 |     5
 102 |     4
(3 rows)



    4. Mostra quants clients amb un nom que contingui 2 espais en blanc té assignat cada venedor.

training=# select rep_clie, count(*) from clientes where empresa like '% % %' group by rep_clie;
 rep_clie | count 
----------+-------
      109 |     1
      104 |     1
      102 |     2
(3 rows)



    5. Mostra quantes comandes ha fet cada venedor a cada client que tingui un codi múltiple de 5 o múltiple de 3 i, a més a més,
     que el codi de client no sigui ni el 110 ni el 102.

training=# select rep, count(*) from pedidos where (clie%5=0 or clie%3=0) and clie<>110 and clie<>102 group by rep;
 rep | count 
-----+-------
 102 |     3
 105 |     4
 107 |     3
 108 |     6
(4 rows)


       
    6. Quin és el producte amb el qual s'ha facturat més diners?

training=# select fab, producto, sum(importe) from pedidos group by fab, producto order by 3 desc limit 1;
 fab | producto |   sum    
-----+----------+----------
 rei | 2a44r    | 67500.00
(1 row)


       
    7. Quins codis de venedor han fet més d'una comanda al mateix client?

training=# select clie,rep, count(*) from pedidos group by clie,rep having count(*)>=2;
 clie | rep | count 
------+-----+-------
 2111 | 103 |     2
 2106 | 102 |     2
 2108 | 109 |     2
 2118 | 108 |     4
 2112 | 108 |     2
 2124 | 107 |     2
 2103 | 105 |     4
 2107 | 110 |     2
(8 rows)


       
    8. Quins són els  id_producto de la taula productos que es fabriquen a més d'una fàbrica?
    
training=# select id_producto, count(*) from productos group by id_producto having count(*)>1;
 id_producto | count 
-------------+-------
 41003       |     2
(1 row)


