-- 1.- Quantes oficines tenim a cada regió?
training=# select region, count(*) from oficinas group by region;
 region | count 
--------+-------
 Este   |     3
 Oeste  |     2
(2 rows)


-- 2.- Quants representants de ventes hi ha a cada oficina?
training=# select oficina_rep, count(*) from repventas where oficina_rep is not null group by oficina_rep;
 oficina_rep | count 
-------------+-------
          13 |     1
          22 |     1
          21 |     2
          11 |     2
          12 |     3
(5 rows)



-- 3.- Quants representants de ventes té assignats cada cap de respresentants?
training=# select director, count(*) from repventas where director is not null group by director;
 director | count 
----------+-------
      106 |     3
      101 |     1
      108 |     2
      104 |     3
(4 rows)


-- 4.- Quina és, per cada oficina, la suma de les quotes dels seus representants? I la mitjana de les quotes per oficina?
training=# select oficina_rep, count(oficina_rep), sum(cuota), avg(cuota) from repventas where oficina_rep is not null group by oficina_rep;
 oficina_rep | count |    sum    |         avg         
-------------+-------+-----------+---------------------
          13 |     1 | 350000.00 | 350000.000000000000
          22 |     1 | 300000.00 | 300000.000000000000
          21 |     2 | 700000.00 | 350000.000000000000
          11 |     2 | 575000.00 | 287500.000000000000
          12 |     3 | 775000.00 | 258333.333333333333
(5 rows)


-- 5.- Quin és la cuota que té la quota més alta de cada oficina?
training=# select oficina_rep, max(cuota) from repventas where oficina_rep is not null group by oficina_rep;
 oficina_rep |    max    
-------------+-----------
          13 | 350000.00
          22 | 300000.00
          21 | 350000.00
          11 | 300000.00
          12 | 300000.00
(5 rows)


-- 6.- Quants clients representa cada venedor-repventas?

training=# select rep_clie, count(*) from clientes group by rep_clie;
 rep_clie | count 
----------+-------
      106 |     2
      103 |     3
      110 |     1
      101 |     3
      105 |     2
      107 |     1
      108 |     2
      109 |     2
      104 |     1
      102 |     4
(10 rows)



-- 7.- Quin és el limit de credit mes alt dels clients de cada venedor-repventas?
training=# select rep_clie, max(limite_credito) from clientes group by rep_clie;
 rep_clie |   max    
----------+----------
      106 | 65000.00
      103 | 50000.00
      110 | 35000.00
      101 | 65000.00
      105 | 50000.00
      107 | 40000.00
      108 | 60000.00
      109 | 55000.00
      104 | 20000.00
      102 | 65000.00
(10 rows)



-- 8.- Per cada codi de fàbrica diferents, quants productes hi ha?
training=# select id_fab, count(*) from productos group by id_fab;
 id_fab | count 
--------+-------
 imm    |     6
 aci    |     7
 fea    |     2
 qsa    |     3
 rei    |     4
 bic    |     3
(6 rows)


-- 9.- Per cada codi de producte diferent, a quantes fàbriques es fabrica?
training=# select id_producto, count(*) from productos group by id_producto;
 id_producto | count 
-------------+-------
 887p        |     1
 773c        |     1
 4100z       |     1
 41002       |     1
 41004       |     1
 xk48a       |     1
 114         |     1
 2a44l       |     1
 4100y       |     1
 xk47        |     1
 xk48        |     1
 887x        |     1
 887h        |     1
 2a44g       |     1
 41672       |     1
 41089       |     1
 2a44r       |     1
 779c        |     1
 112         |     1
 775c        |     1
 2a45c       |     1
 41003       |     2
 4100x       |     1
 41001       |     1
(24 rows)


-- 10.- Per cada nom de producte diferent, quants codis (if_fab + id_prod) tenim?
training=# select descripcion, count(*) from productos group by descripcion;

    descripcion    | count 
-------------------+-------
 Reductor          |     3
 Cubierta          |     1
 Extractor         |     1
 Manivela          |     1
 Articulo Tipo 3   |     1
 V Stago Trinquete |     1
 Articulo Tipo 2   |     1
 Pasador Bisagra   |     1
 Ajustador         |     1
 Articulo Tipo 1   |     1
 Montador          |     1
 Soporte Riostra   |     1
 Riostra 1-Tm      |     1
 Articulo Tipo 4   |     1
 Bancada Motor     |     1
 Riostra 1/2-Tm    |     1
 Bisagra Izqda.    |     1
 Retn              |     1
 Perno Riostra     |     1
 Plate             |     1
 Retenedor Riostra |     1
 Riostra 2-Tm      |     1
 Bisagra Dcha.     |     1
(23 rows)




-- 11.- Per cada producte (if_fab + id_prod), quantes comandes tenim? group by fab, producto;
# se coge id fab + id prod porque hay productos con el mismo id prod pero son de diferente id fab, por tanto, se ha de agrupar por este conjunto 
para ver diferentes productos.

training=# select fab, producto, count(*) from pedidos group by fab, producto;
 fab | producto | count 
-----+----------+-------
 fea | 114      |     2
 qsa | xk47     |     3
 rei | 2a44g    |     1
 bic | 41003    |     2
 rei | 2a45c    |     2
 imm | 773c     |     1
 qsa | k47      |     1
 rei | 2a44r    |     2
 aci | 41003    |     1
 aci | 4100x    |     2
 aci | 4100z    |     2
 imm | 775c     |     1
 aci | 41002    |     2
 imm | 779c     |     2
 rei | 2a44l    |     1
 aci | 41004    |     3
 aci | 4100y    |     1
 fea | 112      |     1
(18 rows)



-- 12.- Per cada client, quantes comandes tenim?
training=# select clie, count(*) from pedidos group by clie;
 clie | count 
------+-------
 2102 |     1
 2111 |     3
 2112 |     2
 2109 |     1
 2101 |     1
 2114 |     2
 2120 |     1
 2106 |     2
 2107 |     2
 2124 |     2
 2117 |     1
 2118 |     4
 2108 |     3
 2103 |     4
 2113 |     1
(15 rows)



-- 13.- Quantes comandes ha realitzat cada representant de vendes?
training=# select rep, count(*) from pedidos group by rep;
 rep | count 
-----+-------
 106 |     2
 103 |     2
 110 |     2
 101 |     3
 105 |     5
 107 |     3
 108 |     7
 109 |     2
 102 |     4
(9 rows)


