MIGUEL AMORÓS MORET

EXAMEN B

1)training=# select clie, sum(importe) from pedidos where 0.2*importe > 2000 group by clie order by 2 desc limit 3;
 clie |   sum    
------+----------
 2112 | 45000.00
 2117 | 31500.00
 2109 | 31350.00
(3 rows)




2)training=# select distinct rep_clie from clientes where rep_clie<=110 and (empresa like 'S%' or empresa like 'A%');
 rep_clie 
----------
      101
      105
      109
      110
(4 rows)



3)training=# select clie, count(*), sum(importe) from pedidos group by clie having count(*)>1 and sum(importe)>30000;
 clie | count |   sum    
------+-------+----------
 2112 |     2 | 47925.00
 2103 |     4 | 35582.00
(2 rows)



4)training=# select fab, producto, count(*), sum(cant), sum(importe) from pedidos group by fab, producto having sum(cant)>60;
 fab | producto | count | sum |   sum   
-----+----------+-------+-----+---------
 aci | 41002    |     2 |  64 | 4864.00
 aci | 41004    |     3 |  68 | 7956.00
(2 rows)



5)training=# select fab, producto, count(*) from pedidos where (fab='rei' or fab='aci') group by fab, producto having count(*)>1;
 fab | producto | count 
-----+----------+-------
 aci | 41002    |     2
 aci | 41004    |     3
 aci | 4100x    |     2
 aci | 4100z    |     2
 rei | 2a44r    |     2
 rei | 2a45c    |     2
(6 rows)



6)training=# select oficina_rep, count(*) from repventas where oficina_rep is not null and edad<50 group by oficina_rep having count(*)>=2;
 oficina_rep | count 
-------------+-------
          12 |     3
(1 row)



7)training=# select id_fab, count(*) from productos where precio<=1000 and precio>=100 and (descripcion like '%or' or descripcion like '%ra') and (id_producto like '%7%' or id_producto like '%8%') group by id_fab;
 id_fab | count 
--------+-------
 imm    |     2
 qsa    |     3
(2 rows)




8)training=# select fab, rep, sum(cant) from pedidos group by fab, rep order by 3 desc; 
 fab | rep | sum 
-----+-----+-----
 aci | 105 | 134
 aci | 101 |  40
 qsa | 108 |  26
 aci | 103 |  24
 rei | 102 |  24
 imm | 107 |  22
 fea | 109 |  10
 aci | 108 |  10
 fea | 107 |  10
 rei | 108 |  10
 aci | 110 |   9
 rei | 110 |   8
 rei | 106 |   7
 fea | 106 |   6
 aci | 102 |   6
 rei | 103 |   6
 qsa | 102 |   6
 rei | 101 |   5
 imm | 109 |   3
 imm | 108 |   3
 imm | 102 |   2
 bic | 108 |   1
 bic | 107 |   1
(23 rows)




9)training=# select director, count(*) from repventas where director is not null group by director order by 1 asc limit 1;
 director | count 
----------+-------
      101 |     1
(1 row)



10)training=# select fab, sum(cant) from pedidos group by fab having sum(cant)>50;
 fab | sum 
-----+-----
 aci | 223
 rei |  60
(2 rows)


