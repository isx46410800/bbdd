    1. Visualitza l'import total i el número de comandes per producte que s'han fet dels productes que tenen un codi que comneça
     per 4 i acaba en 2, en 3 o en 4 i dels quals s'han fet 3 o més comandes.

	 
	training=# select fab, producto, sum(importe), count(*) from pedidos where (producto like '4%2' or producto like '4%3' or producto like '4%4') group by fab, producto having count(*)>=3;
	fab | producto |   sum   | count 
	-----+----------+---------+-------
	aci | 41004    | 7956.00 |     3
	(1 row)




    2. Volem saber quants clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei
 training=# select count(distinct clie) from pedidos where cant>7 and (fab='aci' or fab='rei');
 count 
-------
     7
(1 row)



    3. Estem interessats en els clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei. 
    Volem saber quins codis de clients ens han fet dues o més comandes d'aquestes característiques.
    training=# select clie, sum(cant), count(*) from pedidos where fab='aci' or fab='rei' group by clie having sum(cant)>=7 and count(*)>1;
	 clie | sum | count 
	------+-----+-------
	 2103 |  99 |     4
	 2107 |  17 |     2
	 2111 |  65 |     3
	(3 rows)


      
    


    4. Volem saber quin fabricant és el que té més unitats dels seus productes als nostres magatzems. Visualitza el codi de la fàbrica
     i el total d'unitats dels seus productes que tenim.
     
	 training=# select id_fab, sum(existencias) from productos group by id_fab order by 2 desc limit 1;
	 id_fab | sum 
	--------+-----
	 aci    | 880
	(1 row)


     


    5. Entre els nostres productes n'hi ha que tenen el mateix nom(descripcion). Visualitza la descripcion d'aquests productes
     I el preu del més car.
     
	 training=# select id_producto, descripcion, precio from productos order by precio desc;
	 id_producto |    descripcion    | precio  
	-------------+-------------------+---------
	 2a44r       | Bisagra Dcha.     | 4500.00
	 2a44l       | Bisagra Izqda.    | 4500.00
	 4100y       | Extractor         | 2750.00
	 4100z       | Montador          | 2500.00
	 779c        | Riostra 2-Tm      | 1875.00
	 775c        | Riostra 1-Tm      | 1425.00
	 773c        | Riostra 1/2-Tm    |  975.00
	 41003       | Manivela          |  652.00
	 887x        | Retenedor Riostra |  475.00
	 xk47        | Reductor          |  355.00
	 2a44g       | Pasador Bisagra   |  350.00
	 887p        | Perno Riostra     |  250.00
	 114         | Bancada Motor     |  243.00
	 41089       | Retn              |  225.00
	 41672       | Plate             |  180.00
	 112         | Cubierta          |  148.00
	 xk48        | Reductor          |  134.00
	 xk48a       | Reductor          |  117.00
	 41004       | Articulo Tipo 4   |  117.00
	 41003       | Articulo Tipo 3   |  107.00
	 2a45c       | V Stago Trinquete |   79.00
	 41002       | Articulo Tipo 2   |   76.00
	 41001       | Articulo Tipo 1   |   55.00
	 887h        | Soporte Riostra   |   54.00
	 4100x       | Ajustador         |   25.00
	(25 rows)

	training=# select id_producto, descripcion, precio from productos order by precio desc limit 1;
	 id_producto |  descripcion   | precio  
	-------------+----------------+---------
	 2a44l       | Bisagra Izqda. | 4500.00
	(1 row)


   


    6. Volem saber a quines oficines treballen venedors amb un cognom que comenci per A o per S I un nom que comenci
     amb T o amb S o amb B o amb N. També volem saber quants trebalaldors d'aquestes característiqes hi treballen 
     I quina és la suma de les seves quotes per oficina. No volem que es visualitzi el Tom Snyder perquè ell no pertany a cap oficina en particular.

	training=# select oficina_rep, count(*), sum(cuota) from repventas where (nombre like '% A%' or nombre like '% S%') and (nombre like 'T%'or nombre like 's%' or nombre like 'B%' or nombre like 'N%') and oficina_rep is not null group by oficina_rep;
	 oficina_rep | count |    sum    
	-------------+-------+-----------
			  12 |     1 | 200000.00
			  13 |     1 | 350000.00
			  22 |     1 | 300000.00
	(3 rows)
	    
     
     


    7. Quins són els codis dels representants que tenen assignats més de 2 clients amb un nom d'empresa que contingui la lletra o I la lletra I?
    
	training=# select rep_clie, count(*) from clientes where empresa like '%o%' and empresa like '%i%' group by rep_clie having count(*)>2;
	 rep_clie | count 
	----------+-------
		  102 |     3
	(1 row)




    8. Quins codis de clients ens han comprat 3 o més vegades?

training=# select clie, count(*) from pedidos group by clie having count(*)>=3;
 clie | count 
------+-------
 2111 |     3
 2118 |     4
 2108 |     3
 2103 |     4
(4 rows)



    9. Quins codis de clients ens han comprat més d'una vegada el mateix producte?
    
	training=# select clie, fab, producto, count(*) from pedidos group by clie, fab, producto having count(*)>1;
	 clie | fab | producto | count 
	------+-----+----------+-------
	 2103 | aci | 41004    |     2
	(1 row)
    


    10. D'entre els productes que s'han venut més d'una vegada, quin és els que ens ha donat un total d'import més baix? 
    
	 training=# select fab, producto, min(importe) from pedidos group by fab, producto having count(*)>1 order by 3 asc;
	 fab | producto |   min    
	-----+----------+----------
	 aci | 4100x    |   150.00
	 rei | 2a45c    |   632.00
	 bic | 41003    |   652.00
	 aci | 41004    |   702.00
	 aci | 41002    |   760.00
	 qsa | xk47     |   776.00
	 qsa | k47      |  1420.00
	 fea | 114      |  1458.00
	 fea | 112      |  1480.00
	 rei | 2a44g    |  2100.00
	 imm | 773c     |  2925.00
	 aci | 41003    |  3745.00
	 imm | 779c     |  3750.00
	 aci | 4100z    | 15000.00
	 rei | 2a44r    | 22500.00
	 aci | 4100y    | 27500.00
	 imm | 775c     | 31350.00
	 rei | 2a44l    | 31500.00
	(18 rows)

training=# select fab, producto, min(importe) from pedidos group by fab, producto order by 3 asc limit 1;
 fab | producto |  min   
-----+----------+--------
 aci | 4100x    | 150.00
(1 row)
    
    Visualitza el codi del producta,la quantitat de cops que s'ha venut I el total d'import que s'ha cobrat per aquest producte.
 
 training=# select fab, producto, min(importe), count(*), sum(importe) from pedidos group by fab, producto having count(*)>1 order by 3 asc limit 1;
 fab | producto |  min   | count |  sum   
-----+----------+--------+-------+--------
 aci | 4100x    | 150.00 |     2 | 750.00
(1 row)



